import { Card } from "@tremor/react";
import { TestsNumbers } from "../components/info/testsNumbers";
import { Multiple } from "../components/info/multiple";
import { useRouter } from "next/router";
import { useContext } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { DashboardContext } from "@/utils/dashboardContext";
import { useTranslation } from "next-i18next";
import Head from "next/head";
import { Execution, TestSuite, emptyExecution } from "../components/utils/interfaces";
import { getExecution } from "../components/utils/utils";
import { ExecutionDonut } from "@/components/info/executionDonut";
import { Testsuites } from "@/components/tables/testSuites/testSuites";

export async function getServerSideProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["testinfo", "sidebar", "title", "common"])),
    },
  };
}

export default function ExecutionPage() {
    const router = useRouter();
    const { dashboardData } = useContext(DashboardContext);
    const { t } = useTranslation(["title", "common", "testinfo"]);
    let testObj:Execution = emptyExecution;
    let testSuitesObj:TestSuite[] = [];

    const { query } = router;
    const id = query.id;

    if (id && dashboardData && dashboardData.length > 0 && dashboardData[0].groups) {
      testObj = getExecution(dashboardData[0].groups, id.toString());
      testSuitesObj = testObj.testSuites || [];
    }
        
    return (
      <>
      <Head>
          <title>{t("executions")}</title>
      </Head>
      <main className="m-6">
          <Card>
          <Multiple
            column1={<ExecutionDonut {...testObj} />}
            column2={<TestsNumbers {...testObj} />}
          />
          </Card>
          <Card className="mt-10">
            <Testsuites {...testSuitesObj}/>
          </Card>
      </main>
      </>
   );
 }