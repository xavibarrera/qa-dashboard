import { Small } from "@/components/info/small";
import { ExecutionsResult } from "@/components/info/executionsResult";
import { Area } from "@/components/charts/area/area";
import { Card, Col, Grid} from "@tremor/react";
import { Multiple } from "@/components/info/multiple";
import { NameDateDonut } from "../components/info/nameDateDonut";
import { TestsNumbers } from "../components/info/testsNumbers";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { UserContext } from "../utils/userContext";
import { DashboardContext } from "@/utils/dashboardContext";
import { useContext } from "react";
import Head from "next/head";
import { getSelectedExecutions } from "../components/utils/executions"
import { avgTimeOfSelected } from "../components/utils/time"
import { getHeaderInfo } from "../components/utils/header"
import { Execution, Header } from "../components/utils/interfaces";

export async function getServerSideProps({ locale }: any) {
  const breadCrumbsData = [
    {
      label: "Home",
      path: "/",
    },
    {
      label: "Project",
      path: "/project",
    },
  ];

  return {
    props: {
      breadCrumbsData,
      ...(await serverSideTranslations(locale, ["common", "sidebar", "title"])),
    },
  };
}

export default function Project() {

  const { userData } = useContext(UserContext);
  const { dashboardData } = useContext(DashboardContext);

  let objTestsNumbers:Array<Execution> = [];
  let headerObjects:Header = {};
  let avgTime = "0";
  if (dashboardData.totalExecutions !== 0) {
    objTestsNumbers = getSelectedExecutions(dashboardData[0].groups);
    avgTime = avgTimeOfSelected(objTestsNumbers);
    headerObjects = getHeaderInfo(objTestsNumbers);
  }
  const sumatoriaPassed = objTestsNumbers.reduce((accumulator, item) => {
    if (item.numPassedTests !== undefined) {
      return accumulator + item.numPassedTests;
    }
    return accumulator;
  }, 0);



  const { t } = useTranslation(["common", "title"]);

  if (userData.status === "authenticated") {
    return (
      <>
        <Head>
          <title>{t("title:project")}</title>
        </Head>
        <main className="bg-slate-50 p-6 sm:p-10">
          <Grid numCols={6} className="gap-6">
            <Card className="flex justify-center items-center">
              <Small
                header={headerObjects.totalExecutions}
                text={t("executions")}
                color1={undefined}
                color2={undefined}
                time={"24h"}
                invisible={"invisible"}
                percentNumber={15}
              />
            </Card>
            <Card className="flex justify-center items-center">
              <Small
                header={headerObjects.totalTests}
                time={"24h"}
                text={t("test")}
                color1={undefined}
                color2={undefined}
                invisible={""}
                percentNumber={5}
              />
            </Card>
            <Card className="flex justify-center items-center">
              <Small
                header={avgTime ? avgTime.toString() : "0"}
                time={"24h"}
                text={t("avarageTime")}
                color1={undefined}
                color2={undefined}
                invisible={"invisible"}
                percentNumber={75}
              />
            </Card>
            <Card className="flex justify-center items-center">
              <Small
                header={sumatoriaPassed.toString()}
                time={"24h"}
                text={t("success")}
                color1={undefined}
                color2={"green" as const}
                invisible={"invisible"}
                percentNumber={85}
              />
            </Card>
            <Col numColSpan={1} numColSpanLg={2}>
              <Card>
                <ExecutionsResult
                  failed={headerObjects.totalFailed ?? 0}
                  passed={headerObjects.totalPassed ?? 0}
                  other={headerObjects.totalOther ?? 0}
                  showButton={false}
                />
              </Card>
            </Col>
          </Grid>
          <ul>
            {objTestsNumbers.map((item: any) => (
              <li key={item._id} className="mt-4 h-fit">
                <Card>
                  <Multiple
                    column1={<NameDateDonut {...item} />}
                    column2={<TestsNumbers {...item} />}
                    column3={<Area {...item.executions} />}
                    redirect={"/testinfo"}
                    token={item.token}
                  />
                </Card>
              </li>
            ))}
          </ul>
        </main>
      </>
    );
  }
}
