import "@/styles/globals.css";
import type { AppProps } from "next/app";
import Header from "../components/header/header";
import { appWithTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { useState, useEffect, useMemo } from "react";
import Sidebar from "components/sidebar/sidebar";
import { DashboardContext } from "@/utils/dashboardContext";
import { UserContext } from "@/utils/userContext";

const initialUser = {
  status: "unauthenticated",
};

let initialDashboard = [{
    _id: "",
    idUser: "",
    totalExecutions: 0,
    totalTest: 0,
    numFailedTests: 0,
    numPassedTests: 0,
    numOtherTests: 0,
    groups: []
}]

function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  let [userData, setUserData] = useState(initialUser);
  let [dashboardData, setDashboardData] = useState(initialDashboard);

  useEffect(() => {
    let userObject = localStorage.getItem('userObject');
    if (userObject != null) {
      setUserData(JSON.parse(userObject));
    }
    let dashboardObject = localStorage.getItem('dashboardObject');
    if (dashboardObject) {
      try {
        setDashboardData(JSON.parse(dashboardObject));
      } catch (error) {
        console.error('Error parsing dashboard data:', error);
      }
    }
  }, []);

  const memorizedDashboardContextValue = useMemo(() => ({
    dashboardData,
    setDashboardData,
  }), [dashboardData, setDashboardData]);

  const memorizedDataContextValue = useMemo(() => ({
    userData,
    setUserData,
  }), [userData, setUserData]);

  if (Component.displayName !== "ErrorPage") {
    if (router.pathname.includes("Password") || router.pathname == "/") {
      return (
        <DashboardContext.Provider value={memorizedDashboardContextValue}>
          <UserContext.Provider value={memorizedDataContextValue}>
            <div className="min-w-[94rem]">
              <Component {...pageProps} />
            </div>
          </UserContext.Provider>
        </DashboardContext.Provider>
      )
    }
    else {
      return (
        <DashboardContext.Provider value={memorizedDashboardContextValue}>
          <UserContext.Provider value={memorizedDataContextValue}>
			<div className="min-w-[94rem] flex">{/* 94rem = 376px https://tailwindcss.com/docs/customizing-spacing */}
              <div className="max-w-[80rem] min-h-screen max-h-screen overflow-y-scroll no-scrollbar flex-none" style={{ width: "260px" }}>
                <Sidebar {...pageProps} />
              </div>
              <div className="bg-slate-50 min-h-screen max-h-screen overflow-y-scroll flex-grow">
                <Header {...pageProps} />
                <Component {...pageProps} />
              </div>
            </div>
          </UserContext.Provider>
        </DashboardContext.Provider>
      );
    }
	
  } else {
    // Layout 404 page not found
    return (
      <div>
        <h1>Page not found</h1>
      </div>
    );
  }
}
export default appWithTranslation(App);
