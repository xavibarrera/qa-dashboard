import { Button, Metric, Subtitle } from "@tremor/react";
import { useRouter } from "next/router";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Head from "next/head";

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "forgotPassword",
        "sidebar",
        "titles",
      ])),
    },
  };
}

export default function changedPassword() {
  const router = useRouter();
  const { t } = useTranslation(["forgotPassword", "titles"]);
  return (
    <>
      <Head>
        <title>{t("titles:changed")}</title>
      </Head>
      <main>
        <div className="flex h-screen justify-center items-center">
          <div className="text-center ">
            <Metric>{t("ready")}</Metric>
            <Subtitle className="mt-4">{t("changedPassword")}</Subtitle>
            <Button
              variant="primary"
              color="orange"
              className="h-8 w-28 mt-10"
              data-testid="btnLogin"
              onClick={() => {
                router.push("/");
              }}
            >
              {t("button")}
            </Button>
          </div>
        </div>
      </main>
    </>
  );
}
