import { useState } from "react";
import PasswordModal from "@/components/account/passwordModal";
import NewPasswordModal from "@/components/account/newPasswordModal";
import ProfileCard from "@/components/account/profileCard";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";


export async function getServerSideProps({ locale }: any) {
    const breadCrumbsData = [
      {
        label: "Home",
        path: "/",
      },
      {
        label: "Account",
        path: "/account",
      },
    ];
  
    return {
      props: {
        breadCrumbsData,
        ...(await serverSideTranslations(locale, [
          "account",
          "sidebar",
          "title",
          "common",
        ])),
      },
    };
  }

export default function Account() {
    const [isChangePasswordModalOpen, setChangePasswordModalOpen] = useState(false);
    const [isNewPasswordModalOpen, setNewPasswordModalOpen] = useState(false);
    const [incorrectPassword, setIncorrectPassword] = useState(false);

    return (
        <div className="p-4">
            <ProfileCard
                setChangePasswordModalOpen={setChangePasswordModalOpen}
            />
            <PasswordModal
                isOpen={isChangePasswordModalOpen}
                onClose={() => {
                  setChangePasswordModalOpen(false)
                  setIncorrectPassword(false)
                }}
                incorrectPassword={incorrectPassword}
                setIncorrectPassword={setIncorrectPassword}
                openNewPasswordModal={() => setNewPasswordModalOpen(true)}
            />
            {isNewPasswordModalOpen && (
                <NewPasswordModal
                isOpen={isNewPasswordModalOpen}
                onClose={() => setNewPasswordModalOpen(false)}
                />
            )}
        </div>
    );
}
