import Login from "@/components/login";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Grid } from "@tremor/react";
import Image from "next/image";
import rs from "../public/images/bg13.jpg";
import { useTranslation } from "next-i18next";
import Head from "next/head";



export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["login", "title"])),
    },
  };
}

export default function Index() {
  const { t } = useTranslation("title");
  return (
    <>
      <Head>
        <title>{t("login")}</title>
      </Head>
      <main>
        <Grid numCols={2} className="h-screen">
          <div className="relative">
            <Image src={rs} alt="" fill={true} />
          </div>
          <Login />
        </Grid>
      </main>
    </>
  );
}
