import { Card, Divider, Grid } from "@tremor/react";
import React, { useContext } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Head from "next/head";
import { DashboardContext } from "@/utils/dashboardContext";
import { GroupInput } from "@/components/group/groupInput";
import { GroupCard } from "@/components/group/groupCard";
import { Group } from "@/components/utils/interfaces";

export async function getServerSideProps({ locale }: any) {
  const breadCrumbsData = [
    {
      label: "Home",
      path: "/",
    },
    {
      label: "Edit Groups",
      path: "/groups",
    },
  ];
  return {
    props: {
      breadCrumbsData,
      ...(await serverSideTranslations(locale, ["groups", "sidebar", "title"])),
    },
  };
}

export default function Groups() {
  const { dashboardData } = useContext(DashboardContext);
  const { t } = useTranslation("groups");
  return (
    <div>
      <div className="ml-4">
        <Head>
          <title>{t("title:groups")}</title>
        </Head>
        <GroupInput />
        <Divider />
      </div>
      <main className="bg-slate-50 p-6 sm:p-10">
        <Grid className="gap-6">
          {dashboardData[0].groups.length === 0 ? (
            <Card>{t("emptyGroups")}</Card>
            
          ) : (
            <ul>
              {dashboardData[0].groups.map((group: Group) => (
                <div key={group._id} className="mb-16">
                  <GroupCard group={group} tests={group.tests ?? []}></GroupCard>
                </div>
              ))}
            </ul>
          )}
        </Grid>
      </main>
    </div>
  );
}
