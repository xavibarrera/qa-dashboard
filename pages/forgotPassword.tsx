import ForgotPassword from "components/forgotPassword";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import Head from "next/head";

export async function getStaticProps({ locale }: any) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "forgotPassword",
        "sidebar",
        "title",
      ])),
    },
  };
}

export default function forgotPassword() {
  const { t } = useTranslation("title");
  return (
    <>
      <Head>
        <title>{t("forgot")}</title>
      </Head>
      <main>
        <ForgotPassword />
      </main>
    </>
  );
}
