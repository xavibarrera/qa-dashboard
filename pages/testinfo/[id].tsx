import { Card } from "@tremor/react";
import { NameDateDonut } from "../../components/info/nameDateDonut";
import { TestsNumbers } from "../../components/info/testsNumbers";
import { Multiple } from "../../components/info/multiple";
import { useRouter } from "next/router";
import { useContext } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { DashboardContext } from "@/utils/dashboardContext";
import { useTranslation } from "next-i18next";
import Head from "next/head";
import { Execution, Test, emptyTest } from "../../components/utils/interfaces";
import { Area } from "@/components/charts/area/area";
import { TestExecutions } from "@/components/tables/testExecutions/testExecutions";
import { getTest } from "../../components/utils/utils";

export async function getServerSideProps({ locale ,resolvedUrl, query }: any) {
  const breadCrumbsData = [
    {
      label: "Home",
      path: "/",
    },
    {
      label: "Project",
      path: "/project",
    },
    {
      label: query.id,
      path: resolvedUrl,
    },
  ];

  return {
    props: {
      breadCrumbsData,
      ...(await serverSideTranslations(locale, [
        "testinfo",
        "sidebar",
        "title",
        "common",
      ])),
    },
  };
}

export default function TestInfo() {
  const router = useRouter();
  const { dashboardData } = useContext(DashboardContext);
  const { t } = useTranslation(["title", "common"]);
  let testObj: Test = emptyTest;
  let executionObj: Execution[] = [];
  const { query } = router;
  const id = query.id;

  if (id && dashboardData && dashboardData.length > 0 && dashboardData[0].groups) {
    testObj = getTest(dashboardData[0].groups, id.toString());
    executionObj = testObj.executions || [];
  }

  return (
    <>
      <Head>
        <title>{t("executions")}</title>
      </Head>
      <main className="m-6">
        <Card>
          <Multiple
            column1={<NameDateDonut {...testObj} />}
            column2={<TestsNumbers {...testObj} />}
            column3={<Area {...executionObj} />}
          />
        </Card>
        <Card className="mt-10">
          <TestExecutions {...executionObj} />
        </Card>
      </main>
    </>
  );
}