import { dbConnect } from "utils/dbConnection";
import Results from "models/results";

dbConnect();

export default async function handler(req: any, res: any) {
    const { method, query: {id} } = req;
    if (method === "GET") {
        try {
            const result = await Results.find({ idUser: id });
            if (!result.toString())return res.status(404).json({ msg: "User not found" });
            return res.status(200).json(result);
        } catch (err : any) {
            return res.status(500).json({ error: err.message });
        }
    } else {
        return res.status(400).json({ message: 'Invalid method' })
    }
}