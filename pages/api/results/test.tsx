import { dbConnect } from "utils/dbConnection";
import Results from "models/results";
import randomstring from "randomstring";

dbConnect();

export default async function handler(req: any, res: any) {
    const { method, body } = req;
    if (method === "POST") {
        const token = randomstring.generate();
        try {
            const result = await Results.findOneAndUpdate(
                {  idUser: body.id, "groups._id": body.groupId},
                { $push: { "groups.$.tests": {
                    token: token,
                    name: body.test,
                    numTotalTests: 0,
                    numFailedTests: 0,
                    numPassedTests: 0,
                    numOtherTests: 0,
                    selected: true,
                    tests: []
                }}},
                { new: true }
            );
            if (!result) {
                return res.status(404).json({ msg: "Execution not found" });
            }
            
            return res.status(200).json(result);
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }else if(method === 'DELETE') {
        try {
            const { userId, groupId, testId } = body;
            
            const groupBeforeDelete = await Results.findOne(
                { idUser: userId, "groups._id": groupId },
                { "groups.$": 1 }
            );

            if (!groupBeforeDelete) {
                return res.status(404).json({ msg: 'Group not found' });
            }
            const testToDelete = groupBeforeDelete.groups[0].tests.find((test: any) => test._id.toString() === testId);
            const values = {
                numTotalTests: -testToDelete.numTotalTests,
                numFailedTests: -testToDelete.numFailedTests,
                numPassedTests: -testToDelete.numPassedTests,
                numOtherTests: -testToDelete.numOtherTests,
                executionsNum: -testToDelete.executions.length
            }
            if (!testToDelete) {
                return res.status(404).json({ msg: 'Test not found' });
            }

            await Results.findOneAndUpdate(
                { idUser: userId, "groups._id": groupId },
                {
                    $inc: {
                        "groups.$.numFailedTests": values.numFailedTests,
                        "groups.$.numOtherTests": values.numOtherTests,
                        "groups.$.numPassedTests": values.numPassedTests,
                        "groups.$.numTotalTests": values.numTotalTests,
                      },
                }
            );

            const userBeforeDelete = await Results.findOne({ idUser: userId });

            if (!userBeforeDelete) {
                return res.status(404).json({ msg: 'User not found' });
            }

            await Results.findOneAndUpdate(
                { idUser: userId },
                {
                    $inc: {
                        "numFailedTests": values.numFailedTests,
                        "numOtherTests": values.numOtherTests,
                        "numPassedTests": values.numPassedTests,
                        "totalTest": values.numTotalTests,
                        "totalExecutions": values.executionsNum,
                    },
                }
            );

            const deleteTest = await Results.findOneAndUpdate(
                {  idUser: userId, "groups._id": groupId },
                { $pull: { "groups.$.tests": { _id: testId } } },
                { new: true }
            );

            return res.status(200).json(deleteTest);
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }
	else if (method === "PUT") {
        try {
            const { userId, groupId, testId, newTestName } = body;

            const group = await Results.findOneAndUpdate(
                {
                    idUser: userId,
                    "groups._id": groupId,
                    "groups.tests._id": testId,
                },
                {
                    $set: {
                        "groups.$.tests.$[test].name": newTestName,
                    },
                },
                {
                    arrayFilters: [
                        {
                            "test._id": testId,
                        },
                    ],
                    new: true,
                }
            );

            if (!group) {
                return res.status(404).json({ msg: "Group or Test not found" });
            }

            return res.status(200).json(group);
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    } 
	else {
        return res.status(400).json({ message: 'Invalid method' });
    }
}
