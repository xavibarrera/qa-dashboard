import { test, expect,APIRequestContext,request } from "@playwright/test";
test.describe("Request without token", () => {
  test("Connection rejected", async ({ request }) => {
    const idUser = await request.post(`/api/results/[id]`, {
      data: {
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
      },
    });
    expect(idUser.ok()).toBeFalsy();
  });
});
test.describe("Request with token", () => {
  let context: APIRequestContext;
  test.beforeAll(async () => {
    context = await request.newContext({});
    const cookie = await context.post(`/api/users/auth/login`, {
      data: {
        email: "qatesting.redsauce@gmail.com",
        password: "Redsauce23!",
      },
    });
    expect(cookie.ok()).toBeTruthy();
  });
  test("Valid search", async () => {
    const idUser = await context.get(`/api/results/ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf`);
	expect(idUser.status()).toBe(200);
	expect(idUser.ok()).toBeTruthy();
	expect(await idUser.text()).toMatch("_id");
  });
  test("not valid search", async () => {
    const idUser = await context.get(`/api/results/ksCfbSvebgXS38OjEeSDCHVX8WVmYCya`);
	expect(idUser.status()).toBe(404);
	expect(idUser.ok()).toBeFalsy();
	expect(await idUser.text()).toMatch("User not found");
  });
  test("Invalid method", async () => {
    const idUser = await context.post(`/api/results/ksCfbSvebgXS38OjEeSDCHVX8WVmYCya`);
	expect(idUser.ok()).toBeFalsy();
	expect(idUser.status()).toBe(400);
	expect(await idUser.text()).toMatch("Invalid method");
  });
});
