import { dbConnect } from "utils/dbConnection";
import Results from "models/results";
import createTestToken from "utils/createTestToken";

dbConnect();

export default async function handler(req: any, res: any) {
    const { method, body } = req;

    if (method === "GET") {
        try {
            const result = await Results.find();
            return res.status(200).json(result);
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else if (method === 'POST') {
        try {
            const newResult = new Results(body);
            createTestToken(newResult);
            const savedResult = await newResult.save();
            return res.status(201).json(savedResult);
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else {
        return res.status(400).json({ message: 'Invalid method' })
    }
}