import { dbConnect } from "utils/dbConnection";
import Results from "models/results";

dbConnect();

export default async function handler(req: any, res: any) {
    const { method, body } = req;
    if (method === "PATCH") {
      try {
        const { group, test } = body;
        const result = await Results.findOne({
          "groups._id": group,
          "groups.tests._id": test,
        });
  
        if (!result) {
          return res.status(404).json({ message: "No se encontró el resultado" });
        }
        const groupIndex = result.groups.findIndex((g: any) => g._id.toString() === group.toString());
        const testIndex = result.groups[groupIndex].tests.findIndex((t: any) => t._id.toString() === test.toString());
        result.groups[groupIndex].tests[testIndex].selected = !result.groups[groupIndex].tests[testIndex].selected;
        await result.save();
  
        return res.status(200).json(result);
      } catch (err: any) {
        return res.status(500).json({ error: err.message });
      }
    } else {
      return res.status(400).json({ message: "Método inválido" });
    }
  }