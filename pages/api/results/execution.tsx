import { dbConnect } from "utils/dbConnection";
import Results from "models/results";
import { xmlToJson, parseJUnitJSON} from "@/utils/database";
import { getExecutionNumber } from "../utils";

dbConnect();

export default async function handler(req: any, res: any) {
    const { method, body, headers } = req;
    if (method === 'POST') {
		const mergedXML = body;
        const junitJson = xmlToJson(mergedXML);
        if (junitJson.hasOwnProperty('err')) {  // eslint-disable-line
            res.status(400).json(junitJson);
        } else {
            const jsonInfo = parseJUnitJSON(junitJson);
            const { timestamp, execTime, numTotalTests, numFailedTests, numPassedTests, numOtherTests, testSuites } = jsonInfo;
            try {
                const document = await Results.findOne({ "groups.tests": { $elemMatch: { token: headers.token } } }, { "groups.$": 1 });

                if (!document) {
                    return res.status(404).send('No document found with the specified token');
                }
                const executionNum = getExecutionNumber(document, headers.token);

                await Results.updateOne(
                    { "groups.tests.token": headers.token },
                    { 
                        $push: { "groups.$[i].tests.$[j].executions": { executionNum, timestamp, execTime, numTotalTests, numFailedTests, numPassedTests, numOtherTests, testSuites } } ,
                        $inc: {
                            "groups.$[i].tests.$[j].numFailedTests": numFailedTests,
                            "groups.$[i].tests.$[j].numOtherTests": numOtherTests,
                            "groups.$[i].tests.$[j].numPassedTests": numPassedTests,
                            "groups.$[i].tests.$[j].numTotalTests": numTotalTests,
                            "groups.$[i].numFailedTests": numFailedTests,
                            "groups.$[i].numOtherTests": numOtherTests,
                            "groups.$[i].numPassedTests": numPassedTests,
                            "groups.$[i].numTotalTests": numTotalTests,
                            "totalTest": numTotalTests,
                            "numFailedTests": numFailedTests,
                            "numPassedTests": numPassedTests,
                            "numOtherTests": numOtherTests,
                            "totalExecutions": 1
                         } 
                    },
                    { arrayFilters: [ { "i._id": document.groups[0]._id }, { "j.token": headers.token } ] }
                );

                res.status(200).send(document);
            } catch (err: any) {
                return res.status(500).json({ error: err.message });
            }
        }
    }
    else {
        return res.status(400).json({ message: 'Invalid method' })
    }
}