import { test, expect, APIRequestContext, request } from "@playwright/test";
test.describe("Request without token", () => {
  test("Connection rejected", async ({ request }) => {
    const result = await request.post(`/api/results/executionDev`, {
      data: {
        token: "00000000000000000000000000000000",
      },
    });
    expect(result.ok()).toBeFalsy();
  });
});
test.describe("Request with token", () => {
  let context: APIRequestContext;
  let groupID: string;
  let testToken: string;
  test.beforeAll(async () => {
    context = await request.newContext({});
    const cookie = await context.post(`/api/users/auth/login`, {
      data: {
        email: "qatesting.redsauce@gmail.com",
        password: "Redsauce23!",
      },
    });
    expect(cookie.ok()).toBeTruthy();
    const query = await context.post(`/api/results/group`, {
      data: {
        group: "prueba",
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
      },
    });
    expect(query.status()).toBe(200);
    expect(query.ok()).toBeTruthy();
    expect(await query.text()).toMatch("_id");
    const aux = await query.json();
    groupID = aux.groups[0]._id;
    const queryTest = await context.post(`/api/results/test`, {
      data: {
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
        groupId: groupID,
        test: "TestDePrueba",
      },
    });
    expect(queryTest.status()).toBe(200);
    expect(queryTest.ok()).toBeTruthy();
    expect(await queryTest.text()).toMatch("_id");
    const aux2 = await queryTest.json();
    testToken = aux2.groups[0].tests[0].token;
  });
  test("Valid token", async () => {
    const query = await context.post(`/api/results/executionDev`, {
      data: {
        token: testToken,
      },
    });
    expect(query.status()).toBe(200);
    expect(query.ok()).toBeTruthy();
    expect(await query.text()).toMatch("OK");
  });
  test("Invalid user id", async () => {
    const query = await context.post(`/api/results/executionDev`, {
      data: {
        token: "00000000000000000000000000000000",
      },
    });
    expect(query.status()).toBe(404);
    expect(query.ok()).toBeFalsy();
    expect(await query.text()).toMatch(
      "No document found with the specified token"
    );
  });
  test("Invalid method", async () => {
    const query = await context.get(`/api/results/executionDev`);
    expect(query.ok()).toBeFalsy();
    expect(query.status()).toBe(400);
    expect(await query.text()).toMatch("Invalid method");
  });
});
