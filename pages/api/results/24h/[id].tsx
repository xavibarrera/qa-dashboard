import { dbConnect } from "utils/dbConnection";
import Execution from "@/models/results";

dbConnect();

export default async function handler(req: any, res: any) {
    const { method, query: {id} } = req;
    
    if (method === "GET") {
        try {
            const execution = await Execution.find({ idUser: id, "groups.tests.executions.updatedAt":{$gt:new Date(Date.now() - 24*60*60 * 1000)} })
            if (!execution) return res.status(404).json({ msg: "Execution not found" });
            return res.status(200).json(execution);
        } catch (err : any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else if (method === "DELETE") {
        try {
            const deletedExecution = await Execution.findByIdAndDelete(id);
            if (!deletedExecution) res.status(404).json({ msg: "Execution not found" })
            return res.status(204).json();
        } catch (err : any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else {
        return res.status(400).json({ message: 'Invalid method' })
    }
}