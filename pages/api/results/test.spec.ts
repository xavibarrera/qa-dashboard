import { test, expect, APIRequestContext, request } from "@playwright/test";
//Post missing delete and put
test.describe("Request without token", () => {
  test("Connection rejected", async ({ request }) => {
    const result = await request.post(`/api/results/test`, {
      data: {
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
        groupId: "a1",
        test: "TestDePrueba",
      },
    });
    expect(result.ok()).toBeFalsy();
  });
});
test.describe("Request with token", () => {
  let context: APIRequestContext;
  let groupID: string;
  test.beforeAll(async () => {
    context = await request.newContext({});
    const cookie = await context.post(`/api/users/auth/login`, {
      data: {
        email: "qatesting.redsauce@gmail.com",
        password: "Redsauce23!",
      },
    });
    expect(cookie.ok()).toBeTruthy();
    const query = await context.post(`/api/results/group`, {
      data: {
        group: "prueba",
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
      },
    });
    expect(query.status()).toBe(200);
    expect(query.ok()).toBeTruthy();
    expect(await query.text()).toMatch("_id");
    const aux = await query.json();
    groupID = aux.groups[0]._id;
  });
  test("Valid group id", async () => {
    const query = await context.post(`/api/results/test`, {
      data: {
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
        groupId: groupID,
        test: "TestDePrueba",
      },
    });
    expect(query.status()).toBe(200);
    expect(query.ok()).toBeTruthy();
    expect(await query.text()).toMatch("_id");
  });
  test("Invalid group id", async () => {
    const query = await context.post(`/api/results/test`, {
      data: {
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
        groupId: "000000000000000000000000",
        test: "TestDePrueba",
      },
    });
    expect(query.status()).toBe(404);
    expect(query.ok()).toBeFalsy();
    expect(await query.text()).toMatch("not found");
  });
    test("Invalid method", async () => {
      const query = await context.get(`/api/results/test`);
      expect(query.ok()).toBeFalsy();
      expect(query.status()).toBe(400);
      expect(await query.text()).toMatch("Invalid method");
    });
});
