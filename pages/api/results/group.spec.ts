import { test, expect, APIRequestContext, request } from "@playwright/test";
//Post missing delete and put
test.describe("Request without token", () => {
  test("Connection rejected", async ({ request }) => {
    const result = await request.post(`/api/results/group`, {
      data: {
        group: "prueba",
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
      },
    });
    expect(result.ok()).toBeFalsy();
  });
});
test.describe("Request with token", () => {
  let context: APIRequestContext;
  test.beforeAll(async () => {
    context = await request.newContext({});
    const cookie = await context.post(`/api/users/auth/login`, {
      data: {
        email: "qatesting.redsauce@gmail.com",
        password: "Redsauce23!",
      },
    });
    expect(cookie.ok()).toBeTruthy();
  });
  test("Valid user id", async () => {
    const query = await context.post(`/api/results/group`, {
      data: {
        group: "prueba",
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmYCyf",
      },
    });
    expect(query.status()).toBe(200);
    expect(query.ok()).toBeTruthy();
    expect(await query.text()).toMatch("_id");
  });
  test("Invalid user id", async () => {
    const query = await context.post(`/api/results/group`, {
      data: {
        group: "prueba",
        id: "ksCfbSvebgXS38OjEeSDCHVX8WVmaayf",
      },
    });
    expect(query.status()).toBe(404);
    expect(query.ok()).toBeFalsy();
    expect(await query.text()).toMatch("not found");
  });
  test("Invalid method", async () => {
    const query = await context.get(`/api/results/group`);
    expect(query.ok()).toBeFalsy();
    expect(query.status()).toBe(400);
    expect(await query.text()).toMatch("Invalid method");
  });
});
