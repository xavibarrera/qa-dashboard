import { dbConnect } from "utils/dbConnection";
import Results from "models/results";

dbConnect();

export default async function handler(req: any, res: any) {
  const { method, body } = req;
  if (method === "POST") {
    try {
      const result = await Results.findOneAndUpdate(
        { idUser: body.id },
        {
          $push: {
            groups: {
              name: body.group,
              numTotalTests: 0,
              numFailedTests: 0,
              numPassedTests: 0,
              numOtherTests: 0,
              tests: [],
            },
          },
        },
        { new: true }
      );
      if (!result) {
        return res.status(404).json({ msg: "Execution not found" });
      }

      return res.status(200).json(result);
    } catch (err: any) {
      return res.status(500).json({ error: err.message });
    }
  } else if (method === "DELETE") {
    try {
      const { userId, groupId } = body;
      const groupBeforeDelete = await Results.findOne(
        { idUser: userId, "groups._id": groupId },
        { "groups.$": 1 }
      );

      if (!groupBeforeDelete) {
        return res.status(404).json({ msg: "Group not found" });
      }

      const groupToDelete = groupBeforeDelete.groups[0];
      let executionsNum = 0;

      groupToDelete.tests.forEach((test: any) => {
        executionsNum += test.executions.length;
      });

      const values = {
        numTotalTests: -groupToDelete.numTotalTests,
        numFailedTests: -groupToDelete.numFailedTests,
        numPassedTests: -groupToDelete.numPassedTests,
        numOtherTests: -groupToDelete.numOtherTests,
        executionsNum: -executionsNum,
      };

      await Results.findOneAndUpdate(
        { idUser: userId },
        {
          $inc: {
            numFailedTests: values.numFailedTests,
            numOtherTests: values.numOtherTests,
            numPassedTests: values.numPassedTests,
            totalTest: values.numTotalTests,
            totalExecutions: values.executionsNum,
          },
        }
      );

      const deleteGroup = await Results.findOneAndUpdate(
        { idUser: userId },
        { $pull: { groups: { _id: groupId } } },
        { new: true }
      );

      return res.status(200).json(deleteGroup);
    } catch (err: any) {
      return res.status(500).json({ error: err.message });
    }
  } else if (method === "PUT") {
    try {
      const { userId, groupId, newGroupName } = body;

      await Results.updateOne(
        { idUser: userId, "groups._id": groupId },
        {
          $set: {
            "groups.$.name": newGroupName,
            "groups.$[].tests.$[].groupName": newGroupName,
          },
        }
      );

      await Results.updateOne(
        { idUser: userId, "groups._id": groupId },
        {
          $set: {
            "groups.$[].tests.$[].executions.$[].groupName": newGroupName,
          },
        }
      );

      const updatedGroup = await Results.findOne(
        { idUser: userId, "groups._id": groupId },
        { "groups.$": 1 }
      );

      return res.status(200).json(updatedGroup);
    } catch (err: any) {
      return res.status(500).json({ error: err.message });
    }
  } else {
    return res.status(400).json({ message: "Invalid method" });
  }
}
