import cookie from 'cookie';

export default async function handler(req: any, res: any) {
    const { method } = req;
    if (method === "GET") {
        res.setHeader('Set-Cookie',
            cookie.serialize('userLoged', "1",
                {
                    maxAge: -1,
                    path: '/'
                }));
        return res.status(200).json({ message: "logout done" });
    }
    else {
        return res.status(400).json({ message: "Invalid method" });
    }
}