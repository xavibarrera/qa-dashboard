import { dbConnect } from "utils/dbConnection";
import User from "models/user";
import sendEmail from "utils/email";

dbConnect();

export default async function forgotPassword(req: any, res: any) {
    const jwt = require("jsonwebtoken");  // eslint-disable-line
    const { method, body } = req;

    if (method === "POST") {
        const { email, locale} = body;
        try {
            const user = await User.findOne({ email: email });
            if (!user) {
                return res.status(200).json( { message: "Email sended" });
            }
            const secret = user.password + "abc123";
            const token = jwt.sign({ email: user.email, id: user._id }, secret, { expiresIn: "5m" });
            const link = process.env.NEXT_PUBLIC_url + `/${locale}/resetPassword/${user._id}/${token}`;
            await sendEmail(email, link);
            return res.status(200).json( { message: "Email sended" });
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else {
        return res.status(400).json({ message: "Invalid method" });
    }
}