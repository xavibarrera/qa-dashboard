import { dbConnect } from "utils/dbConnection";
import User from "models/user";
import * as EmailValidator from 'email-validator';
import cookie from "cookie";
import bcrypt from "bcryptjs";

dbConnect();

export default async function handler(req: any, res: any) {
  const { method, body } = req;
  if (method === "POST") {
    if (EmailValidator.validate(body.email)) {
      try {
        const userDB = await User.findOne({ email: body.email });
        if (!userDB) {
          return res.status(401).json({ message: "Unregistered user" });
        }
        const checkPassword = await bcrypt.compare(
          body.password,
          userDB.password
        );
        if (!checkPassword) {
          return res.status(401).json({ message: "Authentication Failed" });
        }
        const jwt = require("jsonwebtoken"); // eslint-disable-line
        const newUser = jwt.sign(
          {
            username: userDB.username,
            email: userDB.email,
            idDashboard: userDB.idDashboard,
            token: process.env.NEXT_PUBLIC_token,
          },
          process.env.NEXT_PUBLIC_secret
        );
        res.setHeader(
          "Set-Cookie",
          cookie.serialize("userLoged", JSON.stringify(newUser), {
            httpOnly: true,
            maxAge: 60 * 60 * 24 * 7,
            sameSite: "strict",
            path: "/",
          })
        );
		const userLoged = {
			username: userDB.username,
			email: userDB.email,
			idDashboard: userDB.idDashboard
		};

		return res.status(200).json({ message: "Authentication Success",  body: userLoged })
      } catch (err: any) {
        return res.status(500).json({ error: err.message });
      }
    } else {
      return res.status(401).json({ message: "Invalid email" });
    }
  } else {
    return res.status(400).json({ message: "Invalid method" });
  }
}
