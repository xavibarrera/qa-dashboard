import { dbConnect } from "utils/dbConnection";
import User from "models/user";
import bcrypt from 'bcryptjs';

dbConnect();

export default async function verifyPassword(req: any, res: any) {

    const { body, method } = req;

    if (method === "POST") {
        try {
            const { email, password } = body;
            const user = await User.findOne({ email: email });
            if (!user) {
                return res.status(401).json({ message: "Unregistered user" });
            }
            const isPasswordValid = await bcrypt.compare(password, user.password);
            if (isPasswordValid) {
                return res.status(200).json({ message: "Password is valid" });
            } else {
                return res.status(401).json({ message: "Invalid password" });
            }
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    } else {
        return res.status(400).json({ error: "Invalid method" });
    }
}