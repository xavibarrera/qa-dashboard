import { test, expect } from "@playwright/test";
test("Valid login", async ({ request }) => {
  const login = await request.post(`/api/users/auth/login`, {
    data: {
      email: "qatesting.redsauce@gmail.com",
      password: "Redsauce23!",
    },
  });
  expect(login.headers()).toHaveProperty('set-cookie');
  expect(login.ok()).toBeTruthy();
  expect(login.status()).toBe(200);
  expect(await login.text()).toMatch("Authentication Success");
});
test("Unregistered user", async ({ request }) => {
  const login = await request.post(`/api/users/auth/login`, {
    data: {
      email: "qatesting.bluesauce@gmail.com",
      password: "Redsauce21!",
    },
  });
  expect(login.ok()).toBeFalsy();
  expect(login.status()).toBe(401);
  expect(await login.text()).toMatch("Unregistered user");
});
test("Invalid login", async ({ request }) => {
  const login = await request.post(`/api/users/auth/login`, {
    data: {
      email: "qatesting.redsauce@gmail.com",
      password: "Redsauce22!",
    },
  });
  expect(login.ok()).toBeFalsy();
  expect(login.status()).toBe(401);
  expect(await login.text()).toMatch("Authentication Failed");
});
test("Invalid method", async ({ request }) => {
  const login = await request.get(`/api/users/auth/login`);
  expect(login.ok()).toBeFalsy();
  expect(login.status()).toBe(400);
  expect(await login.text()).toMatch("Invalid method");
});
