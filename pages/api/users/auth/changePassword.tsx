import { dbConnect } from "utils/dbConnection";
import User from "models/user";
import bcrypt from 'bcryptjs';

dbConnect();

export default async function changePassword(req: any, res: any) {
    const { method, body } = req;

    if (method === "PUT") {
        const { email, password } = body;
        try {
            const user = await User.findOne({ email: email });
            if (!user) {
                return res.status(401).json({ message: "Unregistered user" });
            }
            const salt = await bcrypt.genSalt(10);
            const hashPass = await bcrypt.hash(password, salt)
            await User.updateOne({ email: user.email }, { password: hashPass});
            return res.status(200).json({ message: "Password changed" })
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else {
        return res.status(400).json({ message: "Invalid method" });
    }
}