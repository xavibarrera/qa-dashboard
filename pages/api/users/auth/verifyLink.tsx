import { dbConnect } from "utils/dbConnection";
import User from "models/user";

dbConnect();

export default async function verifyLink(req: any, res: any) {
    const jwt = require("jsonwebtoken");  // eslint-disable-line
    const { body, method } = req;

    if (method === "POST") {
        try {
            const user = await User.findOne({ _id: body.id });
            if (!user) {
                return res.status(401).json({ message: "Unregistered user" });
            }
            const secret = user.password + "abc123";
            const verify = jwt.verify(body.token, secret);
            return res.status(200).json({ email: verify.email });

        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else {
        return res.status(400).json({ error: "Invalid method" });
    }
}