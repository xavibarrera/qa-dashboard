import { dbConnect } from "utils/dbConnection";
import User from "models/user";

dbConnect();

export default async function handler(req: any, res: any) {

    const { method, body } = req;

    if (method === "POST") {
        try {
            const user = await User.findOne({ email: body.email });
            if (!user) {
                return res.status(404).json({ message: "User not found" });
            }
            await User.updateOne({ email: user.email }, { dashboard: body.idDashboard });
            return res.status(200).json({ message: 'Dashboard added to the user' })
        } catch (err: any) {
            return res.status(500).json({ error: err.message });
        }
    }
    else {
        return res.status(400).json({ message: 'Invalid method' })
    }

}