import { Data, Test } from "@/components/utils/interfaces";

export function getExecutionNumber(document: Data, token: string) {
  if (!document.groups || document.groups.length === 0) {
    throw new Error("Invalid document format: 'groups' property is missing or empty.");
  }

  const firstGroup = document.groups[0];
  if (!firstGroup || !firstGroup.tests || firstGroup.tests.length === 0) {
    throw new Error("Invalid document format: 'tests' property is missing or empty in the first group.");
  }

  const testIndex = firstGroup.tests.findIndex((test: Test) => test.token === token);

  if (testIndex === -1) {
    throw new Error("Test with the given token not found.");
  }

  const executions = firstGroup.tests[testIndex].executions;

  if (!executions || executions.length === 0) {
    return 1; // If no executions exist, return 1 for the first execution number.
  }

  const executionsSize = executions.length;
  const lastExecutionNum = executions[executionsSize - 1]?.executionNum ?? 0;

  const executionNum = lastExecutionNum + 1;
  return executionNum;
}
