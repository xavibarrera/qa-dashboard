import { Small } from "@/components/info/small";
import { ExecutionsResult } from "@/components/info/executionsResult";
import { Bar } from "@/components/charts/bar";
import { Card, Grid } from "@tremor/react";
import { Executions } from "../components/tables/executions";
import { useContext } from "react";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { DashboardContext } from "@/utils/dashboardContext";
import { useTranslation } from "next-i18next";
import { getLastExecution } from "../components/utils/executions"
import { averageExecTime } from "../components/utils/time"
import Head from "next/head";
import { getPercent } from "../components/utils/utils";
import { AreaFilter } from "@/components/charts/area/areafilter";

export async function getServerSideProps(context : any) {
  const breadCrumbsData = [
    {
      label: "Home",
      path: "/",
    },
  ];

  return {
    props: {
      breadCrumbsData,
      ...(await serverSideTranslations(context.locale, ["common", "sidebar", "title", "groups"])),
    },
  };
}

export default function Home() {

  const {dashboardData} = useContext(DashboardContext);
  let avgTime = averageExecTime(dashboardData[0]);
  let lastExec = getLastExecution(dashboardData[0].groups);
  let percent = getPercent(dashboardData[0]);
  const { t } = useTranslation(["common", "sidebar", "title"]);

  return (
    <>
      <Head>
        <title>{t("title:home")}</title>
      </Head>
      <main className="m-6">
        <Grid numCols={3} className="gap-6">
          <Grid numCols={2} className="gap-6">
            <Card>
              <Small
                header={dashboardData[0]?.totalExecutions ?? 0}
                text={t("executions")}
                time="24h"
                invisible="invisible"
                percentNumber={15}
              />
            </Card>
            <Card>
              <Small
                header={dashboardData[0]?.numFailedTests + dashboardData[0]?.numOtherTests ?? 0}
                text={t("failedTest")}
                time="24h"
                color1={"yellow" as const}
                color2={"yellow" as const}
                invisible="invisible"
                percentNumber={50}
              />
            </Card>
            <Card>
              <Small
                header={dashboardData[0]?.totalTest ?? 0}
                text={t("test")}
                time="24h"
                invisible=""
                percentNumber={percent}
              />
            </Card>
            <Card>
              <Small
                header= {avgTime ? avgTime.toString() : "0"}
                text={t("avarageTime")}
                time="24h"
                invisible="invisible"
                percentNumber={75}
              />
            </Card>
          </Grid>
          <Card>
            <ExecutionsResult
              failed={dashboardData[0]?.numFailedTests ?? 0}
              passed={dashboardData[0]?.numPassedTests ?? 0}
              other={dashboardData[0]?.numOtherTests ?? 0}
            />
          </Card>
          <Card>
            <strong> {t("lastExecutionResults")}: </strong>
            <span className="text-sm">
              {lastExec?.groupName ?? ""} - {lastExec?.testName ?? ""}
            </span>
            <div className="mt-8">
              <Bar
                data={[
                  {
                    Date: lastExec ? lastExec.timestamp : "",
                    Failed: lastExec ?  lastExec.numFailedTests : 0,
                    Passed: lastExec ? lastExec.numPassedTests : 0,
                    Other: lastExec ? lastExec.numOtherTests : 0,
                  },
                ]}
                group={dashboardData[0].groups?.[0]?.name ?? ""}
                test={dashboardData[0].groups?.[0]?.tests?.[0]?.name ?? ""}
                layout={"horizontal" as const}
                stack={false}
                maxValue={lastExec?.numTotalTests ? lastExec.numTotalTests : 0}
                index="Date"
                className="mt-4 h-52"
              />
            </div>
          </Card>
        </Grid>
        <div className="mt-10">
          <Card>
            {dashboardData[0] && (
              <AreaFilter/>
            )}
          </Card>
        </div>
        <div className="mt-10" id="test-cases-chart">
          <Card>
            {dashboardData[0] && (
              <Executions {...dashboardData[0]} />
            )}
          </Card>
        </div>
      </main>
    </>
  );
}
