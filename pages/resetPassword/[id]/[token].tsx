import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { Metric } from "@tremor/react";
import { useTranslation } from "next-i18next";
import ResetPassword from "components/resetPassword";
import Head from "next/head";


export async function getServerSideProps(context: any) {
  const res = await fetch(
    process.env.NEXT_PUBLIC_url + `/api/users/auth/verifyLink`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: context.params.id,
        token: context.params.token,
      }),
    }
  );
  const result = await res.json();
  const locale = context.locale;
  return {
    props: {
      result,
      ...(await serverSideTranslations(locale, ["forgotPassword"])),
    },
  };
}
export default function ResetPasswordPage(data: any) {
  const { t } = useTranslation("title");
  if (data.result.email === undefined) {
    return (
      <div className="flex h-screen justify-center items-center">
        <div className="text-center ">
          <Metric>{t("expired")}</Metric>
        </div>
      </div>
    );
  }
  const email = data.result.email
  return (
    <>
      <Head>
        <title>{t("forgot")}</title>
      </Head>
      <main>
        <ResetPassword email={email} />
      </main>
    </>
  );
}
