module.exports = {
	i18n: {
	  defaultLocale: "en",
	  locales: ["en", "es"],
	  
	},
	localePath: "./locales",
	react: { useSuspense: false },
  };
  