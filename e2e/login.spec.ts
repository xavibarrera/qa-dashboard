import { test, expect } from "./qaDashboardFixture";
import data from "./data";
test.describe("Login Tests", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("/");
  });

  test("Valid login", async ({ page,qaDashboard }) => {
	await qaDashboard.login(data.validEmail,data.validPassword);
	await expect(page).toHaveURL(/home/)
  });
  test("Invalid login", async ({ page,qaDashboard }) => {
	await qaDashboard.login(data.invalidEmail,data.invalidPassword);
    const loginError = page.getByTestId("loginError");
    await expect(loginError).toHaveText(data.errorMessage);
    await expect(page).toHaveURL("/");
  });
  
  test("error 500", async ({ page,qaDashboard }) => {
    await page.route("/api/users/auth/login", (route: any) => {
      route.fulfill({
        status: 500,
      });
    });
	await qaDashboard.login(data.validEmail,data.validPassword);
	const loginError = page.getByTestId("loginError");
    await expect(loginError).toHaveText(data.errorMessageConnection);
  });
});
