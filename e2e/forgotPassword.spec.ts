import { test, expect } from "./qaDashboardFixture";
test.describe("Recover Password", () => {
  test.beforeEach(async ({ page }) => {
    await page.goto("/");
  });
  test("Valid recover password", async ({ page, qaDashboard }) => {
    await expect(page).toHaveURL("/");
    const recoverPasswordButton = page.getByTestId("btnRecoverPassword");
    await recoverPasswordButton.click();
	await expect(page).toHaveURL(/forgotPassword/);
	await test.step('Request password change', async () => {
		await qaDashboard.recoverPassword("qatesting.redsauce@gmail.com", "Redsauce23!");
	});
	await expect(page).toHaveURL("/");
	await test.step('Log in to check that the new password works', async () => {
		await qaDashboard.login("qatesting.redsauce@gmail.com", "Redsauce23!");
		await expect(page).toHaveURL(/home/)	  
	});

  });
});
