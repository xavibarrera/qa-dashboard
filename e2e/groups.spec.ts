import { test, expect } from "./qaDashboardFixture";
import data from "./data";
test.describe("Groups Tests", () => {
  test.beforeEach(async ({ page, qaDashboard }) => {
    await page.goto("/");
    await qaDashboard.login(data.validEmail,data.validPassword);
    await expect(page).toHaveURL(/home/)
    await page.goto("/groups")
  });

  test("Correct group insertion", async ({ page }) => {
    await expect(page).toHaveURL("/groups");
    const addGroupInput = page.getByTestId("addGroupInput");
    await addGroupInput.click();
    await page.getByTestId('addGroupInput').fill('New group');
    await page.getByTestId("addGroupButton").click();
    const addedGroup = page.getByTestId("groupName").nth(-1);
    await expect(addedGroup).toHaveText('New group');
  });

  test("Correct test insertion", async ({ page }) => {
    await expect(page).toHaveURL("/groups");
    const addTestInput = page.getByTestId("addTestInput").nth(-1);
    await addTestInput.click();
    await page.getByTestId('addTestInput').nth(-1).fill('New test');
    await page.getByTestId("addTestButton").nth(-1).click();
    const addedTest = page.getByTestId("testName").nth(-1);
    await expect(addedTest).toHaveText('New test');
  });

  test("Correct group edit", async ({ page }) => {
    await expect(page).toHaveURL("/groups");

    const editGroupIcon = page.getByTestId("editGroupInput").nth(-1);
    await editGroupIcon.click();
    await page.getByTestId('inputGroupName').nth(-1).fill('Group name edited');
    await page.keyboard.press('Enter');
    const groupName = page.getByTestId('groupName').nth(-1);
    await expect(groupName).toHaveText('Group name edited');
    });

  test("Correct test edit", async ({ page }) => {
    await expect(page).toHaveURL("/groups");

    const editTestIcon = page.getByTestId("testNameIcon").nth(-1);
    await editTestIcon.click();
    await page.getByTestId('editTestInput').fill('Test name edited');
    await page.keyboard.press('Enter');
    const groupName = page.getByTestId('testName').nth(-1);
    await expect(groupName).toHaveText('Test name edited');
  });

  
  test("Correct test delete", async ({ page }) => {
    await expect(page).toHaveURL("/groups");

    const deleteTestButton = page.getByTestId("deleteTestButton").nth(-1);
    await deleteTestButton.click();
    const modalDeleteTestButton = page.getByTestId("modalDeleteTestButton")
    await modalDeleteTestButton.click();
    expect(page).not.toContain('Test to delete');
  });

  test("Correct group delete", async ({ page }) => {
    await expect(page).toHaveURL("/groups");

    const deleteGroupButton = page.getByTestId("deleteGroupButton").nth(-1);
    await deleteGroupButton.click();
    const modalDeleteGroupButton = page.getByTestId("modalDeleteGroupButton")
    await modalDeleteGroupButton.click();
    expect(page).not.toContain('Group to delete');
  });
})