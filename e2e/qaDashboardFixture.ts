import {test as baseTest} from "@playwright/test";
import QADashboard from "./qaDashboard"
interface Pages {
	qaDashboard : QADashboard
}

const testPages = baseTest.extend<Pages>({
	qaDashboard: async ({page},use) =>{
		await use(new QADashboard(page))
	}
})

export const test = testPages;
export const expect = testPages.expect