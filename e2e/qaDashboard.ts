import {expect,Page} from "@playwright/test";
import {mailHelper} from "../utils/mail.helper"
import data from "./data";

export default class QADashboard{
	constructor(public page:Page){}
	async login(email:string,password:string){
		await expect(this.page).toHaveURL("https://dashboard-test.redsauce.net");
		const emailInput = this.page.getByTestId("inputEmail");
		await emailInput.fill(email);
		await expect(emailInput).toHaveValue(email);
		const passwordInput = this.page.getByTestId("inputPassword");
		await passwordInput.fill(password);
		await expect(passwordInput).toHaveValue(password);
		const loginButton = this.page.getByTestId("btnLogin");
		await loginButton.click();
	}
	async recoverPassword(email:string,password:string){
		const emailInput = this.page.getByTestId("inputEmail");
		await emailInput.fill(email);
		await expect(emailInput).toHaveValue(email);
		const sendButton = this.page.getByTestId("btnLogin");
		await sendButton.click();
		const confirmationMessage = this.page.getByTestId("confirmation");
		await expect(confirmationMessage).toHaveText(data.messageSended);
		const emailHTML = await mailHelper.readEmail(this.page,
            "qadashboard@redsauce.net",
            "qatesting.redsauce@gmail.com",
            "Reset password link"
        );
		const urlResetPassword = await mailHelper.parseBody(emailHTML)
		await this.page.goto(urlResetPassword);
		await expect(this.page).toHaveURL(/resetPassword/);
		const passwordInput = this.page.getByTestId("inputPassword");
		await passwordInput.fill(password);
		await expect(passwordInput).toHaveValue(password);
		await sendButton.click();
		await expect(this.page).toHaveURL(/changedPassword/);
		await sendButton.click();
	}
}