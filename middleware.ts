import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import { jwtVerify } from "jose";

export async function middleware(request: NextRequest) {
  const userCookie = request.cookies.get("userLoged");
  let logged = false;
  const excludedRoutes = ['/api/results/execution','/api/results/executionDev'];
  if (userCookie !== undefined) {
    const jwt = userCookie.value.toString().replace('"', "");
    const secret = new TextEncoder().encode(process.env.NEXT_PUBLIC_secret);
    let { payload } = await jwtVerify(jwt, secret);
    if (payload.token === process.env.NEXT_PUBLIC_token) logged = true;
  }
  if (request.nextUrl.pathname.includes("/api/results")&& !excludedRoutes.includes(request.nextUrl.pathname)) {
    if (!logged) {
      return new Response("Unauthorized", { status: 401 });
    }
  } else if (
    request.nextUrl.pathname.includes("/home") ||
    request.nextUrl.pathname.includes("/project") ||
    request.nextUrl.pathname.includes("/groups")
  ) {
    if (!logged) {
      return NextResponse.redirect(new URL("/", request.url));
    }
  } else if (request.nextUrl.pathname === "/") {
    if (logged) {
      return NextResponse.redirect(new URL("/home", request.url));
    }
  }
}
