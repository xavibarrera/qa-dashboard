set -x

apk update 
apk upgrade
apk add --no-cache openssh-client curl
curl -L https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 -o /usr/bin/cloudflared
chmod +x /usr/bin/cloudflared

mkdir -p ~/.ssh

echo "$SSH_CONFIG" > ~/.ssh/config
echo "$PRIVATE_KEY" > ~/.ssh/id_rsa
echo "$PUBLIC_KEY" > ~/.ssh/id_rsa.pub

chmod 0600 ~/.ssh/id_rsa