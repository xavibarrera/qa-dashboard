set -xe

HOST="jenkins@ssh-dashboard.redsauce.net"
ROOT="~/dashboard/prod"
LOCATION="$HOST:$ROOT"

ssh $HOST "cd $ROOT && git pull && bash ./gitlab-scripts/updateEnvFile.sh prod && sudo docker compose -f docker-compose.prod.yml down && sudo chown -R jenkins:jenkins ./mongodb_backup ./mongodb_data && sudo docker compose -f docker-compose.prod.yml build && sudo docker compose -f docker-compose.prod.yml up -d && sudo docker system prune -f"