set -xe

HOST="jenkins@ssh-dashboard.redsauce.net"
ROOT="~/dashboard/pre"
LOCATION="$HOST:$ROOT"

ssh $HOST "cd $ROOT && git pull && bash ./gitlab-scripts/updateEnvFile.sh testing && sudo docker compose -f docker-compose.testing.yml down && sudo chown -R jenkins:jenkins ./mongodb_backup ./mongodb_data && sudo docker compose -f docker-compose.testing.yml build && sudo docker compose -f docker-compose.testing.yml up -d && sudo docker system prune -f"