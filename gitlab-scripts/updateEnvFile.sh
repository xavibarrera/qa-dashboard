#!/bin/bash

updateEnvFile() {
  envFilePath="./.env.production"
  dockerComposeFilePath=$1

  urlProdValue=$(grep -oP "(?<=URL_PROD=).*" "$dockerComposeFilePath")

  if [[ -n $urlProdValue ]]; then
    sed -i "/NEXT_PUBLIC_url/c\NEXT_PUBLIC_url=$urlProdValue" "$envFilePath"
    echo "Archivo $envFilePath actualizado con éxito."
	echo "text $urlProdValue"
  else
    echo "No se encontró el valor de URL_PROD en el archivo $dockerComposeFilePath."
  fi
}

mode=$1

if [[ $mode == "prod" || $mode == "testing" ]]; then
  if [[ $mode == "prod" ]]; then
    updateEnvFile "./docker-compose.prod.yml"
  else
    updateEnvFile "./docker-compose.testing.yml"
  fi
else
  echo "Modo inválido. Debes especificar 'prod' o 'testing'."
fi
