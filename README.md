This is a [Next.js](https://nextjs.org/) project bootstrapped with ['create-next-app'](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).
## Ejecución
Para ejecutar el servidor local es necesario tener abierto 'docker desktop'
- 'npm run dev': Levanta docker con la BD y la web en localhost:3000 (entorno de desarollo)
- 'npm run build': Levanta docker y genera el directorio './build' necesario para ejecutar el entorno de produccion.
- 'npm run start': Levanta la web en (entorno de produccion)

## Docker
comando 'docker compose up -d' ejecuta el fichero 'docker-compose.yml' que monta dos contenedores:
- 'mongo_dev': Contiene la base de datos.
- 'mongo_seed': Copia el contenido de './dummyData' y añade los datos iniciales a 'mongo_dev'

## Estructura
- La carpeta './pages' contiene el fichero _app.tsx que se encarga de renderizar las páginas que componen nuestra web.
Cada fichero .tsx de este directorio es una ruta del proyecto accesible publicamente.
- Los componentes utilizados para construir las páginas se encuentran en la carpeta './components'.
- './locales/[lang]' contiene las traducciones para ese idioma de cada página.
- './testing/' contiene los test de componentes

## Testing
- 'npm run component' Ejecuta todos los test de componentes y genera un fichero .xml con los resultados usando junit.
- 'npm run e2e' Ejecuta todos los test de e2e y genera un fichero .xml con los resultados usando junit.
- 'npm run sendXML' Ejecuta el script que usara el cliente para añadir xml a un grupo,sendXML.mjs contiene el script, config.mjs pra modificar el path de los ficheros y el token, xmlVariables.mjs se usan a la hora de juntar los xml.

## Eslint
Se usa ESLint para revisar el código. Las reglas de configuración se encuentran en el archivo '.eslintrc.json'.
Se lanza ESLint con 'npm run lint'. Para resolver los problemas detectados se ejecuta 'npm run lint:fix' y se puede especificar un archivo o directorio en concreto.

## Credenciales
Estas credenciales permiten hacer login tanto en QADashboard como en el propio gmail.
- ##### Dashboard vacío
- 'qatesting.redsauce@gmail.com'
- 'Redsauce23!'

- qatesting.redsauce+venca@gmail.com
- pass: Venca2023!
