export default function createTestToken(body : any) {
    let randomstring = require("randomstring");
    for (let i of body.groups.length) {
        for (let j of body.groups[i].tests.length) {
            body.groups[i].tests[j].token = randomstring.generate();
        }
    }
}