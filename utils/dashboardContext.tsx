import { createContext } from "react";

let initialDashboard = [{
    _id: "",
    idUser: "",
    totalExecutions: 0,
    totalTest: 0,
    numFailedTests: 0,
    numPassedTests: 0,
    numOtherTests: 0,
    groups: []
}
]
export const DashboardContext = createContext<any>(initialDashboard);