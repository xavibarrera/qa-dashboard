//es idUser de results y idDashboard de users que conecta usuario y dashboard
export async function homeData(idDashboard : string) {
    try {        
        const res = await fetch(process.env.NEXT_PUBLIC_url + `/api/results/` + idDashboard, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        });
        const result = await res.json();
        return result;

    } catch (err) {
        console.log(err);
    }

} 
export async function homeCard(idDashboard : string) {
    try {        
        const res = await fetch(process.env.NEXT_PUBLIC_url + `/api/results/24h/` + idDashboard, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        });
        const result = await res.json();
        return result;

    } catch (err) {
        console.log(err);
    }

} 