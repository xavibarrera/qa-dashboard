import { XMLParser } from 'fast-xml-parser';
import * as fs from 'fs';
import * as xmlVariables from './xmlVariables.mjs';
import path from 'path';

function xmlToJson(xmlData: any) {
  const options = {
    ignoreAttributes: false,
    allowBooleanAttributes: true,
  };

  const parser = new XMLParser(options);
  let result = parser.parse(xmlData);
  return result;
}

function parseJUnitJSON(jsonData: any) {
  let testCases = [];
  let suites = jsonData.testsuites.testsuite;
  if (!Array.isArray(suites)) {
    suites = [suites];
  }

  let numFailedTests = 0;
  let numOtherTests = 0;
  let numTotalTests = 0;
  let numPassedTests = 0;
  let timestamp = 0;
  let execTime = 0;

  const testSuites = suites.map((suite: any, index: number) => {
    let numFailedTestsSuite = 0;
    let numOtherTestsSuite = 0;
    let numPassedTestsSuite = 0;

    if (index === 0) {
        timestamp = suite['@_timestamp'];
        execTime = parseFloat(suite['@_time']);
      }
    if (Array.isArray(suite.testcase)) {
      testCases = suite.testcase.map((testCase: any) => {
        let status;
        if (testCase.hasOwnProperty('failure')) {
          status = 'Failure';
          numFailedTestsSuite++;
        } else if (testCase.hasOwnProperty('error')) {
          status = 'Error';
          numFailedTestsSuite++;
        } else if (testCase.hasOwnProperty('skipped')) {
          status = 'Skipped';
          numOtherTestsSuite++;
        } else {
          status = 'Successful';
          numPassedTestsSuite++;
        }
        numTotalTests++;
        return { name: testCase['@_name'], execTime: parseFloat(testCase['@_time']), status };
      });
    } else {
        let status;
        if( suite.testcase.hasOwnProperty('skipped')){
            status = 'Skipped';
            numOtherTestsSuite++;
        } else if ( suite.testcase.hasOwnProperty('error') || suite.testcase.hasOwnProperty('failure')) {
            status = 'Error';
            numFailedTestsSuite++;
        } else {
            status = 'Successful';
            numPassedTestsSuite++;
        }
        numTotalTests++;
      testCases = [
        {
          name: suite.testcase['@_name'],
          execTime: parseFloat(suite.testcase['@_time']),
          status,
        },
      ];
    }
    numPassedTests += numPassedTestsSuite;
    numOtherTests += numOtherTestsSuite;
    numFailedTests += numFailedTestsSuite;
    return {
      name: suite['@_name'],
      numPassedTests: numPassedTestsSuite,
      numFailedTests: numFailedTestsSuite,
      numOtherTests: numOtherTestsSuite,
      numTotalTests: numTotalTests,
      execTime: suite['@_time'],
      testCases,
    };
  });

  return {
    name: jsonData.testsuites['@_name'] ? jsonData.testsuites['@_name'] : 'undefined',
    numFailedTests,
    numOtherTests,
    numTotalTests,
    numPassedTests,
    timestamp,
    execTime,
    testSuites,
  };
}

let header = xmlVariables.header;
let footer = xmlVariables.footer;

async function mergeXML(dirname: any) {
    let result = [header];
    let files = fs
      .readdirSync(dirname)
      .filter((el) => {
        return path.extname(el) === '.xml';
      });
  
    for await (let file of files) {
        let fileContent = fs.readFileSync(path.join(dirname, file), 'utf-8');
        let lines = fileContent.split('\n').map(line => line.replace(/^\s*-\s*/, ''));
        lines = lines.filter(line => line.trim() !== "" && !line.startsWith('-'));
        if (lines.length >= 3) {
            lines = lines.slice(2, -1);
        } else {
            lines = [];
        }
        result = result.concat(lines);
    }
    result.push(footer);
    return result.join('\n');
}

export { xmlToJson, parseJUnitJSON, mergeXML };
