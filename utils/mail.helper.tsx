import { expect } from "@playwright/test";
import { get_messages } from "gmail-tester";
import { resolve } from "path";

export const mailHelper = {
  async messageChecker(fromEmail: string, toEmail: string, subject: string) {
    const email = await get_messages(
      resolve(__dirname, "credentials.json"),
      resolve(__dirname, "token.json"),
      {
        from: fromEmail,
        to: toEmail,
        subject: subject,
        include_body: true,
        after: new Date(Date.now() - (1000 * 120)),
      }
    );
    return email;
  },

  async readEmail(
    page: any,
    senderEmail: string,
    receiverEmail: string,
    subject: string
  ): Promise<string> {
    let emails = await mailHelper.messageChecker(
      senderEmail,
      receiverEmail,
      subject
    );
    let startTime = Date.now();
    while (emails.length === 0 && Date.now() - startTime < 20000) {
      await page.waitForTimeout(5000);
      emails = await mailHelper.messageChecker(
        senderEmail,
        receiverEmail,
        subject
      );
    }
    expect(emails.length).toBeGreaterThanOrEqual(1); //ensure new mail arrived
    expect(emails[0].subject).toContain(subject.substr(0, 50)); //assert subject
    if (emails[0].body !== undefined) {
		return emails[0].body.html;
    } else throw new Error("readEmail Failed");
  },
  async parseBody(body: string): Promise<string> {
	body = body.replace(/ |<b>|<\/b>/g, "");
    return body;
  },
};
