import { NextRouter } from "next/router";

export default function handleLocaleChange(translation: string, router : NextRouter) {
    const value = translation;

    router.push(router.route, router.asPath, {
        locale: value,
    });
}