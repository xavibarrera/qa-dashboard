import {connect} from 'mongoose';

const conn = {
    isConnected: 0
}

export async function dbConnect() {
    if (conn.isConnected == 1) return;
	if(process.env.NEXT_PUBLIC_api_url !==undefined){
		const db = await connect(process.env.NEXT_PUBLIC_api_url);
		conn.isConnected = db.connections[0].readyState;
	}
}