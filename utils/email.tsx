export default async function sendEmail(email : string, link : string) {
  let nodemailer = require('nodemailer');
  try {
    const transporter = nodemailer.createTransport({
      port: 465,
      host: "smtp.ionos.es",
      auth: {
        user: 'qadashboard@redsauce.net',
        pass: 'dcN7338L89Pwspt'
      },
      secure: true,
    })

    const mailData = {
      from: 'qadashboard@redsauce.net',
      to: email,
      subject: `Reset password link`,
      text: link,
      html: `<b>${link}</b>`
    }

    await transporter.sendMail(mailData);

  } catch (err: any) {
    console.log("Error to send email")
  }
}