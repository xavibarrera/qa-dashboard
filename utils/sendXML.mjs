import { url, token, pathFiles } from "./config.mjs";
import * as fs from "fs";
import * as xmlVariables from "./xmlVariables.mjs";
import * as path from "path";
import fetch from "node-fetch";

let header = xmlVariables.header;
let footer = xmlVariables.footer;

async function mergeXML(dirname) {
  let result = [header];
  let files = fs.readdirSync(dirname).filter((el) => {
    return path.extname(el) === ".xml";
  });

  for await (let file of files) {
    let fileContent = fs.readFileSync(path.join(dirname, file), "utf-8");
    let lines = fileContent
      .split("\n")
      .map((line) => line.replace(/^\s*-\s*/, ""));
    lines = lines.filter((line) => line.trim() !== "" && !line.startsWith("-"));
    if (lines.length >= 3) {
      lines = lines.slice(2, -1);
    } else {
      lines = [];
    }
    result = result.concat(lines);
  }
  result.push(footer);
  return result.join("\n");
}

async function sendPostRequest() {
  try {
	const xmlContent = await mergeXML(pathFiles);
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/xml",
        token: token,
      },
      body: xmlContent,
    });

    if (response.ok) {
      const responseData = await response.json();
      console.log("Respuesta del servidor:", responseData);
    } else {
      throw new Error("Error en la solicitud");
    }
  } catch (error) {
    console.error("Error al enviar la solicitud:", error.message);
  }
}


sendPostRequest();
