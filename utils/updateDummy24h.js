const fs = require('fs');

// Ruta del archivo
const archivo = './dummyData/testgroups.json';

// Nuevo contenido de la línea 60
const nuevoContenido = '				  "$date": ' + '"'+new Date(Date.now()).toISOString()+'"';

// Leer el archivo
fs.readFile(archivo, 'utf8', (error, data) => {
  if (error) {
    console.error('Error al leer el archivo:', error);
    return;
  }

  // Dividir el contenido en líneas
  const lineas = data.split('\n');

  // Reemplazar la línea 60 con el nuevo contenido
  lineas[72] = nuevoContenido;

  // Unir las líneas de nuevo en un solo texto
  const nuevoArchivo = lineas.join('\n');

  // Escribir el archivo modificado
  fs.writeFile(archivo, nuevoArchivo, 'utf8', (error) => {
    if (error) {
      console.error('Error al escribir el archivo:', error);
      return;
    }

    console.log('Updated now');
  });
});