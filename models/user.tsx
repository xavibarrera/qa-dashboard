import { Schema, model, models } from 'mongoose';

interface IUser {
    email: string
    password: string
    idDashboard: string
    username: string
}

const userSchema = new Schema<IUser>({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    idDashboard: {
        type: String,
        required: false
    },
    username: {
        type: String,
        required: false
    }
}, {
    timestamps: true,
    versionKey: false
});

export default models.User || model<IUser>('User', userSchema);