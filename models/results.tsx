import { Schema, model, models, Document } from 'mongoose';

interface ITestCase {
    name: { type: string, required: true };
    execTime: { type: number, required: true };
    status: { type: string, required: true };
}

interface ITestSuite {
    name: { type: string, required: true };
    execTime: { type: number, required: true };
    numFailedTests: { type: number, required: true };
    numPassedTests: { type: number, required: true };
    numOtherTests: { type: number, required: true };
    testCases: ITestCase[];
}

interface ITestExecution {
    executionNum: { type: number, required: true },
    timestamp: { type: string, required: true };
    execTime: { type: number, required: true };
    numTotalTests: { type: number, required: true };
    numFailedTests: { type: number, required: true };
    numPassedTests: { type: number, required: true };
    numOtherTests: { type: number, required: true };
    testSuites: ITestSuite[];
}

interface ITest {
    token: { type: string, required: true };
    name: { type: string, required: true };
    numTotalTests: { type: number, required: true };
    numFailedTests: { type: number, required: true };
    numPassedTests: { type: number, required: true };
    numOtherTests: { type: number, required: true };
    selected: { type: boolean, required: true};
    executions: ITestExecution[];
}
interface IGroup {
    name: { type: string, required: true };
    numTotalTests: { type: number, required: true };
    numFailedTests: { type: number, required: true };
    numPassedTests: { type: number, required: true };
    numOtherTests: { type: number, required: true };
    tests: ITest[];
}
interface ITestGroup extends Document {
    idUser: { type: string, required: true };
	totalExecutions: { type: number, required: true };
	totalTest: { type: number, required: true };
    numFailedTests: { type: number, required: true };
    numPassedTests: { type: number, required: true };
    numOtherTests: { type: number, required: true };
    groups: IGroup[];
	
}

const testCaseSchema = new Schema<ITestCase>({
    name: { type: String, required: true },
    execTime: { type: Number, required: true },
    status: { type: String, required: true },
});

const testSuiteSchema = new Schema<ITestSuite>({
    name: { type: String, required: true },
    execTime: { type: Number, required: true },
    numFailedTests: { type: Number, required: true },
    numPassedTests: { type: Number, required: true },
    numOtherTests: { type: Number, required: true },
    testCases: [testCaseSchema],
});

const executionSchema = new Schema<ITestExecution>({
    executionNum: { type: Number, required: true },
    timestamp: { type: String, required: true },
    execTime: { type: Number, required: true },
    numTotalTests: { type: Number, required: true },
    numFailedTests: { type: Number, required: true },
    numPassedTests: { type: Number, required: true },
    numOtherTests: { type: Number, required: true },
    testSuites: [testSuiteSchema],
},{
    timestamps: true,
    versionKey: false
});

const testsSchema = new Schema<ITest>({
    token: { type: String, required: true },
    name: { type: String, required: true },
    numTotalTests: { type: Number, required: true },
    numFailedTests: { type: Number, required: true },
    numPassedTests: { type: Number, required: true },
    numOtherTests: { type: Number, required: true },
    selected: { type: Boolean, required: true },
    executions: [executionSchema],
});

const groupSchema = new Schema<IGroup>({
    name: { type: String, required: true },
    numTotalTests: { type: Number, required: true },
    numFailedTests: { type: Number, required: true },
    numPassedTests: { type: Number, required: true },
    numOtherTests: { type: Number, required: true },
    tests: { type: [testsSchema], required: true },
});

const testGroupSchema = new Schema<ITestGroup>({
    idUser: { type: String, required: true },
	totalExecutions: { type: Number, required: true },
	totalTest: { type: Number, required: true },
    numFailedTests: { type: Number, required: true },
    numPassedTests: { type: Number, required: true },
    numOtherTests: { type: Number, required: true },
    groups: [groupSchema],
}/*, { _id: false }*/);

const results = models.TestGroup || model<ITestGroup>('TestGroup', testGroupSchema);
export default results;