/* eslint-disable @typescript-eslint/no-empty-function */
import { test, expect } from "@playwright/experimental-ct-react";
import { InputEmail } from "./inputEmail";

test.describe("Input email validation with default values", () => {
  let component: any;
  test.beforeEach(async ({ mount }) => {
    component = await mount(
      <InputEmail setEmailData={() => {}} setEmailValidated={() => {}} />
    );
  });
  test("Placeholder correctly displayed", async () => {
    const emailInput = component.getByTestId("inputEmail");
    const placeholderText = await emailInput.getAttribute("placeholder");
    expect(placeholderText).toBe("Email");
  });
  test("Insert text works", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill("prueba");
    await expect(emailInput).toHaveValue("prueba");
  });

  test("Empty email error message", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill(" ");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).toHaveText("Enter your email address");
  });

  test("Wrong email message by missing @", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill("pepgmail.com");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).toHaveText("Invalid email");
  });

  test("Wrong email message by missing .", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill("prueba@gmailcom");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).toHaveText("Invalid email");
  });

  test("A valid email does not show error message", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill("prueba@gmail.com");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).not.toBeVisible();
  });
});

test.describe("Input email validation with custom values", () => {
  let component: any;
  test.beforeEach(async ({ mount }) => {
    component = await mount(
      <InputEmail
        setEmailData={() => {}}
        setEmailValidated={() => {}}
        emptyEmailMessage="Introduce tu email"
        wrongEmailMessage="Email no válido"
      />
    );
  });
  test("Placeholder correctly displayed", async () => {
    const emailInput = component.getByTestId("inputEmail");
    const placeholderText = await emailInput.getAttribute("placeholder");
    expect(placeholderText).toBe("Email");
  });
  test("Empty email error message", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill(" ");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).toHaveText("Introduce tu email");
  });

  test("Wrong email message by missing @", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill("pruebagmail.com");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).toHaveText("Email no válido");
  });

  test("Wrong email message by missing .", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill("prueba@gmailcom");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).toHaveText("Email no válido");
  });
  test("A valid email does not show error message", async () => {
    const emailInput = component.getByTestId("inputEmail");
    await emailInput.fill("prueba@gmail.com");
    await emailInput.blur();
    const errorMessage = component.locator(".tremor-TextInput-errorMessage");
    await expect(errorMessage).not.toBeVisible();
  });
});
test.describe("Check custom functions", () => {
  test.describe("With default values", () => {
    let component: any;
    let onEnter: boolean;
    let setEmailData: boolean;
    let setEmailValidated: boolean;
    test.beforeEach(async ({ mount }) => {
      onEnter = false;
      setEmailData = false;
      setEmailValidated = false;
      component = await mount(
        <InputEmail
          onEnter={async () => {
            onEnter = true;
          }}
          setEmailData={async () => {
            setEmailData = true;
          }}
          setEmailValidated={async () => {
            setEmailValidated = true;
          }}
        />
      );
    });
    test("Test Function onEnter", async () => {
      expect(onEnter).toBe(false);
      const EmailInput = component.getByTestId("inputEmail");
      await EmailInput.press("Enter");
      expect(onEnter).toBe(true);
    });
    test("Test Function setEmailData", async () => {
      expect(setEmailData).toBe(false);
      const EmailInput = component.getByTestId("inputEmail");
      await EmailInput.fill("P");
      expect(setEmailData).toBe(true);
    });
    test("Test Function setEmailValidated", async () => {
      let valid_email = process.env.EMAIL;
      expect(setEmailValidated).toBe(false);
      const EmailInput = component.getByTestId("inputEmail");
      await EmailInput.fill(valid_email);
      expect(setEmailValidated).toBe(true);
    });
  });
});
