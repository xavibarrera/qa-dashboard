/* eslint-disable @typescript-eslint/no-empty-function */
import { test, expect } from "@playwright/experimental-ct-react";
import { InputPassword } from "./inputPassword";

test.describe("Input Password validation with default values", () => {
  test.describe("Details = false", () => {
    let component: any;
    test.beforeEach(async ({ mount }) => {
      component = await mount(
        <InputPassword
          setPasswordData={() => {}}
          setPasswordValidated={() => {}}
        />
      );
    });

    test("Placeholder correctly displayed", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      const placeholderText = await passwordInput.getAttribute("placeholder");
      expect(placeholderText).toBe("Password");
    });
    test("Insert text works", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("Prueba");
      await expect(passwordInput).toHaveValue("Prueba");
    });
    test("Hide password works", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("Prueba");
      await expect(component).toHaveScreenshot("encryptedPassword.png");
      const switchPasswordVisibility = component.getByTestId(
        "btnSwitchPasswordVisibility"
      );
      await switchPasswordVisibility.click();
      await expect(component).toHaveScreenshot("decryptedPassword.png");
    });
    test("Empty password error message", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("");
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText("Enter the password");
    });
    test("Wrong password error message no details", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.PASSWORD);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText("Invalid password");
    });
  });
  test.describe("Details = true", () => {
    let component: any;
    test.beforeEach(async ({ mount }) => {
      component = await mount(
        <InputPassword
          setPasswordData={() => {}}
          setPasswordValidated={() => {}}
          details={true}
        />
      );
    });

    test("Wrong password error message by missing special characters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.PASSWORD);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "It must be at least 8 characters long, and include a capital letter, a number and a symbol."
      );
    });
    test("Wrong password error message by missing uppercase letters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_LOWER);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "It must be at least 8 characters long, and include a capital letter, a number and a symbol."
      );
    });
    test("Wrong password error message by missing lowercase letters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_UPPER);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "It must be at least 8 characters long, and include a capital letter, a number and a symbol."
      );
    });
    test("Wrong password error message by missing numbers", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_NUM);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "It must be at least 8 characters long, and include a capital letter, a number and a symbol."
      );
    });
    test("Wrong password error message by not reach 8 characters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_SHORT);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "It must be at least 8 characters long, and include a capital letter, a number and a symbol."
      );
    });
    test("A valid email does not show error message", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.PASSWORD);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).not.toBeVisible();
    });
  });
});

test.describe("Input Password validation with custom values", () => {
  test.describe("Details = false", () => {
    let component: any;
    test.beforeEach(async ({ mount }) => {
      component = await mount(
        <InputPassword
          emptyPassMessage="Introduce tu contraseña"
          invalidPassword="Contraseña inválida"
          invalidPasswordExplained="Debe tener al menos 8 caracteres, e incluir una mayúscula, un número y un símbolo."
          placeholder="Contraseña"
          setPasswordData={() => {}}
          setPasswordValidated={() => {}}
        />
      );
    });
    test("Insert text works", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("Prueba");
      await expect(passwordInput).toHaveValue("Prueba");
    });
    test("Hide password works", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("Prueba");
      await expect(component).toHaveScreenshot("encryptedPasswordCustom.png");
      const switchPasswordVisibility = component.getByTestId(
        "btnSwitchPasswordVisibility"
      );
      await switchPasswordVisibility.click();
      await expect(component).toHaveScreenshot("decryptedPasswordCustom.png");
    });
    test("Empty password error message", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("");
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText("Introduce tu contraseña");
    });
    test("Wrong password error message no details", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_SIGN);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText("Contraseña inválida");
    });
    test("Placeholder correctly displayed", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      const placeholderText = await passwordInput.getAttribute("placeholder");
      expect(placeholderText).toBe("Contraseña");
    });
  });
  test.describe("Details = true", () => {
    let component: any;
    test.beforeEach(async ({ mount }) => {
      component = await mount(
        <InputPassword
          emptyPassMessage="Introduce tu contraseña"
          invalidPassword="Contraseña inválida"
          invalidPasswordExplained="Debe tener al menos 8 caracteres, e incluir una mayúscula, un número y un símbolo."
          placeholder="Contraseña"
          setPasswordData={() => {}}
          setPasswordValidated={() => {}}
          details={true}
        />
      );
    });
    test("Wrong password error message by missing special characters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_SIGN);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "Debe tener al menos 8 caracteres, e incluir una mayúscula, un número y un símbolo."
      );
    });
    test("Wrong password error message by missing uppercase letters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_LOWER);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "Debe tener al menos 8 caracteres, e incluir una mayúscula, un número y un símbolo."
      );
    });
    test("Wrong password error message by missing lowercase letters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_UPPER);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "Debe tener al menos 8 caracteres, e incluir una mayúscula, un número y un símbolo."
      );
    });
    test("Wrong password error message by missing numbers", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS_NUM);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "Debe tener al menos 8 caracteres, e incluir una mayúscula, un número y un símbolo."
      );
    });
    test("Wrong password error message by not reach 8 characters", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("Re1!");
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).toHaveText(
        "Debe tener al menos 8 caracteres, e incluir una mayúscula, un número y un símbolo."
      );
    });
    test("A valid email does not show error message", async () => {
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.PASSWORD);
      await passwordInput.blur();
      const errorMessage = component.locator(".tremor-TextInput-errorMessage");
      await expect(errorMessage).not.toBeVisible();
    });
  });
});

test.describe("Check custom functions", () => {
  test.describe("With default values", () => {
    let component: any;
    let onEnter: boolean;
    let setPasswordData: boolean;
    let setPasswordValidated: boolean;
    test.beforeEach(async ({ mount }) => {
      onEnter = false;
      setPasswordData = false;
      setPasswordValidated = false;
      component = await mount(
        <InputPassword
          onEnter={async () => {
            onEnter = true;
          }}
          setPasswordData={async () => {
            setPasswordData = true;
          }}
          setPasswordValidated={async () => {
            setPasswordValidated = true;
          }}
        />
      );
    });
    test("Test Function onEnter", async () => {
      expect(onEnter).toBe(false);
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.press("Enter");
      expect(onEnter).toBe(true);
    });
    test("Test Function setPasswordData", async () => {
      expect(setPasswordData).toBe(false);
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill("P");
      expect(setPasswordData).toBe(true);
    });
    test("Test Function setPasswordValidated", async () => {
      expect(setPasswordValidated).toBe(false);
      const passwordInput = component.getByTestId("inputPassword");
      await passwordInput.fill(process.env.WRONG_PASS);
      expect(setPasswordValidated).toBe(true);
    });
  });
});
