import { useState, useContext } from "react";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import { useRouter } from "next/router";
import { logOut } from "../../sidebar/eventHandlers";
import { UserContext } from "@/utils/userContext";
import { DashboardContext } from "@/utils/dashboardContext";

export function OptionsDropdown () {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  const { setUserData } = useContext(UserContext);
  let { setDashboardData } = useContext(DashboardContext);
  const options = ["Account", "Logout"]

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const goToAccountPage = () => {
    router.push("/account")
  }

  const chooseOption = (option:any) => {
    if (option === "Account") {
      goToAccountPage();
    } else if (option === "Logout") {
      logOut(setUserData, setDashboardData, router)
    }    
    setIsOpen(false);
  };

  return (
    <div className="relative inline-block text-left">
      <AccountBoxIcon className="w-6 h-6" onClick={toggleDropdown}/>
      {isOpen && (
        <div className="origin-bottom-right absolute right-0 mt-2 w-36 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 z-50">
          <div className="py-1">
            {options.map((option:any, index:any) => (
              <button
                key={index}
                onClick={() => chooseOption(option)}
                className="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
              >
                {option}
              </button>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}
