import { useState } from "react";
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import { useRouter } from "next/router";
import handleLocaleChange from "utils/handleLocaleChange";

export function LanguagesDropdown() {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  const [selectedOption, setSelectedOption] = useState("ES");
  const options = ["ES", "EN"];

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const chooseOption = (option: any) => {
    if (options.includes(option)) {
      handleLocaleChange(option.toLowerCase(), router);
    }
    setSelectedOption(option);
    setIsOpen(false);
  };

  return (
    <div className="relative inline-flex text-left">
      {isOpen ? <ArrowDropDownIcon onClick={toggleDropdown}/> : <ArrowDropUpIcon onClick={toggleDropdown}/>}
      {selectedOption}
      {isOpen && (
        <div className=" absolute right-0 mt-8 w-36 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 z-50">
          <div className="py-1">
            {options.map((option, index) => (
              <button
                key={index}
                onClick={() => chooseOption(option)}
                className={`block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 ${
                  option === selectedOption ? "bg-gray-200" : ""
                }`}
              >
                {option}
              </button>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}
