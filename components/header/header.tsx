import { Grid, Col } from "@tremor/react";
import { useState } from "react";
import Breadcrumbs from "../breadcrumbs";
import { OptionsDropdown } from "./dropdowns/options";
import { LanguagesDropdown } from "./dropdowns/languages";
const Header = (props: any) => {
  const [isOpen, setIsOpen] = useState(false);

  const breadcrumbs = props.breadCrumbsData || [{ label: "Error", path: "/" }];

  return (
    <header>
      <Grid numCols={5} className="m-6">
        <Col className="flex justify-start" numColSpan={1}>
          <Breadcrumbs items={breadcrumbs} />
        </Col>
        <Col className="flex justify-end gap-2" numColSpan={4}>
          <div className="relative inline-block mr-6">
            <button
              onClick={() => setIsOpen(!isOpen)}
              type="button"
              className="text-gray-600 focus:outline-none"
            >
              <LanguagesDropdown/>
            </button>
          </div>
          <div className="relative inline-block">
            <button
              onClick={() => setIsOpen(!isOpen)}
              type="button"
              className="text-gray-600 focus:outline-none mr-6"
            >
              <OptionsDropdown/>
            </button>
          </div>
        </Col>
      </Grid>
    </header>
  );
};
export default Header;