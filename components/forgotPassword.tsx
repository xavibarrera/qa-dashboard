import { Text, Button, Metric, Subtitle } from "@tremor/react";
import { useTranslation } from "next-i18next";
import { useState } from "react";
import { useRouter } from "next/router";
import { InputEmail } from "../components/inputEmail";

interface ForgotPasswordData {
  email: string;
  locale: string | undefined;
}

export default function ForgotPassword() {
  const [messageEmail, setMessageEmail] = useState("");
  const [errorEmail, setErrorEmail] = useState(false);
  const [emailSended, setEmailSended] = useState(false);
  const { t } = useTranslation("forgotPassword");
  const router = useRouter();
  const [emailData, setEmailData] = useState("");
  const [emailValidated, setEmailValidated] = useState(false);

  function hideError() {
    setMessageEmail("");
    setErrorEmail(false);
  }
  async function handleForgotPassword(data: ForgotPasswordData) {
    if (emailValidated) {
      try {
        await fetch(process.env.NEXT_PUBLIC_url +`/api/users/auth/forgotPassword`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
                });

        setEmailSended(true);
      } catch (err) {
        const message = t("serverError");
        setMessageEmail(message);
        setErrorEmail(true);
        setTimeout(hideError, 2000);
      }
    }
  }
  return (
    <div className="flex h-screen justify-center items-center">
      <div className="text-center ">
        <Metric>{t("title")}</Metric>
        <Subtitle className="mt-4">{t("subtitle")}</Subtitle>
        <div className="mt-10 ">
          <InputEmail
            value={emailData}
            setEmailData={setEmailData}
            setEmailValidated={setEmailValidated}
            onEnter={() => {
              handleForgotPassword({
                email: emailData,
                locale: router.locale,
              });
            }}
          />
        </div>
        {errorEmail && <Text className="text-red-500">{messageEmail}</Text>}
        {emailSended && (
          <div data-testid="confirmation">
            <Text className="mt-4"> {t("emailSended")}</Text>
          </div>
        )}
        <Button
          variant="primary"
          color="orange"
          className="h-8 w-28 mt-10"
          data-testid="btnLogin"
          disabled={!emailValidated}
          onClick={() =>
            handleForgotPassword({
              email: emailData,
              locale: router.locale,
            })
          }
        >
          {t("button")}
        </Button>
      </div>
    </div>
  );
}
