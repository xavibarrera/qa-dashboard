import { ReactNode } from "react";
import Link from "next/link";
export interface CrumbItem {
  label: ReactNode;
  path: string; // e.g., /development/programming-languages/python
}
export interface BreadcrumbsProps {
  items: CrumbItem[];
}

const Breadcrumbs = ({ items }: BreadcrumbsProps) => {
  return (
    <>
      {items.map((crumb, i) => {
        const isLastItem = i === items.length - 1;
        if (!isLastItem) {
          return (
            <div key={crumb.path}>
              <Link
                href={crumb.path}
                className="text-gray-500 hover:text-orange-400"
              >
                <strong>{crumb.label}</strong>
              </Link>
              <span>{"\u00A0>\u00A0"}</span>
            </div>
          );
        } else {
          return (
            <h1 className="text-orange-400" key={crumb.path}>
              <strong>{crumb.label}</strong>
            </h1>
          );
        }
      })}
    </>
  );
};
export default Breadcrumbs;
