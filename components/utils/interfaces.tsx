export interface Data {
    groups?: Group[];
    idUser?: string;
    numFailedTests?: number;
    numOtherTests?: number;
    numPassedTests?: number;
    totalExecutions?: number;
    totalTest?: number;
    __v?: number;
    _id?: string;
}

export interface Group {
    tests?: Test[];
    name?: string;
    timestamp?: string;
    numFailedTests?: number;
    numOtherTests?: number;
    numPassedTests?: number;
    numTotalTests?: number;
    _id?: string;
}

export interface Test {
    executions?: Execution[];
    name?: string;
    numFailedTests?: number;
    numOtherTests?: number;
    numPassedTests?: number;
    numTotalTests?: number;
    selected?: boolean;
    token?: string;
    _id?: string;
}

export interface Execution {
    groupName?: string;
    testName?: string;
    executionNum?: number;
    execTime?: number;
    numFailedTests?: number;
    numOtherTests?: number;
    numPassedTests?: number;
    numTotalTests?: number;
    testSuites?: TestSuite[];
    timestamp?: string;
    _id?: string;
}

export interface TestSuite {
    execTime?: number;
    name?: string;
    numFailedTests?: number;
    numOtherTests?: number;
    numPassedTests?: number;
    testCases?: TestCase[];
    _id?: string;
}

export interface TestCase {
    execTime?: number;
    name?: string;
    status?: string;
    _id?: string;
}
export interface Header {
    totalExecutions?: number;
    totalTests?: number;
    totalPassed?: number;
    totalFailed?: number;
    totalOther?: number;
    timestamp?: string;
}
export interface MultipleExecutions {
    executions?: Execution[];
    numTotalExecutions?: number;
    numFailedTests?: number;
    totalTests?: number;
}

export interface DeleteModalProps {
    isOpen: boolean;
    closeModal: () => void;
    handleDeleteGroup: (groupId: string) => void;
    handleDeleteTest?: (groupId: string, testId: string) => void;
    groupId: string;
    testId?: string;
}

export let emptyExecution = {
    groupName: "",
    testName: "",
    executionNum: 0,
    timestamp: "",
    numFailedTests: 0,
    numPassedTests: 0,
    numOtherTests: 0,
    execTime: 0,
    testSuites: []
}

export let emptyExecutions = {
    numTotalExecutions: 0,
    numFailedTests: 0,
    totalTests: 0,
    executions: []
}

export let emptyTest = {
    executions: [],
    name: "Empty",
    numFailedTests: 0,
    numOtherTests: 0,
    numPassedTests: 0,
    numTotalTests: 0,
    selected: true,
    token: "asdjfnjnj2nj4nj234njwe",
    _id: "2123r23f245634erfg"
}