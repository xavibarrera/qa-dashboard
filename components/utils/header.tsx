import { Test, Header, Execution } from "./interfaces";

export function getHeaderInfo(dataArray: Array<Test>) {
    if (!Array.isArray(dataArray)) {
      dataArray = [dataArray];
    }
  
    let totalExecutions = 0;
    let totalTests = 0;
    let totalPassed = 0;
    let totalFailed = 0;
    let totalOther = 0;
  
    dataArray.forEach((item: Test) => {
      if (item.executions) {
        totalExecutions += item.executions.length;
        item.executions.forEach((dataItem: Execution) => {
          totalTests += (dataItem.numFailedTests || 0) + (dataItem.numPassedTests || 0) + (dataItem.numOtherTests || 0);
          totalPassed += dataItem.numPassedTests || 0;
          totalFailed += dataItem.numFailedTests || 0;
          totalOther += dataItem.numOtherTests || 0;
        });
      }
    });
  
    const resumen: Header = {
      totalExecutions,
      totalTests,
      totalPassed,
      totalFailed,
      totalOther,
    };
    return resumen;
}