export const data = [
    {
        name: "A",
        numFailedTests: 33,
        numOtherTests: 7,
        numPassedTests: 75,
        numTotalTests: 115,
        _id: "2354i354bhi234ui432uhi6huif",
        tests: [
            {
                name: "Con fallos",
                numFailedTests: 4,
                numOtherTests: 2,
                numPassedTests: 30,
                numTotalTests: 42,
                selected: false,
                token: "efjoisdaa3gj34",
                _id: "234rf349694cfd8579847f9f6",
                executions: [
                  {
                    executionNum: 1,
                    execTime: 80,
                    numFailedTests: 4,
                    numOtherTests: 0,
                    numPassedTests: 10,
                    numTotalTests: 14,
                    testSuites: [] as any,
                    timestamp: "2023-06-27T09:14:23",
                    _id: "ef253544f4cfd8579847f9f6",
                  },
                  {
                    executionNum: 2,
                    execTime: 70,
                    numFailedTests: 0,
                    numOtherTests: 0,
                    numPassedTests: 20,
                    numTotalTests: 28,
                    testSuites: [] as any,
                    timestamp: "2023-06-27T09:14:23",
                    _id: "ef253544f4cfd8579847f9f6",
                  }
                ],
            },
            {
                name: "Sin fallos",
                numFailedTests: 10,
                numOtherTests: 2,
                numPassedTests: 30,
                numTotalTests: 42,
                selected: true,
                token: "sdferg3546dg546",
                _id: "25gfbtdf",
                executions: [
                  {
                    executionNum: 1,
                    execTime: 80,
                    numFailedTests: 0,
                    numOtherTests: 0,
                    numPassedTests: 10,
                    numTotalTests: 14,
                    testSuites: [] as any,
                    timestamp: "2023-06-27T09:14:23",
                    _id: "89gh6ghcf57",
                  },
                  {
                    executionNum: 2,
                    execTime: 70,
                    numFailedTests: 0,
                    numOtherTests: 0,
                    numPassedTests: 20,
                    numTotalTests: 28,
                    testSuites: [] as any,
                    timestamp: "2023-06-27T09:14:23",
                    _id: "12dv34dc5fgg",
                  }
                ],
              }
        ]
    },
    {
        name: "B",
        numFailedTests: 29,
        numOtherTests: 7,
        numPassedTests: 45,
        numTotalTests: 81,
        _id: "33254hb3vi4534fui3y",
        tests: [
          {
            name: "B1",
            numFailedTests: 20,
            numOtherTests: 5,
            numPassedTests: 25,
            numTotalTests: 50,
            selected: false,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 0,
                numOtherTests: 0,
                numPassedTests: 0,
                numTotalTests: 0,
                testSuites: [] as any,
                timestamp: "2023-06-27T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 20,
                numOtherTests: 5,
                numPassedTests: 25,
                numTotalTests: 50,
                testSuites: [] as any,
                timestamp: "2023-06-27T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              }
            ],
          },
          {
            name: "last execution",
            numFailedTests: 9,
            numOtherTests: 2,
            numPassedTests: 20,
            numTotalTests: 31,
            selected: false,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 4,
                numOtherTests: 0,
                numPassedTests: 10,
                numTotalTests: 14,
                testSuites: [] as any,
                timestamp: "2023-06-27T09:14:24",
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 5,
                numOtherTests: 2,
                numPassedTests: 10,
                numTotalTests: 17,
                testSuites: [] as any,
                timestamp: "2023-06-27T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              }
            ],
          }
        ]
    }  
]

export const empty_data = [
    {
      name: "A",
      numFailedTests: 33,
      numOtherTests: 7,
      numPassedTests: 75,
      numTotalTests: 115,
      _id: "2354i354bhi234ui432uhi6huif",
      tests: [
        {
          name: "A1",
          numFailedTests: 4,
          numOtherTests: 0,
          numPassedTests: 30,
          numTotalTests: 34,
          selected: true,
          token: "efjoisdaa3gj34",
          _id: "234rf349694cfd8579847f9f6",
          executions: [],
        }
      ]
    },
    {
      name: "B",
      numFailedTests: 29,
      numOtherTests: 7,
      numPassedTests: 45,
      numTotalTests: 81,
      _id: "33254hb3vi4534fui3y",
      tests: [
        {
          name: "B1",
          numFailedTests: 20,
          numOtherTests: 5,
          numPassedTests: 25,
          numTotalTests: 50,
          selected: true,
          token: "efjoisdaa3gj34",
          _id: "234rf349694cfd8579847f9f6",
          executions: [],
        },
        {
          name: "B2",
          numFailedTests: 9,
          numOtherTests: 2,
          numPassedTests: 20,
          numTotalTests: 31,
          selected: true,
          token: "efjoisdaa3gj34",
          _id: "234rf349694cfd8579847f9f6",
          executions: [],
        }
      ]
    }
]

export const non_selected = [
    {
      name: "A",
      numFailedTests: 33,
      numOtherTests: 7,
      numPassedTests: 75,
      numTotalTests: 115,
      _id: "2354i354bhi234ui432uhi6huif",
      tests: [
        {
          name: "A1",
          numFailedTests: 4,
          numOtherTests: 0,
          numPassedTests: 30,
          numTotalTests: 34,
          selected: false,
          token: "efjoisdaa3gj34",
          _id: "234rf349694cfd8579847f9f6",
          executions: [
            {
              execTime: 80,
              numFailedTests: 4,
              numOtherTests: 0,
              numPassedTests: 10,
              numTotalTests: 14,
              testSuites: [] as any,
              timestamp: "2023-06-26T09:14:23",
              _id: "ef253544f4cfd8579847f9f6",
            },
            {
              execTime: 70,
              numFailedTests: 0,
              numOtherTests: 0,
              numPassedTests: 20,
              numTotalTests: 20,
              testSuites: [] as any,
              timestamp: "2023-06-26T09:14:23",
              _id: "2343546546756756823",
            }
          ],
        }
      ]
    },
    {
      name: "B",
      numFailedTests: 29,
      numOtherTests: 7,
      numPassedTests: 45,
      numTotalTests: 81,
      _id: "33254hb3vi4534fui3y",
      tests: [
        {
          name: "B1",
          numFailedTests: 20,
          numOtherTests: 5,
          numPassedTests: 25,
          numTotalTests: 50,
          selected: false,
          token: "efjoisdaa3gj34",
          _id: "234rf349694cfd8579847f9f6",
          executions: [
            {
              execTime: 80,
              numFailedTests: 0,
              numOtherTests: 0,
              numPassedTests: 0,
              numTotalTests: 0,
              testSuites: [] as any,
              timestamp: "2022-06-26T09:14:23",
              _id: "ef253544f4cfd8579847f9f6",
            },
            {
              execTime: 70,
              numFailedTests: 20,
              numOtherTests: 5,
              numPassedTests: 25,
              numTotalTests: 50,
              testSuites: [] as any,
              timestamp: "2022-06-26T09:14:23",
              _id: "ef253544f4cfd8579847f9f6",
            }
          ],
        },
        {
          name: "B2",
          numFailedTests: 9,
          numOtherTests: 2,
          numPassedTests: 20,
          numTotalTests: 31,
          selected: false,
          token: "efjoisdaa3gj34",
          _id: "234rf349694cfd8579847f9f6",
          executions: [
            {
              execTime: 80,
              numFailedTests: 4,
              numOtherTests: 0,
              numPassedTests: 10,
              numTotalTests: 14,
              testSuites: [] as any,
              timestamp: "2022-06-26T09:14:23",
              _id: "798fgh578fgh578d",
            },
            {
              execTime: 70,
              numFailedTests: 5,
              numOtherTests: 2,
              numPassedTests: 10,
              numTotalTests: 17,
              testSuites: [] as any,
              timestamp: "2022-06-26T09:14:23",
              _id: "2323232325359876543244f4c",
            }
          ],
        }
      ]
    }
]

const currentDate = new Date();
const previousWeekDate = new Date(currentDate);
previousWeekDate.setDate(currentDate.getDate() - 5);

export const updated_week_data = {
    idUser: "53freerger4h5yu4eswfgrt",
    numFailedTests: 100,
    numOtherTests: 8,
    numPassedTests: 5,
    totalExecutions: 12,
    __v: 1234567809876543,
    _id: "34tu9isdvnj23r3rsddddd",
    groups: [
      {
        name: "A",
        numFailedTests: 33,
        numOtherTests: 7,
        numPassedTests: 75,
        numTotalTests: 115,
        _id: "2354i354bhi234ui432uhi6huif",
        tests: [
          {
            name: "A1",
            numFailedTests: 4,
            numOtherTests: 0,
            numPassedTests: 30,
            numTotalTests: 34,
            selected: true,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 4,
                numOtherTests: 0,
                numPassedTests: 10,
                numTotalTests: 14,
                testSuites: [] as any,
                timestamp: previousWeekDate.toISOString(),
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 0,
                numOtherTests: 0,
                numPassedTests: 20,
                numTotalTests: 20,
                testSuites: [] as any,
                timestamp: previousWeekDate.toISOString(),
                _id: "2343546546756756823",
              }
            ],
          }
        ]
      },
      {
        name: "B",
        numFailedTests: 29,
        numOtherTests: 7,
        numPassedTests: 45,
        numTotalTests: 81,
        _id: "33254hb3vi4534fui3y",
        tests: [
          {
            name: "B1",
            numFailedTests: 20,
            numOtherTests: 5,
            numPassedTests: 25,
            numTotalTests: 50,
            selected: false,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 0,
                numOtherTests: 0,
                numPassedTests: 0,
                numTotalTests: 0,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 20,
                numOtherTests: 5,
                numPassedTests: 25,
                numTotalTests: 50,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              }
            ],
          },
          {
            name: "B2",
            numFailedTests: 9,
            numOtherTests: 2,
            numPassedTests: 20,
            numTotalTests: 31,
            selected: true,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 4,
                numOtherTests: 0,
                numPassedTests: 10,
                numTotalTests: 14,
                testSuites: [] as any,
                timestamp: previousWeekDate.toISOString(),
                _id: "798fgh578fgh578d",
              },
              {
                execTime: 70,
                numFailedTests: 5,
                numOtherTests: 2,
                numPassedTests: 10,
                numTotalTests: 17,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "2323232325359876543244f4c",
              }
            ],
          }
        ]
      }
    ]
}

export const updated_week_data_return = {
    numTotalExecutions: 3,
    numFailedTests: 8,
    totalTests: 48,
    executions: [
        {
          execTime: 80,
          groupName: "A",
          testName: "A1",
          numFailedTests: 4,
          numOtherTests: 0,
          numPassedTests: 10,
          numTotalTests: 14,
          testSuites: [] as any,
          timestamp: previousWeekDate.toISOString(),
          _id: "ef253544f4cfd8579847f9f6"
        },
        {
          execTime: 70,
          groupName: "A",
          testName: "A1",
          numFailedTests: 0,
          numOtherTests: 0,
          numPassedTests: 20,
          numTotalTests: 20,
          testSuites: [] as any,
          timestamp: previousWeekDate.toISOString(),
          _id: "2343546546756756823",
        },
        {
          execTime: 80,
          groupName: "B",
          testName: "B2",
          numFailedTests: 4,
          numOtherTests: 0,
          numPassedTests: 10,
          numTotalTests: 14,
          testSuites: [] as any,
          timestamp: previousWeekDate.toISOString(),
          _id: "798fgh578fgh578d",
        },
    ]        
}

const previousDayDate = new Date();
previousDayDate.setHours(currentDate.getHours() - 23);
export const updated_day_data = {
    idUser: "53freerger4h5yu4eswfgrt",
    numFailedTests: 100,
    numOtherTests: 8,
    numPassedTests: 5,
    totalExecutions: 12,
    __v: 1234567809876543,
    _id: "34tu9isdvnj23r3rsddddd",
    groups: [
      {
        name: "A",
        numFailedTests: 33,
        numOtherTests: 7,
        numPassedTests: 75,
        numTotalTests: 115,
        _id: "2354i354bhi234ui432uhi6huif",
        tests: [
          {
            name: "A1",
            numFailedTests: 4,
            numOtherTests: 0,
            numPassedTests: 30,
            numTotalTests: 34,
            selected: true,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 4,
                numOtherTests: 0,
                numPassedTests: 10,
                numTotalTests: 14,
                testSuites: [] as any,
                timestamp: previousDayDate.toISOString(),
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 0,
                numOtherTests: 0,
                numPassedTests: 20,
                numTotalTests: 20,
                testSuites: [] as any,
                timestamp: previousDayDate.toISOString(),
                _id: "2343546546756756823",
              }
            ],
          }
        ]
      },
      {
        name: "B",
        numFailedTests: 29,
        numOtherTests: 7,
        numPassedTests: 45,
        numTotalTests: 81,
        _id: "33254hb3vi4534fui3y",
        tests: [
          {
            name: "B1",
            numFailedTests: 20,
            numOtherTests: 5,
            numPassedTests: 25,
            numTotalTests: 50,
            selected: false,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 0,
                numOtherTests: 0,
                numPassedTests: 0,
                numTotalTests: 0,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 20,
                numOtherTests: 5,
                numPassedTests: 25,
                numTotalTests: 50,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              }
            ],
          },
          {
            name: "B2",
            numFailedTests: 9,
            numOtherTests: 2,
            numPassedTests: 20,
            numTotalTests: 31,
            selected: true,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 4,
                numOtherTests: 0,
                numPassedTests: 10,
                numTotalTests: 14,
                testSuites: [] as any,
                timestamp: previousDayDate.toISOString(),
                _id: "798fgh578fgh578d",
              },
              {
                execTime: 70,
                numFailedTests: 5,
                numOtherTests: 2,
                numPassedTests: 10,
                numTotalTests: 17,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "2323232325359876543244f4c",
              }
            ],
          }
        ]
      }
    ]
}

export const updated_day_data_return = {
    numTotalExecutions: 3,
    numFailedTests: 8,
    totalTests: 48,
    executions: [
        {
          execTime: 80,
          groupName: "A",
          testName: "A1",
          numFailedTests: 4,
          numOtherTests: 0,
          numPassedTests: 10,
          numTotalTests: 14,
          testSuites: [] as any,
          timestamp: previousDayDate.toISOString(),
          _id: "ef253544f4cfd8579847f9f6"
        },
        {
          execTime: 70,
          groupName: "A",
          testName: "A1",
          numFailedTests: 0,
          numOtherTests: 0,
          numPassedTests: 20,
          numTotalTests: 20,
          testSuites: [] as any,
          timestamp: previousDayDate.toISOString(),
          _id: "2343546546756756823",
        },
        {
          execTime: 80,
          groupName: "B",
          testName: "B2",
          numFailedTests: 4,
          numOtherTests: 0,
          numPassedTests: 10,
          numTotalTests: 14,
          testSuites: [] as any,
          timestamp: previousDayDate.toISOString(),
          _id: "798fgh578fgh578d",
        },
    ]        
}

export const deprecated_data = {
    idUser: "53freerger4h5yu4eswfgrt",
    numFailedTests: 100,
    numOtherTests: 8,
    numPassedTests: 5,
    totalExecutions: 12,
    __v: 1234567809876543,
    _id: "34tu9isdvnj23r3rsddddd",
    groups: [
      {
        name: "A",
        numFailedTests: 33,
        numOtherTests: 7,
        numPassedTests: 75,
        numTotalTests: 115,
        _id: "2354i354bhi234ui432uhi6huif",
        tests: [
          {
            name: "A1",
            numFailedTests: 4,
            numOtherTests: 0,
            numPassedTests: 30,
            numTotalTests: 34,
            selected: true,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 4,
                numOtherTests: 0,
                numPassedTests: 10,
                numTotalTests: 14,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 0,
                numOtherTests: 0,
                numPassedTests: 20,
                numTotalTests: 20,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "2343546546756756823",
              }
            ],
          }
        ]
      },
      {
        name: "B",
        numFailedTests: 29,
        numOtherTests: 7,
        numPassedTests: 45,
        numTotalTests: 81,
        _id: "33254hb3vi4534fui3y",
        tests: [
          {
            name: "B1",
            numFailedTests: 20,
            numOtherTests: 5,
            numPassedTests: 25,
            numTotalTests: 50,
            selected: false,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 0,
                numOtherTests: 0,
                numPassedTests: 0,
                numTotalTests: 0,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              },
              {
                execTime: 70,
                numFailedTests: 20,
                numOtherTests: 5,
                numPassedTests: 25,
                numTotalTests: 50,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "ef253544f4cfd8579847f9f6",
              }
            ],
          },
          {
            name: "B2",
            numFailedTests: 9,
            numOtherTests: 2,
            numPassedTests: 20,
            numTotalTests: 31,
            selected: true,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: [
              {
                execTime: 80,
                numFailedTests: 4,
                numOtherTests: 0,
                numPassedTests: 10,
                numTotalTests: 14,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "798fgh578fgh578d",
              },
              {
                execTime: 70,
                numFailedTests: 5,
                numOtherTests: 2,
                numPassedTests: 10,
                numTotalTests: 17,
                testSuites: [] as any,
                timestamp: "2022-06-26T09:14:23",
                _id: "2323232325359876543244f4c",
              }
            ],
          }
        ]
      }
    ]
}
