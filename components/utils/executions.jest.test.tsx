import { getFailedExecutions, getLastExecution, getSelectedExecutions, getLastExecutions } from './executions'
import { Execution, MultipleExecutions, Test } from './interfaces';
import { expect } from 'chai';
import { data, empty_data, non_selected, updated_week_data, updated_day_data, updated_day_data_return, updated_week_data_return, deprecated_data } from "./body"

describe('getFailedExecutions', () => {
  it('Devuelve las ejecuciones con algún fallo del array de ejecuciones enviada', () => {
    const expectedReturn = {
      numTotalExecutions: 2,
      numFailedTests: 4,
      totalTests: 42,
      executions: [data[0].tests[0].executions[0]]
    };
    const result = getFailedExecutions(data[0].tests[0]);
    expect(result).to.deep.equal(expectedReturn);
  });

  it('Devuelve una array vacío ya que las ejecuciones no hay ejecuciones con fallo', () => {
    const expectedReturn: MultipleExecutions =  {
      executions: [], 
      "numFailedTests": 0, 
      "numTotalExecutions": 0, 
      "totalTests": 0
    };

    const body = {
      numTotalExecutions: 3,
      numFailedTests: 10,
      totalTests: 5,
      executions: []
    };
    const result = getFailedExecutions(body);
    expect(result).to.deep.equal(expectedReturn);
  });

});

describe('getLastExecution', () => {
  it('Devuelve la última ejecucion de un array de grupos', () => {

    const expectedReturn = {
      execTime: 80,
      numFailedTests: 4,
      numOtherTests: 0,
      numPassedTests: 10,
      numTotalTests: 14,
      testSuites: [] as any,
      timestamp: "2023-06-27T09:14:24",
      _id: "ef253544f4cfd8579847f9f6",
    };
    const result = getLastExecution(data);
    expect(result).to.deep.equal(expectedReturn);
  });

  it('Devuelve un objeto vacío ya que no hay ejecuciones en los grupos', () => {
    const expectedReturn: Execution = {
      execTime: 0,
      executionNum: 0,
      groupName: "",
      numFailedTests: 0,
      numOtherTests: 0,
      numPassedTests: 0,
      testName: "",
      testSuites: [],
      timestamp: "",
    }
    const result = getLastExecution(empty_data);
    expect(result).to.deep.equal(expectedReturn);
  });

});

describe('getSelectedExecutions', () => {
  it('Devuelve una lista de los tests seleccionados', () => {


    const expectedReturn = [
      {
        name: "Sin fallos",
        numFailedTests: 10,
        numOtherTests: 2,
        numPassedTests: 30,
        numTotalTests: 42,
        token: "sdferg3546dg546",
        _id: "25gfbtdf",
        executions: [
          {
            executionNum: 1,
            execTime: 80,
            numFailedTests: 0,
            numOtherTests: 0,
            numPassedTests: 10,
            numTotalTests: 14,
            testSuites: [] as any,
            timestamp: "2023-06-27T09:14:23",
            _id: "89gh6ghcf57",
          },
          {
            executionNum: 2,
            execTime: 70,
            numFailedTests: 0,
            numOtherTests: 0,
            numPassedTests: 20,
            numTotalTests: 28,
            testSuites: [] as any,
            timestamp: "2023-06-27T09:14:23",
            _id: "12dv34dc5fgg",
          }
        ]
      }
    ];
    const result = getSelectedExecutions(data);
    expect(result).to.deep.equal(expectedReturn);
  });

  it('Devuelve una lista vacía ya que no hay tests seleccionados', () => {
    const expectedReturn: Array<Test> = []
    const result = getSelectedExecutions(non_selected);
    expect(result).to.deep.equal(expectedReturn);
  });
});

describe('getLastExecutions', () => {
  it('Devuelve el conjunto de ejecuciones de la última semana', () => {
    const result = getLastExecutions(updated_week_data, "7DAYS");
    expect(result).to.deep.equal(updated_week_data_return);
  });

  it('Devuelve el conjunto de ejecuciones del último dia', () => {
    const result = getLastExecutions(updated_day_data, "DAY");
    expect(result).to.deep.equal(updated_day_data_return);
  });

  it('Devuelve un objeto vacío ya que no hay ejecuciones del último dia', () => {
    const empty_body = { "executions": [], "numFailedTests": 0, "numTotalExecutions": 0, "totalTests": 0 }
    const result = getLastExecutions(deprecated_data, "DAY");
    expect(result).to.deep.equal(empty_body);
  });
});