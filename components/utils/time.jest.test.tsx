import { calculateDailyTotals, averageExecTime, avgTimeOfSelected } from './time';
import { expect } from 'chai';

describe('calculateDailyTotals', () => {
  it('Devuelve un array con los tests totales de cada dia con tests de la última semana', () => {
    const currentDate = new Date();
    const previousDate = new Date(currentDate);
    previousDate.setDate(currentDate.getDate() - 1);
    const data = [
      {
        execTime: 68.249,
        groupName: "Grupo 1",
        testName: "Test 1", 
        testSuites: [] as any,
        _id: "1",
        timestamp: previousDate.toISOString(),
        numPassedTests: 3,
        numFailedTests: 2,
        numOtherTests: 1,
        numTotalTests: 6
      },
      {
        execTime: 68.247,
        groupName: "Grupo 2",
        testName: "Test 2", 
        testSuites: [] as any,
        _id: "2",        
        timestamp: '2020-07-04T15:00:00Z',
        numPassedTests: 2,
        numFailedTests: 1,
        numOtherTests: 0,
        numTotalTests: 3
      },
      {
        execTime: 68.248,
        groupName: "Grupo 3",
        testName: "Test 3", 
        testSuites: [] as any,
        _id: "3",
        timestamp: '2020-07-05T12:00:00Z',
        numPassedTests: 1,
        numFailedTests: 0,
        numOtherTests: 2,
        numTotalTests: 3
      },
    ];

    const expectedCurrentDate = new Date();
    const expectedPreviousDate = new Date(currentDate);
    expectedPreviousDate.setDate(expectedCurrentDate.getDate() - 1);
    const expectedDailyTotals = [
      {
        timestamp: expectedPreviousDate.toISOString().split('T')[0],
        numPassedTests: 3,
        numFailedTests: 2,
        numOtherTests: 1,
      }
    ];

    const result = calculateDailyTotals(data);
    expect(result).to.deep.equal(expectedDailyTotals);
  });
  
  it('Omite un test con fecha posterior a la actual', () => {
    const currentDate = new Date();
    const futureDate = new Date();
    futureDate.setDate(currentDate.getDate() + 1);
    const data = [
      {
        execTime: 68.249,
        groupName: "Grupo 1",
        testName: "Test 1", 
        testSuites: [] as any,
        _id: "1",
        timestamp: futureDate.toISOString(),
        numPassedTests: 3,
        numFailedTests: 2,
        numOtherTests: 1,
        numTotalTests: 6
      }
    ];
  
    const result = calculateDailyTotals(data);
    expect(result).to.deep.equal([]);
  });

  it('Añade un test de hace exactamente una semana a la fecha actual', () => {
    const currentDate = new Date();
    const oneWeek = new Date();
    oneWeek.setDate(currentDate.getDate() - 7);
    const data = [
      {
        execTime: 68.249,
        groupName: "Grupo 1",
        testName: "Test 1", 
        testSuites: [] as any,
        _id: "1",
        timestamp: oneWeek.toISOString(),
        numPassedTests: 3,
        numFailedTests: 2,
        numOtherTests: 1,
        numTotalTests: 6
      }
    ];

    const expectedCurrentDate = new Date();
    const expectedPreviousDate = new Date(currentDate);
    expectedPreviousDate.setDate(expectedCurrentDate.getDate() - 7);
    const expectedData = [
      {
        timestamp: expectedPreviousDate.toISOString().split('T')[0],
        numPassedTests: 3,
        numFailedTests: 2,
        numOtherTests: 1,
      }
    ];
  
    const result = calculateDailyTotals(data);
    expect(result).to.deep.equal(expectedData);
  });
});

describe('averageExecTime', () => {
  it('Calcula la media de tiempo de todas las ejecuciones', () => {
    const data = [
      {
        idUser: "1",
        numFailedTests: 10,
        numOtherTests: 2,
        numPassedTests: 50,
        totalExecutions: 62,
        __v: 1212,
        _id: "22",
        groups: [
          {
            name: "A",
            numFailedTests: 42,
            numOtherTests: 0,
            numPassedTests: 18,
            numTotalTests: 60,
            _id: "64a6849694cfd8579847f9f6",
            tests: [
              {
                name: "A",
                numFailedTests: 42,
                numOtherTests: 0,
                numPassedTests: 18,
                numTotalTests: 60,
                selected: true,
                token: "efjoisdaa3gj34",
                _id: "234rf349694cfd8579847f9f6",
                executions: [
                  {
                    execTime: 80,
                    numFailedTests: 42,
                    numOtherTests: 0,
                    numPassedTests: 18,
                    numTotalTests: 60,
                    testSuites: [] as any,
                    timestamp: "2023-06-26T09:14:23",
                    _id: "ef253544f4cfd8579847f9f6",
                  },
                  {
                    execTime: 70,
                    numFailedTests: 42,
                    numOtherTests: 0,
                    numPassedTests: 18,
                    numTotalTests: 60,
                    testSuites: [] as any,
                    timestamp: "2023-06-26T09:14:23",
                    _id: "ef253544f4cfd8579847f9f6",
                  }
                ],
              },
            ],
          },
        ],
      },
    ];

    const expectedAvgTime = "00:01:15";
    const result = averageExecTime(data[0]);
    expect(result).to.deep.equal(expectedAvgTime);
  });

  it('Si no hay ejecuciones, devuelve 00:00:00', () => {
    const data = [
      {
        idUser: "1",
        numFailedTests: 10,
        numOtherTests: 2,
        numPassedTests: 50,
        totalExecutions: 62,
        __v: 1212,
        _id: "22",
        groups: [
          {
            name: "A",
            numFailedTests: 42,
            numOtherTests: 0,
            numPassedTests: 18,
            numTotalTests: 60,
            _id: "64a6849694cfd8579847f9f6",
            tests: [
              {
                name: "A",
                numFailedTests: 42,
                numOtherTests: 0,
                numPassedTests: 18,
                numTotalTests: 60,
                selected: true,
                token: "efjoisdaa3gj34",
                _id: "234rf349694cfd8579847f9f6",
                executions: []
              }
            ]
          }
        ]
      }
    ];
    const expectedAvgTime = "00:00:00";
    const result = averageExecTime(data[0]);
    expect(result).to.deep.equal(expectedAvgTime);
  });
});

describe('avgTimeOfSelected', () => {
  it('Calcula la media de tiempo de todas las ejecuciones', () => {
    const data = [
      {
        name: "A",
        numFailedTests: 42,
        numOtherTests: 0,
        numPassedTests: 18,
        numTotalTests: 60,
        selected: true,
        token: "efjoisdaa3gj34",
        _id: "234rf349694cfd8579847f9f6",
        executions: [
          {
            execTime: 80,
            numFailedTests: 42,
            numOtherTests: 0,
            numPassedTests: 18,
            numTotalTests: 60,
            testSuites: [] as any,
            timestamp: "2023-06-26T09:14:23",
            _id: "ef253544f4cfd8579847f9f6",
          },
          {
            execTime: 70,
            numFailedTests: 42,
            numOtherTests: 0,
            numPassedTests: 18,
            numTotalTests: 60,
            testSuites: [] as any,
            timestamp: "2023-06-26T09:14:23",
            _id: "ef253544f4cfd8579847f9f6",
          }
        ],
      },
      {
        name: "B",
        numFailedTests: 42,
        numOtherTests: 0,
        numPassedTests: 18,
        numTotalTests: 60,
        selected: true,
        token: "235sdfdaa3gj34",
        _id: "sdfdfgwert2254y547679",
        executions: [
          {
            execTime: 120,
            numFailedTests: 42,
            numOtherTests: 0,
            numPassedTests: 18,
            numTotalTests: 60,
            testSuites: [] as any,
            timestamp: "2023-06-26T09:14:23",
            _id: "ef253544f4cfd8579847f9f6",
          },
          {
            execTime: 110,
            numFailedTests: 42,
            numOtherTests: 0,
            numPassedTests: 18,
            numTotalTests: 60,
            testSuites: [] as any,
            timestamp: "2023-06-26T09:14:23",
            _id: "ef253544f4cfd8579847f9f6",
          }
        ],
      },
    ]

    const expectedAvgTime = "00:01:35";
    const result = avgTimeOfSelected(data);
    expect(result).to.deep.equal(expectedAvgTime);
  });

  it('Si no hay ejecuciones, devuelve 0', () => {
    const data = [
      {
        name: "A",
        numFailedTests: 42,
        numOtherTests: 0,
        numPassedTests: 18,
        numTotalTests: 60,
        _id: "64a6849694cfd8579847f9f6",
        tests: [
          {
            name: "A",
            numFailedTests: 42,
            numOtherTests: 0,
            numPassedTests: 18,
            numTotalTests: 60,
            selected: true,
            token: "efjoisdaa3gj34",
            _id: "234rf349694cfd8579847f9f6",
            executions: []
          }
        ]
      }
    ];
    const expectedAvgTime = "00:00:00";
    const result = avgTimeOfSelected(data);
    expect(result).to.deep.equal(expectedAvgTime);
  });
});