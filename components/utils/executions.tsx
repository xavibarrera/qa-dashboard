import { Test, Group, Data, Execution, MultipleExecutions, emptyExecution } from "./interfaces";

export function getFailedExecutions(input: MultipleExecutions): MultipleExecutions {
  const failedExecutions: Execution[] = [];
  let numTotalExecutions = 0;
  let numFailedTests = 0;
  let totalTests = 0;

  if (input.executions) {
    input.executions.forEach((execution: Execution) => {
      if (execution.numOtherTests !== 0 || execution.numFailedTests !== 0) {
        failedExecutions.push(execution);
        numFailedTests += execution.numFailedTests || 0;
      }
      totalTests += execution.numTotalTests || 0;
      numTotalExecutions++;
    });
  }

  return {
    executions: failedExecutions,
    numTotalExecutions,
    numFailedTests,
    totalTests,
  };
}

  export function getLastExecution(groups: Array<Group>): Execution {

    if (groups.length === 0) return emptyExecution;

    let latestExecution: Execution | undefined = undefined;
    for (const group of groups) {
      if (group.tests) {
        for (const test of group.tests) {
          if (test.executions) {
            for (const execution of test.executions) {
              if (
                execution.timestamp &&
                (!latestExecution ||
                  !latestExecution.timestamp ||
                  execution.timestamp > latestExecution.timestamp)
              ) {
                latestExecution = execution;
              }
            }
          }
        }
      }
    }
  
    return latestExecution || emptyExecution ;
  }
  
  export function getSelectedExecutions(dataArray: Array<Group>): Array<Test> {
    const selectedTests: Array<Test> = [];
  
      dataArray = dataArray ?? [];
  
    for (const testsArray of dataArray) {
        const tests = testsArray.tests ?? [];
      for (const test of tests) {
        if (test.selected) {
            const executions = test.executions ?? [];
            const resultObj = {
                _id: test._id,
                name: test.name,
                numTotalTests: test.numTotalTests,
                numFailedTests: test.numFailedTests,
                numPassedTests: test.numPassedTests,
                numOtherTests: test.numOtherTests,
                token: test.token,
                executions: executions.map((execution: Execution) => ({
                    executionNum: execution.executionNum,
                    numTotalTests: execution.numTotalTests,
                    numFailedTests: execution.numFailedTests,
                    numPassedTests: execution.numPassedTests,
                    numOtherTests: execution.numOtherTests,
                    execTime: execution.execTime,
                    testSuites: [],
                    timestamp: execution.timestamp,
                    _id: execution._id
                })),
            };
          selectedTests.push(resultObj);
        }
      }
    }
    return selectedTests;
  }
  
  export type PeriodType = "DAY" | "7DAYS" | "30DAYS" | "90DAYS";
  export function getLastExecutions(data: Data, period: PeriodType): MultipleExecutions {
    const currentDate = new Date();
  
    let timeRange: number;
    if (period === "7DAYS") {
      timeRange = 7 * 24 * 60 * 60 * 1000;
    } else if (period === "30DAYS") {
      timeRange = 30 * 24 * 60 * 60 * 1000;
    } else if (period === "90DAYS") {
      timeRange = 90 * 24 * 60 * 60 * 1000;
    } else if (period === "DAY"){
      timeRange = 24 * 60 * 60 * 1000;
    } else {
      throw new Error("Invalid period. Put '7DAYS' or '30DAYS' OR '90DAYS' or 'DAY'.");
    }
  
    const timeLimit = currentDate.getTime() - timeRange;
  
    const lastExecutions: Array<Execution> = [];
    let totalTests = 0;
    let numFailedTests = 0;
  
      data.groups?.forEach((group:Group) => {
          group.tests?.forEach((test:Test) => {
              test.executions?.forEach((execution:Execution) => {
                if(execution.timestamp){
                  const executionTimestamp = new Date(execution.timestamp);
                  if (executionTimestamp.getTime() >= timeLimit) {
                      const executionWithNames = {
                        groupName: group.name,
                        testName: test.name,
                        ...execution
                      };
                      lastExecutions.push(executionWithNames);
          
                    totalTests += execution.numTotalTests || 0;
                    numFailedTests += execution.numFailedTests || 0;
                  }
                }
                
              }); 
          });
      });
    
    const numTotalExecutions = lastExecutions.length;
    return { executions: lastExecutions, numTotalExecutions, totalTests, numFailedTests };
  }
