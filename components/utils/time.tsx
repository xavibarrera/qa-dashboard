import { Execution, Data, Group, Test } from "../utils/interfaces";

export function calculateDailyTotals(data: Array<Execution>): Array<Execution> {

  const dailyTotals: Array<Execution> = [];
  
  data.forEach((item) => {
    if (!item.timestamp) {
        return;
    }
    const timestamp = new Date(item.timestamp);
  
    const date = timestamp.toISOString().slice(0, 19);
    const existingDay = dailyTotals.find((day) => day.timestamp === date);
  
    if (existingDay) {
      existingDay.numPassedTests = (existingDay.numPassedTests ?? 0) + (item.numPassedTests ?? 0);
      existingDay.numFailedTests = (existingDay.numFailedTests ?? 0) + (item.numFailedTests ?? 0);
      existingDay.numOtherTests = (existingDay.numOtherTests ?? 0) + (item.numOtherTests ?? 0);
    } else {
      dailyTotals.push({
        timestamp: date,
        numPassedTests: item.numPassedTests,
        numFailedTests: item.numFailedTests,
        numOtherTests: item.numOtherTests,
        execTime: 0,
        testSuites: []
      });
    }
  });
  return sortExecutions(dailyTotals);
}

function sortExecutions(data: Array<Execution>): Array<Execution> {
  return data.sort((a, b) => {
    const dateA = a.timestamp ? new Date(a.timestamp) : null;
    const dateB = b.timestamp ? new Date(b.timestamp) : null;

    if (dateA && dateB) {
      return dateA.getTime() - dateB.getTime();
    } else if (dateA) {
      return -1;
    } else if (dateB) {
      return 1;
    } else {
      return 0;
    }
  });
}

  export function averageExecTime(data: Data): string {
    let sumatoriaExecTime = 0;
    let cantidadExecTime = 0;
    const groups = data.groups ?? [];
    groups.forEach((group: Group) => {
      group.tests?.forEach((test: Test) => {
        test.executions?.forEach((execution: Execution) => {
          if (execution.execTime !== undefined) {
            sumatoriaExecTime += execution.execTime;
            cantidadExecTime++;
          }
        });
      });
    });
  
      let totalSeconds = sumatoriaExecTime / cantidadExecTime;
      totalSeconds = isNaN(totalSeconds) ? 0 : totalSeconds;
      return new Date(totalSeconds * 1000).toISOString().slice(11, 19)
  }
  
  export function avgTimeOfSelected(array: Test[]): string {
    let sumatoriaExecTime = 0;
    let cantidadExecTime = 0;
    array.forEach((item) => {
      item.executions?.forEach((data: Execution) => {
        if (data.execTime !== undefined) {
          sumatoriaExecTime += data.execTime;
          cantidadExecTime++;
        }
      });
    });
  
    if (cantidadExecTime > 0) {
      const promedio = sumatoriaExecTime / cantidadExecTime;
      return new Date(promedio * 1000).toISOString().slice(11, 19)
    } else {
      return "00:00:00";
    }
  }


  export function formatTimestamp(timestamp: string): string {
    const date = new Date(timestamp);
    const day = date.getDate();
    const month = date.toLocaleString('es-ES', { month: 'long' });
    const formattedDate = `${day} de ${month}`;
    const hours = date.getHours();
    const minutes = date.getMinutes();
    return `${formattedDate} - ${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
  }

  export function formatTimeDuration(seconds: number): string {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);
    const remainingSeconds = seconds % 60;
    const deciseconds = Math.round((remainingSeconds - Math.floor(remainingSeconds)) * 10);
  
    const parts = [];
    if (hours > 0) {
      parts.push(`${hours} h`);
    }
    if (minutes > 0) {
      parts.push(`${minutes} min${minutes !== 1 ? 's' : ''}`);
    }
    if (remainingSeconds > 0 || deciseconds > 0) {
      if (remainingSeconds > 0 && deciseconds > 0) {
        parts.push(`${Math.floor(remainingSeconds)}.${deciseconds} seg${remainingSeconds !== 1 ? 's' : ''}`);
      } else {
        parts.push(`${Math.floor(remainingSeconds)} seg${remainingSeconds !== 1 ? 's' : ''}`);
        if (deciseconds > 0) {
          parts.push(`${deciseconds}`);
        }
      }
    }
  
    return parts.join(' ');
  }
  