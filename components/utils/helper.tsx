export function truncateName(name: string,  maxLength = 20){

  return name.slice(0, maxLength) + "...";
}