import { Data, Execution, Group, Test, emptyExecution, emptyTest } from "@/components/utils/interfaces";


export function getPercent(dashboardData: Data){
    let percent = 0;
    if(dashboardData?.totalTest && dashboardData?.numPassedTests){
      percent = (dashboardData?.numPassedTests / dashboardData.totalTest) * 100;
      percent = parseFloat(percent.toFixed(2)); 
    }
    return percent;
}

export function getTest(groups: Group[], token: string): Test {
  for (const group of groups) {
    if(group.tests){
      for (const test of group.tests) {
        if (test.token === token) {
          return test;
        }
      }
    }
  }
  return emptyTest;
}

export function getExecution(groups: Group[], id: string): Execution {
  for (const group of groups) {
    if (group.tests) {
      for (const test of group.tests) {
        if (test.executions) {
          for (const execution of test.executions) {
            if (execution._id === id) {
              return execution;
            }
          }
        }
      }
    }
  }
  return emptyExecution;
}
