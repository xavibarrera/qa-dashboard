import { getHeaderInfo } from './header'
import { expect } from 'chai';

describe('getHeaderInfo', () => {
    it('Devuelve los sumatorios necesarios para la cabecera', () => {
        const data = [
            {
              name: "A",
              numFailedTests: 10,
              numOtherTests: 2,
              numPassedTests: 30,
              numTotalTests: 42,
              selected: true,
              token: "efjoisdaa3gj34",
              _id: "234rf349694cfd8579847f9f6",
              executions: [
                {
                  execTime: 80,
                  numFailedTests: 4,
                  numOtherTests: 0,
                  numPassedTests: 10,
                  numTotalTests: 14,
                  testSuites: [] as any,
                  timestamp: "2023-06-26T09:14:23",
                  _id: "ef253544f4cfd8579847f9f6",
                },
                {
                  execTime: 70,
                  numFailedTests: 6,
                  numOtherTests: 2,
                  numPassedTests: 20,
                  numTotalTests: 28,
                  testSuites: [] as any,
                  timestamp: "2023-06-26T09:14:23",
                  _id: "ef253544f4cfd8579847f9f6",
                }
              ],
            },
            {
              name: "B",
              numFailedTests: 5,
              numOtherTests: 2,
              numPassedTests: 35,
              numTotalTests: 42,
              selected: true,
              token: "235sdfdaa3gj34",
              _id: "sdfdfgwert2254y547679",
              executions: [
                {
                  execTime: 120,
                  numFailedTests: 3,
                  numOtherTests: 0,
                  numPassedTests: 15,
                  numTotalTests: 18,
                  testSuites: [] as any,
                  timestamp: "2023-06-26T09:14:23",
                  _id: "ef253544f4cfd8579847f9f6",
                },
                {
                  execTime: 110,
                  numFailedTests: 2,
                  numOtherTests: 2,
                  numPassedTests: 20,
                  numTotalTests: 24,
                  testSuites: [] as any,
                  timestamp: "2023-06-26T09:14:23",
                  _id: "ef253544f4cfd8579847f9f6",
                }
              ],
            },
        ]

        const expectedHeader = {
            totalExecutions: 4,
            totalFailed: 15,
            totalOther: 4,
            totalPassed: 65,
            totalTests: 84
        };
        const result = getHeaderInfo(data);
        expect(result).to.deep.equal(expectedHeader);
    });

    it('Devuelve un resultado vacío si no hay ejecuciones', () => {
        const data = [
            {
              name: "A",
              numFailedTests: 10,
              numOtherTests: 2,
              numPassedTests: 30,
              numTotalTests: 42,
              selected: true,
              token: "efjoisdaa3gj34",
              _id: "234rf349694cfd8579847f9f6",
              executions: [],
            },
            {
              name: "B",
              numFailedTests: 5,
              numOtherTests: 2,
              numPassedTests: 35,
              numTotalTests: 42,
              selected: true,
              token: "235sdfdaa3gj34",
              _id: "sdfdfgwert2254y547679",
              executions: [],
            },
        ]

        const expectedHeader = {
            totalExecutions: 0,
            totalFailed: 0,
            totalOther: 0,
            totalPassed: 0,
            totalTests: 0
        };
        const result = getHeaderInfo(data);
        expect(result).to.deep.equal(expectedHeader);
    });
});