import { Button, Accordion, AccordionBody } from "@tremor/react";
import { truncateName } from "../utils/helper";
import { Test } from "../utils/interfaces";
import { SidebarTest } from "./sidebarTest";
import { useContext } from "react";
import { DashboardContext } from "@/utils/dashboardContext";
import { useRouter } from "next/router";
import { handleGroupClick } from "./eventHandlers";

export function SidebarGroup({ group }: any) {
    const router = useRouter();
    let {dashboardData, setDashboardData} = useContext(DashboardContext);
	const truncateValue = 20;
    const truncatedGroupName = group.name.length > truncateValue ? truncateName(group.name,truncateValue) : null;
    const allTestsSelected = group.tests.length > 0 && group.tests.every((test: Test) => test.selected);
    return (
      <div key={group._id}>
        <Accordion expanded className={`mb-2 ${allTestsSelected ? "border border-orange-500 rounded-lg" : "rounded-lg"}`}>
          <AccordionBody>
            <Button
              variant="light"
              className="focus:ring-0 mb-2 mt-2 w-40 h-6"
              color="orange"
              onClick={() => handleGroupClick(group, dashboardData, setDashboardData, router)}
            >
              <div className={`-ml-10 text-left ${allTestsSelected ? "text-orange-500" : "text-black"}`}>
                <div className="flex items-center">
                  <div className="w-32">{truncatedGroupName || group.name}</div>
                  {truncatedGroupName && (
                  <span className="bg-white text-black z-50 rounded p-2 absolute ml-2 opacity-0 group-hover:opacity-100 border">
                    {group.name}
                    </span>
                  )}
                </div>
              </div>
            </Button>
            <div>
            {group.tests.map((test: Test) => (
              <SidebarTest
                key={test._id}
                test={test}
                group={group}
              />
            ))}
            </div>
          </AccordionBody>
        </Accordion>
      </div>
    );
}
  