import {Button, Grid, TextInput, AccordionList, Title } from "@tremor/react";
import Image from "next/image";
import { MagnifyingGlassIcon, HomeIcon, PencilSquareIcon } from "@heroicons/react/24/solid";
import rs from "public/images/dashboardlogo.png";
import { useRouter } from "next/router";
import * as React from 'react';
import { DashboardContext } from "@/utils/dashboardContext";
import { UserContext } from "utils/userContext";
import { useTranslation } from "next-i18next";
import { useEffect, useContext } from "react";
import { homeData } from "../../utils/homeData";
import { SidebarGroup } from "./sidebarGroup";
import { Group } from "../utils/interfaces";
import { handleHome, handleGroups, handleEmptySidebarClick } from "./eventHandlers";

export default function Sidebar() {
  const { t } = useTranslation("sidebar");
  let { dashboardData, setDashboardData } = useContext(DashboardContext);
  let { userData } = useContext(UserContext);

  useEffect(() => {
    const revalidationInterval = setInterval(() => {
      mutateData();
    }, 3000);

    return () => {
      clearInterval(revalidationInterval);
    };
  });

  const mutateData = async () => {
    if(userData.status === 'authenticated'){
      let res = await homeData(userData.idDashboard);
      setDashboardData(res);
      localStorage.setItem('dashboardObject', JSON.stringify(res));
    }
  };
  
  const router = useRouter();
  const placeholder = t("search");
  return (
    <div className="h-full mx-3">
      <Image src={rs} alt="logo" className="w-28 h-28 mx-auto" />
      <Grid className=" space-y-4 pt-5">	
        <Button
          className="justify-start ml-5 focus:ring-0"
          variant="light"
          color="gray"
          onClick = {() => handleHome(router)}
          icon={HomeIcon}
        >
          <div className="ml-1">
            {t("home")}
          </div>
           
        </Button>
        <TextInput icon={MagnifyingGlassIcon} placeholder={placeholder} className="bg-slate-50 border-none focus:ring-0"/>
        <AccordionList className="w-full max-w-md mx-auto flex-shrink-0 text-center mb-2  shadow-none">
          {dashboardData?.[0]?.groups?.length === 0 ? (
            <div>
              <Title onClick = {() => handleEmptySidebarClick(router)} className="cursor-pointer"> 
                {t("noData")} 
              </Title>
            </div>
          ) : (
            <ul>
            {dashboardData?.[0]?.groups?.map((group: Group) => (
              <SidebarGroup
                key={group._id}
                group={group}
              />
            ))}
          </ul>
          )}
        </AccordionList>
        <Button
          icon={PencilSquareIcon}
          className="justify-start ml-5 focus:ring-0"
          variant="light"
          color="gray"
          onClick = {() => handleGroups(router)}
        >
          <div className="ml-1">
            {t("editDashboard")}
          </div>
        </Button>
		<div></div>
      </Grid>
    </div>
  );
}
