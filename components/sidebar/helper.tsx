interface Test {
  token: string;
  name: string;
  group: string;
  numFailedTests: number,
  numOtherTests: number,
  numPassedTests: number,
  numTotalTests: number,
  date: string,
  hour: string
}

export function getSidebarData(data: any) {
  const result: {
    name: string;
    numFailedTests: number;
    numOtherTests: number;
    numPassedTests: number;
    numTotalTests: number;
    tests: {
      token: string;
      name: string;
      numFailedTests: number;
      numOtherTests: number;
      numPassedTests: number;
      numTotalTests: number;
      date: string;
      hour: string;
    }[];
  }[] = [];

  data.groups.forEach((group: any) => {
    const groupTest = {
      name: group.name,
      numFailedTests: group.numFailedTests,
      numOtherTests: group.numOtherTests,
      numPassedTests: group.numPassedTests,
      numTotalTests: group.numTotalTests,
      tests: [] as {
        token: string;
        name: string;
        numFailedTests: number;
        numOtherTests: number;
        numPassedTests: number;
        numTotalTests: number;
        date: string;
        hour: string;
      }[],
    };

    group.tests.forEach((test: any) => {
      groupTest.tests.push({
        token: test.token,
        name: test.name,
        numFailedTests: test.numFailedTests,
        numOtherTests: test.numOtherTests,
        numPassedTests: test.numPassedTests,
        numTotalTests: test.numTotalTests,
        date: test.date,
        hour: test.hour
      });
    });

    result.push(groupTest);
  });

  return result;
}

export function initialClickedTests(inputArray: Array<any>): Array<Test> {
  const newArray: Array<Test> = [];
  for (let i = 0; i < inputArray[0].length; i++) {
    const group = inputArray[i];
    const testArray = group.tests;
    for (const element of testArray) {
      const test = element;
      const testObject: Test = {
        token: test.token,
        name: test.name,
        group: group.name,
        numFailedTests: test.numFailedTests,
        numOtherTests: test.numOtherTests,
        numPassedTests: test.numPassedTests,
        numTotalTests: test.numTotalTests,
        date: test.date,
        hour: test.hour
      };
      newArray.push(testObject);
    }
  }
  return newArray;
}