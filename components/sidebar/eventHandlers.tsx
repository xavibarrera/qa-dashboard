
export async function logOut(setUserData: any, setDashboardData: any, router: any) {
    let newUser = {
      username: '',
      email: '',
      idDashboard: '',
      status: 'unauthenticated',
    }

    let initialDashboard = [{
      _id: "",
      idUser: "",
      totalExecutions: 0,
      totalTest: 0,
      numFailedTests: 0,
      numPassedTests: 0,
      numOtherTests: 0,
      groups: []
  }]

    setUserData(newUser);
    setDashboardData(initialDashboard);
    localStorage.setItem('userObject', JSON.stringify(newUser));
    localStorage.setItem('dashboardObject', JSON.stringify(initialDashboard));

    try {
      const res = await fetch(process.env.NEXT_PUBLIC_url + `/api/users/auth/logout`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (res.status === 200) {
        router.push("/");
      }
    } catch (err) {
      console.log(err);
    }
}

export async function handleTestClick(test: any, group: any, dashboardData: any, setDashboardData: any, router: any) {
    let newArray = [];
    if (dashboardData[0].groups) {
      newArray = await Promise.all(
        dashboardData[0].groups.map(async (g: any) => {
          if (g._id === group._id) {
            let newTests = await Promise.all(
              g.tests.map(async (t: any) => {
                if (t._id === test._id) {
                  const response = await fetch(
                    process.env.NEXT_PUBLIC_url + "/api/results/sidebar",
                    {
                      method: "PATCH",
                      body: JSON.stringify({
                        group: group._id,
                        test: test._id
                      }),
                      headers: {
                        "Content-Type": "application/json",
                      },
                    }
                  );
                  if (response.ok) {
                    console.log("Tests updated successfully!");
                  } else {
                    console.error("Failed to update tests.");
                  }
                  return { ...test, selected: !test.selected };
                } else {
                  return { ...t };
                }
              })
            );
            return { ...g, tests: newTests };
          } else {
            return { ...g, tests: [...g.tests] };
          }
        })
      );
    }
    let newDash = { ...dashboardData };
    newDash[0].groups = newArray;
    setDashboardData(newDash);
    localStorage.setItem('dashboardObject', JSON.stringify(newDash));
    router.push("/project");
}

export const handleEmptySidebarClick = (router: any) => {
    router.push("/groups")
}

export async function handleGroupClick(data: any, dashboardData: any, setDashboardData: any, router: any) {

    let newArray = [];
    if (dashboardData[0].groups) {
      newArray = await Promise.all(
        dashboardData[0].groups.map(async (group: any) => {
          if (group._id === data._id) {
            let allTestsFalse = group.tests.every((test: any) => !test.selected);
            let newTests: any[] = [];
            if (allTestsFalse) {
              newTests = await Promise.all(
                group.tests.map(async (test: any) => {
                  const response = await fetch(
                    process.env.NEXT_PUBLIC_url + "/api/results/sidebar",
                    {
                      method: "PATCH",
                      body: JSON.stringify({
                        group: group._id,
                        test: test._id,
                      }),
                      headers: {
                        "Content-Type": "application/json",
                      },
                    }
                  );
                  if (response.ok) {
                    console.log("Tests updated successfully!");
                  } else {
                    console.error("Failed to update tests.");
                  }
                  return { ...test, selected: true };
                })
              );
            } else {
              newTests = await Promise.all(
                group.tests.map(async (test: any) => {
                  if (test.selected) {
                    const response = await fetch(
                      process.env.NEXT_PUBLIC_url + "/api/results/sidebar",
                      {
                        method: "PATCH",
                        body: JSON.stringify({
                          group: group._id,
                          test: test._id,
                        }),
                        headers: {
                          "Content-Type": "application/json",
                        },
                      }
                    );
                    if (response.ok) {
                      console.log("Tests updated successfully!");
                    } else {
                      console.error("Failed to update tests.");
                    }
                    return { ...test, selected: false };
                  }
                  return test;
                })
              );
            }
            return { ...group, tests: newTests };
          } else {
            return { ...group, tests: [...group.tests] };
          }
        })
      );
    }
  
    let newDash = { ...dashboardData };
    newDash[0].groups = newArray;
    setDashboardData(newDash);
    localStorage.setItem("dashboardObject", JSON.stringify(newDash));
    router.push("/project");
}
  
export function handleHome(router: any) {
  router.push("/home");
}
  
export function handleGroups(router: any) {
  router.push("/groups");
}