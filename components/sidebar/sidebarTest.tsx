import { Button } from "@tremor/react";
import { truncateName } from "../utils/helper";
import { handleTestClick } from "./eventHandlers";
import { useContext } from "react";
import { DashboardContext } from "@/utils/dashboardContext";
import { useRouter } from "next/router";

export function SidebarTest({ test, group }: any) {
    const router = useRouter();
    let {dashboardData, setDashboardData} = useContext(DashboardContext);
  const truncateValue = 20;
  const truncatedTestName = test.name && test.name.length > truncateValue ? truncateName(test.name,truncateValue) : null;

  return (
    <div key={test._id}>
      <Button
        className={`focus:ring-0 w-full mb-1 border-none text-black  ${test.selected ? "bg-orange-100" : "bg-gray-100"}`}
        key={test._id}
        variant="secondary"
        color="amber"
        onClick={() => handleTestClick(test, group, dashboardData, setDashboardData, router)}
      >
        <div className="flex items-center">
            <div className="w-33">{truncatedTestName || test.name}</div>
            {truncatedTestName && (
                  <span className="bg-white text-black z-50 rounded p-2 absolute ml-2 opacity-0 group-hover:opacity-100 border">
                  {test.name}
              </span>
            )}
          </div>
      </Button>
    </div>
  );
}
