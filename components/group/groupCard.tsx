import {
    Card,
    Table,
    TableHead,
    TableRow,
    TableHeaderCell,
    TableBody,
    TableCell,
    Text,
    Title,
    TextInput,
    Button
  } from "@tremor/react";
  import { UserContext } from "../../utils/userContext";
  import { DashboardContext } from "../../utils/dashboardContext";
  import React, { useState, useContext, ChangeEvent } from 'react';
  import { homeData } from '../../utils/homeData';
  import EditIcon from '@mui/icons-material/Edit';
  import HighlightOffIcon from '@mui/icons-material/HighlightOff';  
  import { useTranslation } from "next-i18next";
  import { GroupRow } from './groupRow';
  import { DeleteModal } from "./deleteModal";
  import { Group, Test } from "../utils/interfaces";
  import { truncateName } from "../utils/helper";

export function GroupCard({ group, tests }: { group: Group, tests: Test[] }) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [selectedTestId, setSelectedTestId] = useState<string | null>(null);
    const [currentlyEditedTestId, setCurrentlyEditedTestId] = useState<string | null>(null);
    const { userData } = useContext(UserContext);
    const { setDashboardData } = useContext(DashboardContext);
    const [testInputs, setTestInputs] = useState<{ [groupId: string]: string }>({});
    const [isEditingGroupName, setIsEditingGroupName] = useState(false);
    const [editedGroupName, setEditedGroupName] = useState(group.name);
    const [editedTestName, setEditedTestName] = useState<{ [testId: string]: string }>({});

    const handleEditGroupName = () => setIsEditingGroupName(true);
    const closeModal = () => setIsModalOpen(false);
    const handleEditedGroupNameChange = (e: ChangeEvent<HTMLInputElement>) => setEditedGroupName(e.target.value);

    const handleSaveGroupName = async () => {
        const response = await fetch(
            process.env.NEXT_PUBLIC_url + "/api/results/group",
            {
                method: "PUT",
                body: JSON.stringify({
                    userId: userData.idDashboard,
                    groupId: group._id,
                    newGroupName: editedGroupName
                }),
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );

        if (response.ok) {
            setIsEditingGroupName(false);
            const updatedTests = await homeData(userData.idDashboard);
            setDashboardData(updatedTests);
            localStorage.setItem('dashboardObject', JSON.stringify(updatedTests));
        } else {
            console.error("Failed to update group name.");
        }
    };
    const { t } = useTranslation("groups");

    const handleTestInputChange = (groupId: string, value: string) => {
        setTestInputs((prevInputs) => ({
        ...prevInputs,
        [groupId]: value,
        }));
    };

    const handleDeleteGroup = async (groupId: string) => {
        const response = await fetch(
            process.env.NEXT_PUBLIC_url + "/api/results/group",
            {
                method: "DELETE",
                body: JSON.stringify({
                userId: userData.idDashboard,
                groupId: groupId,
                }),
                headers: {
                "Content-Type": "application/json",
                },
            }
        );
        const updatedTests = await homeData(userData.idDashboard);
        setDashboardData(updatedTests);
        localStorage.setItem('dashboardObject', JSON.stringify(updatedTests));

        if (response.ok) {
        console.log("Group deleted successfully!");
        } else {
        console.error("Failed to delete group.");
        }
        closeModal();
    };

    const handleCopyToClipboard = (text:any) => {
        navigator.clipboard.writeText(text)
    };

    const handleDeleteTest = async (groupId: string, testId: string) => {
        const response = await fetch(
            process.env.NEXT_PUBLIC_url + "/api/results/test",
            {
                method: "DELETE",
                body: JSON.stringify({
                userId: userData.idDashboard,
                groupId: groupId,
                testId: testId
                }),
                headers: {
                "Content-Type": "application/json",
                },
            }
        );
        const updatedTests = await homeData(userData.idDashboard);
        setDashboardData(updatedTests);
        setTestInputs((prevTestInputs) => ({
            ...prevTestInputs,
            [groupId]: "",
        }));
        localStorage.setItem('dashboardObject', JSON.stringify(updatedTests));

        if (response.ok) {
        console.log("Test deleted successfully!");
        } else {
        console.error("Failed to delete test.");
        }
        closeModal();
    }

    const handleAddTests = async (groupId: string, test: string) => {
        const trimmedTest = test.trim();
        if(trimmedTest === ""){
            console.error("Test field is empty. Please enter a value.");
            return;
        }
        const response = await fetch(
        process.env.NEXT_PUBLIC_url + "/api/results/test",
        {
            method: "POST",
            body: JSON.stringify({
            id: userData.idDashboard,
            groupId: groupId,
            test: test,
            }),
            headers: {
            "Content-Type": "application/json",
            },
        }
        );
        const updatedTests = await homeData(userData.idDashboard);
        setDashboardData(updatedTests);
        setTestInputs((prevTestInputs) => ({
            ...prevTestInputs,
            [groupId]: "",
        }));
        localStorage.setItem('dashboardObject', JSON.stringify(updatedTests));

        if (response.ok) {
        console.log("Tests added successfully!");
        } else {
        console.error("Failed to add tests.");
        }
    };

    const handleEditedTestNameChange = (testId: string, value: string) => {
        setEditedTestName((prevEditedTestName) => ({
            ...prevEditedTestName,
            [testId]: value,
        }));
    };

    const handleSaveTestName = async (groupId: string, testId: string) => {
        const editedName = editedTestName[testId];
        const response = await fetch(
            process.env.NEXT_PUBLIC_url + "/api/results/test",
            {
                method: "PUT",
                body: JSON.stringify({
                    userId: userData.idDashboard,
                    groupId: groupId,
                    testId: testId,
                    newTestName: editedName,
                }),
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );

        if (response.ok) {
            const updatedTests = await homeData(userData.idDashboard);
            setDashboardData(updatedTests);
            localStorage.setItem('dashboardObject', JSON.stringify(updatedTests));
        } else {
            console.error("Failed to update test name.");
        }
    };
    let startWriting = (t("startWriting"));
    let truncatedName;
    const maxLength = 20;
    let show = false;
    if (group.name !== undefined) {
      if (group.name.length > maxLength) {
        truncatedName = truncateName(group.name)
        show = true;
      }
    }
    return (
        <Card key={group._id}>
            <Title className="flex ml-3 mr-4" data-testid="groupName">
                {isEditingGroupName ? (
                    <input
                    type="text"
                    placeholder={startWriting}
                    value={editedGroupName}
                    onChange={handleEditedGroupNameChange}
                    onBlur={handleSaveGroupName}
                    data-testid="inputGroupName"
                    onKeyPress={(e) => {
                        if (e.key === 'Enter') handleSaveGroupName();
                    }}
                    autoFocus
                    />
                ) : (
                    <>
                    {show ? (
                        <div className="group relative w-max">
                            <div
                                className="w-auto max-w-[24rem]"
                                style={{ fontSize: 'larger', fontWeight: 'bold' }}
                            >
                                {truncatedName}
                            </div>
                            <span className="bg-white text-black rounded p-2 absolute -top-10 left-1/2 transform -translate-x-1/2 opacity-0 transition-opacity group-hover:opacity-100  border border-black/10 dark:border-gray-900/50 dark:bg-gray-700 shadow">
                            {group.name}
                            </span>
                        </div>
                        ) : (
                            <div
                                className="w-auto max-w-[24rem]"
                                style={{ fontSize: 'larger', fontWeight: 'bold' }}
                            >
                                {group.name}
                            </div>
                    )}
                    
                    
                    <EditIcon
                        className="ml-2 mt-2 text-gray-300 cursor-pointer text-sm"
                        onClick={handleEditGroupName}
                        data-testid="editGroupInput"
                    />
                    </>
                )}
                <HighlightOffIcon
                    className="text-red-500 cursor-pointer ml-auto"
                    data-testid="deleteGroupButton"
                    onClick={() => {
                    setSelectedTestId(null);
                    setIsModalOpen(true);
                    }}
                />
                </Title>


            <Table className="mt-5">
                <TableHead>
                    <TableRow>
                        <TableHeaderCell className="w-1/2">{t("groupName")}</TableHeaderCell>
                        <TableHeaderCell className="w-1/2">{t("groupToken")}</TableHeaderCell>
                    </TableRow>
                    <TableRow>
                        <TableHeaderCell>
                            <div className="flex">
                                <TextInput
                                    type="text"
                                    placeholder={startWriting}
                                    className="w-64"
                                    data-testid="addTestInput"
                                    value={testInputs[group._id || 0] || ''}
                                    onChange={(e) => handleTestInputChange(group._id || '', e.target.value)}
                                    onKeyPress={(e) => {
                                        if (e.key === 'Enter') handleAddTests(group._id || '', testInputs[group._id || 0] || '');
                                    }}
                                />
                                <Button
                                    variant="light"
                                    className="bg-green-500 text-white p-3 rounded-lg ml-4"
                                    onClick={() => {handleAddTests(group._id || '', testInputs[group._id || 0] || '')}}
                                    data-testid="addTestButton"
                                >
                                    +
                                </Button>
                            </div>
                        </TableHeaderCell>
                        <TableHeaderCell>
                            <Text>
                                {t("automaticallyGenerated")}
                            </Text>
                        </TableHeaderCell>
                    </TableRow>
                </TableHead>

                <TableBody>
                    {tests.length === 0 ? (
                        <TableRow>
                            <TableCell>
                                {t("noTests")}
                            </TableCell>
                        </TableRow>
                    ) : (
                        tests.map((test: Test) => (
                            <GroupRow
                              key={test.token}
                              test={test}
                              groupId={group._id || ''}
                              currentlyEditedTestId={currentlyEditedTestId}
                              editedTestName={editedTestName}
                              handleEditedTestNameChange={handleEditedTestNameChange}
                              handleSaveTestName={handleSaveTestName}
                              setCurrentlyEditedTestId={setCurrentlyEditedTestId}
                              handleCopyToClipboard={handleCopyToClipboard}
                              setSelectedTestId={setSelectedTestId}
                              setIsModalOpen={setIsModalOpen}
                            />
                          ))
                    )}
                </TableBody>
            </Table>
            <DeleteModal isOpen={isModalOpen} closeModal={closeModal} handleDeleteGroup={handleDeleteGroup} handleDeleteTest={handleDeleteTest} testId={selectedTestId  || ''} groupId={(group._id || '')} />
        </Card>
    );
}