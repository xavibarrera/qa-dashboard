import React from 'react';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import EditIcon from '@mui/icons-material/Edit';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { Test } from '../utils/interfaces';
import { truncateName } from '../utils/helper';

interface GroupRowProps {
  test: Test;
  groupId: string;
  currentlyEditedTestId: string | null;
  editedTestName: { [testId: string]: string };
  handleEditedTestNameChange: (testId: string, value: string) => void;
  handleSaveTestName: (groupId: string, testId: string) => void;
  setCurrentlyEditedTestId: (testId: string | null) => void;
  handleCopyToClipboard: (text: string) => void;
  setSelectedTestId: (testId: string | null) => void;
  setIsModalOpen: (isOpen: boolean) => void;
}

export function GroupRow({
  test,
  groupId,
  currentlyEditedTestId,
  editedTestName,
  handleEditedTestNameChange,
  handleSaveTestName,
  setCurrentlyEditedTestId,
  handleCopyToClipboard,
  setSelectedTestId,
  setIsModalOpen,
}:GroupRowProps) {
  let truncatedName;
  const maxLength = 20;
  let show = false;
  if (test.name !== undefined) {
    if (test.name.length > maxLength) {
      truncatedName = truncateName(test.name)
      show = true;
    }
  }
  return (
    <TableRow key={test.token}>
      <TableCell>
        {currentlyEditedTestId === test._id ? (
          <input
            type="text"
            data-testid="editTestInput"
            value={editedTestName[test._id] || test.name}
            onChange={(e) => handleEditedTestNameChange(test._id || '', e.target.value)}
            onBlur={() => {
              handleSaveTestName(groupId, test._id || '');
              setCurrentlyEditedTestId(null);
            }}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                handleSaveTestName(groupId, test._id || '');
                setCurrentlyEditedTestId(null);
              }
            }}
            autoFocus
          />
        ) : (
          <div className='flex'>
            {show ? (
              <div className="group relative w-max">
                  <div
                      className="w-auto max-w-[24rem]"
                      style={{ fontSize: 'larger', fontWeight: 'bold' }}
                  >
                      {truncatedName}
                  </div>
                  <span className="bg-white text-black rounded p-2 absolute -top-10 left-1/2 transform -translate-x-1/2 opacity-0 transition-opacity group-hover:opacity-100  border border-black/10 dark:border-gray-900/50 dark:bg-gray-700 shadow">
                  {test.name}
                  </span>
              </div>
              ) : (
                  <div
                      className="w-auto max-w-[24rem]"
                      style={{ fontSize: 'larger', fontWeight: 'bold' }}
                      data-testid="testName"
                  >
                      {test.name}
                  </div>
            )}
            <EditIcon
              className="ml-2 mb-1 text-gray-300 cursor-pointer text-sm"
              data-testid="testNameIcon"
              onClick={() => setCurrentlyEditedTestId(test._id || '')}
            />
          </div>
        )}
      </TableCell>
      <TableCell>
        <div>
          <ContentCopyIcon
            className="mr-2 text-gray-300 cursor-pointer"
            onClick={() => handleCopyToClipboard(test.token || '')}
          />
          {test.token}
        </div>
      </TableCell>
      <TableCell>
        <HighlightOffIcon
          className="ml-auto text-red-500 cursor-pointer"
          data-testid="deleteTestButton"
          onClick={() => {
            setSelectedTestId(test._id || '');
            setIsModalOpen(true);
          }}
        />
      </TableCell>
    </TableRow>
  );
}