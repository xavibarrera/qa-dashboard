import {
    Card,
    Title,
    Subtitle,
    Button
} from "@tremor/react";
import React from 'react';
import { useTranslation } from "next-i18next";
import Modal from "react-modal";
import { DeleteModalProps } from "../utils/interfaces";


export function DeleteModal ({ isOpen, closeModal, handleDeleteGroup, handleDeleteTest, groupId, testId }: DeleteModalProps) {
    const { t } = useTranslation("groups");

    return (
        <Modal isOpen={isOpen} onRequestClose={closeModal} className="fixed inset-0 flex items-center justify-center">
            <Card className="p-4 w-500">
                <Title className="text-center mb-12">{t("sure")}</Title>
                <Subtitle className="mb-4">{t("deleteActionMsg")}</Subtitle>
                <Subtitle className="mb-12">{t("youCanAdd")}</Subtitle>
                <div className="flex justify-center space-x-4">
                    {testId && handleDeleteTest ? (
                        <Button 
                            className="bg-orange-500"
                            variant="primary"
                            color="amber"
                            data-testid="modalDeleteTestButton"
                            onClick={() => {handleDeleteTest(groupId, testId)}}
                        >
                            {t("deleteTestBtn")}
                        </Button>
                    ) : (
                        <Button 
                            className="bg-orange-500"
                            variant="primary"
                            color="amber"
                            data-testid="modalDeleteGroupButton"
                            onClick={() => handleDeleteGroup(groupId)}
                        >
                            {t("deleteGroupBtn")}
                        </Button>
                    )}
                    <Button
                        className="bg-orange-500" 
                        variant="primary"
                        color="amber"
                        onClick={closeModal}
                    >
                        {t("cancel")}
                    </Button>
                </div>
            </Card>
        </Modal>
    );
}