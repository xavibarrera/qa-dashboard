import { UserContext } from "../../utils/userContext";
import { DashboardContext } from "@/utils/dashboardContext";
import { useState, useContext} from "react";
import { Button, TextInput } from "@tremor/react";
import { homeData } from "../../utils/homeData";
import { useTranslation } from "next-i18next";

export function GroupInput() {
    const { userData } = useContext(UserContext);
    const { setDashboardData } = useContext(DashboardContext);
    const [group, setGroup] = useState("");
    const { t } = useTranslation("groups");
    const [error, setError] = useState("");
    const [showError, setShowError] = useState(false);
    const [success, setSuccess] = useState("");
    const [showSuccess, setShowSuccess] = useState(false);

	function hideMessage(){
		setTimeout(() => {
			setShowSuccess(false);
			setShowError(false);
		}, 2000);
	}

    const handleAddGroup = async () => {
        const trimmedGroup = group.trim();
        if (trimmedGroup === "") {
            setError(t("emptyInputMessage")!);
            setShowError(true);
			hideMessage();
            return;
        }
        const response = await fetch(
            process.env.NEXT_PUBLIC_url + "/api/results/group",
            {
                method: "POST",
                body: JSON.stringify({ group, id: userData.idDashboard }),
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );
        const updatedGroups = await homeData(userData.idDashboard);
        setDashboardData(updatedGroups);
        localStorage.setItem("dashboardObject", JSON.stringify(updatedGroups));
        setGroup("");
        if (response.ok) {
            setSuccess(t("successAddingGroup")!);
            setShowSuccess(true);
			hideMessage();
        } else {
            setError(t("errorAddingGroup")!);
            setShowError(true);
			hideMessage();
        }
    };

    let addGroupText = t("addGroupText");

    return (
        <div className="flex px-2 py-1">
            <Button
                variant="light"
                className="bg-green-500 text-white p-3 rounded-lg"
                onClick={handleAddGroup}
                data-testid="addGroupButton"
>
                {t("addGroupBtn")}
            </Button>
            <TextInput
                type="text"
                value={group}
                placeholder={addGroupText}
                className="w-64 border border-gray-300 rounded ml-4"
                data-testid="addGroupInput"
                onChange={(e) => setGroup(e.target.value)}
                onKeyPress={(e) => {
                    if (e.key === "Enter") {
                        handleAddGroup();
                    }
                }}
            />
            {showError && <div className="text-red-500 pl-6 pt-2">{error}</div>}
            {showSuccess && <div className="text-green-500 pl-6 pt-2">{success}</div>}
        </div>
    );
}