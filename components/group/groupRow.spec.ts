import { test, expect } from "../../e2e/qaDashboardFixture";

test.describe("Edit and delete test", () => {
    let validEmail =  'qatesting.redsauce@gmail.com'
	let validPassword =  'Redsauce23!'
    
    test.beforeAll(async ({ page, qaDashboard }) => {
        await page.goto("/");
        await qaDashboard.login(validEmail,validPassword);
        await expect(page).toHaveURL(/home/)
    });

    test("Edit test", async ({ page }) => {
        await expect(page).toHaveURL(/.*groups/);
        const groupName = page.getByTestId("groupName").nth(1);
        await page.getByTestId('editGroupIcon').nth(1).click();
        await page.getByTestId('inputGroupName').fill('Cambio de nombre');
        await page.keyboard.press('Enter');
        await expect(groupName).toHaveText('Cambio de nombre');
    });

    
    test("Add test", async ({ page }) => {
        await expect(page).toHaveURL(/.*groups/);
        const testName = page.getByTestId("testName").nth(0);
        await page.getByTestId('testNameIcon').nth(0).click();
        await page.getByTestId('addTest').nth(1).fill('Nuevo nombre test');
        await page.keyboard.press('Enter');
        await expect(testName).toHaveText('Nuevo nombre test');
    });

    test("Delete test", async ({ page }) => {
        await expect(page).toHaveURL(/.*groups/);
        const newGroup = page.getByTestId("groupName").nth(1);
        await page.getByTestId('HighlightOffIcon').nth(3).click();
        await page.getByTestId('modalDeleteButton').click();
        await expect(newGroup).toHaveCount(0); 
    });
});
