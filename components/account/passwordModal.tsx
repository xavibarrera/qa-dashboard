import React, { useState, useContext, useEffect } from "react";
import { UserContext } from "@/utils/userContext";
import Modal from "react-modal";
import { Card, Metric, Subtitle, Button } from "@tremor/react";
import { InputPassword } from "../inputPassword";
import { useTranslation } from "next-i18next";

function PasswordModal({
  isOpen,
  onClose,
  incorrectPassword,
  setIncorrectPassword,
  openNewPasswordModal,
}: any) {

  const { userData } = useContext(UserContext);
  const [currentPassword, setCurrentPassword] = useState("");
  const { t } = useTranslation(["account"]);

  useEffect(() => {
    if (isOpen) {
      setCurrentPassword("");
    }
  }, [isOpen]);

  async function verifyPassword(password: string) {
    const response = await fetch(
        process.env.NEXT_PUBLIC_url + "/api/users/auth/verifyPassword",
        {
            method: "POST",
            body: JSON.stringify({
                email: userData.email,
                password: password
            }),
            headers: {
                "Content-Type": "application/json",
            },
        }
    );
    if (response.ok) {
        return true;
    } else {
      setIncorrectPassword(true);
      return false
    }
  }
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onClose}
      className="fixed inset-0 flex flex-col items-center justify-center"
    >
      <Card className="w-auto">
        <Metric className="font-bold mb-8">
          {t("writeYourPass")}
        </Metric>
        <Subtitle className="mb-16">
        {t("writeYourActualPass")}
        </Subtitle>
        <div className="flex flex-col">
          <InputPassword
            value={currentPassword}
            setPasswordData={setCurrentPassword}
            emptyPassMessage="Enter the password"
            invalidPassword="Invalid password"
            invalidPasswordExplained="It must be at least 8 characters long, and include a capital letter, a number and a symbol."
            placeholder="Password"
          />
          {incorrectPassword && (
            <p className="text-red-500 text-sm">
              {t("incorrectPass")}
            </p>
          )}

          <div className="flex justify-between mt-4">
            <Button
              onClick={onClose}
              className="bg-orange-500 w-fit mx-auto"
              variant="primary"
              color="amber"
            >
              {t("cancel")}
            </Button>
            <Button
              onClick={async () => {
                const isValid = await verifyPassword(currentPassword);
                if (isValid) {
                  onClose();
                  openNewPasswordModal();
                } else {
                  setIncorrectPassword(true);
                }
              }}
              className="bg-orange-500 w-fit mx-auto"
              data-testid="nextModalButton"
              variant="primary"
              color="amber"
            >
              {t("next")}
            </Button>
          </div>
        </div>
      </Card>
    </Modal>
  );
}

export default PasswordModal;
