import { Bold, Button, Card, Metric, Subtitle } from "@tremor/react";
import EditIcon from '@mui/icons-material/Edit';
import { UserContext } from "@/utils/userContext";
import { useContext } from "react";
import { useTranslation } from "next-i18next";

export default function ProfileCard({setChangePasswordModalOpen}: any) {
    const { userData } = useContext(UserContext);
    const { t } = useTranslation(["account"]);
    return (
        <Card>
            <div className="p-4">
                <Metric className="font-bold mb-2" data-testid="profileUsername">
                    {userData.username}
                </Metric>
                <Subtitle className="text-gray-500 mb-8">
                    Subtítulo
                </Subtitle>
                <div className="flex">
                    <div className="flex flex-col mr-64 mb-8">
                        <Bold className="font-semibold mb-4">
                         {t("email")}
                        </Bold>
                        <Subtitle className="mt-2">
                            {userData.email}
                        </Subtitle>
                    </div>
                    <div className="flex flex-col">
                        <Bold className="font-semibold mb-4">
                            {t("password")}
                        </Bold>
                        <div className="flex">
                            <Subtitle className="mt-2 mr-4">
                                *******************
                            </Subtitle>
                            <div onClick={() => setChangePasswordModalOpen(true)} className="cursor-pointer" data-testid="editAccountButton">
                                <EditIcon className="text-orange-400 text-sm"/>
                                <Button 
                                    variant="light" 
                                    className="p-2 text-orange-400 focus:ring-0" 
                                    color="orange"
                                >
                                    {t("edit_password_button")}
                                </Button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </Card>
    );
}
