import React, { useState, useContext } from "react";
import { UserContext } from "@/utils/userContext";
import Modal from "react-modal";
import { Card, Metric, Subtitle, Button } from "@tremor/react";
import { InputPassword } from "../inputPassword";
import { useTranslation } from "next-i18next";

function NewPasswordModal({
  isOpen,
  onClose
}: any) {
    const [newPassword, setNewPassword] = useState("");
    const [invalidPassword, setInvalidPassword] = useState(false);
    const { userData } = useContext(UserContext);
    const { t } = useTranslation(["account"]);

    async function handleChangePassword(newPassword: string) {
      const response = await fetch(
          process.env.NEXT_PUBLIC_url + "/api/users/auth/changePassword",
          {
            method: "PUT",
            body: JSON.stringify({
                email: userData.email,
                password: newPassword
            }),
            headers: {
                "Content-Type": "application/json",
            },
          }
      );

      if (response.ok) {
          return true;
      } else {
          return false;
      }
  }
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onClose}
      className="fixed inset-0 flex flex-col items-center justify-center"
    >
      <Card className="w-auto">
        <Metric className="font-bold mb-8" data-testid="newPasswordTitle">{t("newPassword")}</Metric>
        <Subtitle className="mb-16">
          {t("newPasswordSubtitle")}
        </Subtitle>
        <div className="flex flex-col">
          <div data-testid="changePasswordInput">
            <InputPassword
              value={newPassword}
              setPasswordData={setNewPassword}
              setPasswordValidated={setInvalidPassword}
              emptyPassMessage="Enter the password"
              invalidPassword="Invalid password"
              invalidPasswordExplained="It must be at least 8 characters long, and include a capital letter, a number and a symbol."
              placeholder="Password"
            />
          </div>
          
          <div className="flex justify-between mt-4">
            <Button
              onClick={onClose}
              className="bg-orange-500 w-fit mx-auto"
              variant="primary"
              color="amber"
            >
              Cancelar
            </Button>
            <Button
                onClick={async () => {
                const isValid = await handleChangePassword(newPassword);
                if (isValid && invalidPassword) {
                    onClose();
                    handleChangePassword(newPassword);
                } else {
                    setInvalidPassword(true);
                }
                }}
                className="bg-orange-500 w-fit mx-auto"
                variant="primary"
                data-testid="changePasswordButton"
                color="amber"
            >
                {t("changePassword")}
            </Button>
          </div>
        </div>
      </Card>
    </Modal>
  );
}

export default NewPasswordModal;
