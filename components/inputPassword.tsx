import { TextInput } from "@tremor/react";
import React, { useState } from "react";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

interface InputPasswordProps {
  value?: string;
  setPasswordData: React.Dispatch<React.SetStateAction<string>>;
  setPasswordValidated?: React.Dispatch<React.SetStateAction<boolean>>;
  emptyPassMessage?: string;
  invalidPassword?: string;
  invalidPasswordExplained?: string;
  placeholder?: string;
  details?: boolean;
  onEnter?: () => void;
}

export function InputPassword({
  value,
  setPasswordData,
  setPasswordValidated,
  emptyPassMessage = "Enter the password",
  invalidPassword = "Invalid password",
  invalidPasswordExplained = "It must be at least 8 characters long, and include a capital letter, a number and a symbol.",
  placeholder = "Password",
  details = false,
  onEnter,
}: InputPasswordProps) {
  const [showPassword, setShowPassword] = useState(false);
  const [errorPassword, setErrorPassword] = useState(false);
  const [messagePassword, setMessagePassword] = useState("");

  function resetAuthErrorStates() {
    setErrorPassword(false);
    setMessagePassword("");
  }
  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      onEnter?.();
    }
  };

  const checkInputs = (event: any) => {
    let error = false;
    const re = new RegExp(
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=])[A-Za-z0-9@#$%^&+!=]{8,}$"
    );
    if (event.target.value === "") {
      const message = emptyPassMessage;
      setMessagePassword(message);
      setErrorPassword(true);
      error = true;
    } else if (!re.test(event.target.value) && !details) {
      const message = invalidPassword;
      setMessagePassword(message);
      setErrorPassword(true);
      error = true;
    } else if (!re.test(event.target.value) && details) {
      const message = invalidPasswordExplained;
      setMessagePassword(message);
      setErrorPassword(true);
      error = true;
    }
    setPasswordData(event.target.value);
    if (setPasswordValidated) setPasswordValidated(!error);
  };

  const handlePasswordInputChange = (event: any) => {
    setPasswordData(event.target.value);
    checkInputs(event);
    resetAuthErrorStates();
  };
  return (
    <div className="relative">
      <TextInput
        onKeyDown={handleKeyDown}
        onChange={handlePasswordInputChange}
        onBlur={checkInputs}
        type={showPassword ? "text" : "password"}
        error={errorPassword}
        value={value}
        errorMessage={messagePassword}
        placeholder={placeholder}
        data-testid="inputPassword"
      />
      <div
        className="absolute right-4 top-1"
        data-testid="btnSwitchPasswordVisibility"
        onClick={() => setShowPassword(!showPassword)}
      >
        {showPassword ? (
          <VisibilityIcon className="text-gray-500 text-sm ml-2" />
        ) : (
          <VisibilityOffIcon className="text-gray-500 text-sm ml-2" />
        )}
      </div>
    </div>
  );
}
