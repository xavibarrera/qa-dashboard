import { Text, Button, Metric, Subtitle } from "@tremor/react";
import { useState, useContext } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { UserContext } from "../utils/userContext";
import { DashboardContext } from "../utils/dashboardContext";
import handleLocaleChange from "utils/handleLocaleChange";
import { InputPassword } from "../components/inputPassword";
import { InputEmail } from "../components/inputEmail";

export default function Login() {
  const { setUserData } = useContext(UserContext);
  const { setDashboardData } = useContext(DashboardContext);
  const { t } = useTranslation("login");
  const [emailData, setEmailData] = useState("");
  const [passwordData, setPasswordData] = useState("");
  const [emailValidated, setEmailValidated] = useState(false);
  const [passwordValidated, setPasswordValidated] = useState(false);
  const [loginError, setLoginError] = useState("");
  const [showLoginError, setShowLoginError] = useState(false);
  const router = useRouter();
  function hideLoginError() {
    setLoginError("");
    setShowLoginError(false);
  }
  async function handleLogin(data: any) {
    if (emailValidated && passwordValidated) {
      try {
        const res = await fetch(
          process.env.NEXT_PUBLIC_url + `/api/users/auth/login`,
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          }
        );
        const result = await res.json();
        if (res.status === 200) {
          const newUser = {
            username: result.body.username,
            email: result.body.email,
            idDashboard: result.body.idDashboard,
            status: "authenticated",
          };
          setUserData(newUser);
          localStorage.setItem("userObject", JSON.stringify(newUser));

          const dash = await fetch(
            process.env.NEXT_PUBLIC_url + `/api/results/${newUser.idDashboard}`,
            {
              method: "GET",
              headers: {
                "Content-Type": "application/json",
              },
            }
          );

          const dashboard = await dash.json();
          if (dash.status === 200) {
            setDashboardData(dashboard);
            localStorage.setItem("dashboardObject", JSON.stringify(dashboard));

            if (dashboard[0].groups.length > 0) {
              router.push("/home");
            } else {
              router.push("/groups");
            }
          }
        } else if (res.status === 401) {
          const message = t("wrongAuth");
          setLoginError(message);
          setShowLoginError(true);
          setTimeout(hideLoginError, 2000);
        } else {
          const message = t("serverError");
          setLoginError(message);
          setShowLoginError(true);
          setTimeout(hideLoginError, 2000);
        }
      } catch (err) {
		const message = t("serverError");
		setLoginError(message);
		setShowLoginError(true);
		setTimeout(hideLoginError, 2000);
      }
    }
  }

  async function handleForgotPassword() {
    router.push("/forgotPassword");
  }

  return (
    <div className="flex justify-center items-center text-center">
      <div>
        <Metric>{t("web name")}</Metric>
        <Subtitle className="mt-4 w-80 ">{t("greeting")}</Subtitle>
        <div className="mt-10">
          <InputEmail
            value={emailData}
            setEmailData={setEmailData}
            setEmailValidated={setEmailValidated}
            onEnter={() => {
              handleLogin({
                email: emailData,
                password: passwordData,
              });
            }}
          />
          <div className="mt-4">
            <InputPassword
              value={passwordData}
              setPasswordData={setPasswordData}
              setPasswordValidated={setPasswordValidated}
              onEnter={() => {
                handleLogin({
                  email: emailData,
                  password: passwordData,
                });
              }}
            />
          </div>
          {showLoginError && (<div data-testid="loginError"><Text  className="text-red-500">{loginError}</Text></div>
          )}
        </div>
        <div className="flex justify-center items-center space-x-6 mt-8 text-[#737373]">
          <label className="hover:text-gray-800">
            <input
              id="miCheckbox"
              type="checkbox"
              name="checkbox"
              className="form-checkbox h-3 w-3 mr-1"
              data-testid="btnRemember"
            />
            {t("remember me")}
          </label>
          <Button
            data-testid="btnRecoverPassword"
            onClick={() => {
              handleForgotPassword();
            }}
            variant="light"
            color="neutral"
          >
            {t("forgot password")}
          </Button>
        </div>

        <div className="flex justify-center items-center space-x-6 mt-12">
          <Button
            variant="primary"
            color="orange"
            className="h-8 w-28"
            data-testid="btnLogin"
            onClick={() => {
              handleLogin({
                email: emailData,
                password: passwordData,
              });
            }}
          >
            {t("login")}
          </Button>
        </div>
        <Text className="mt-12">{t("terms")}</Text>
        <div className="flex justify-center items-center space-x-6 mt-6">
          <Button
            variant="light"
            color="gray"
            onClick={() => handleLocaleChange("en", router)}
          >
            EN
          </Button>
          <Button
            variant="light"
            color="gray"
            onClick={() => handleLocaleChange("es", router)}
          >
            ES
          </Button>
        </div>
      </div>
    </div>
  );
}
