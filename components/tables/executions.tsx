import {
  Table,
  TableHead,
  TableHeaderCell,
  TableBody,
  TableRow,
  TableCell,
  Text
} from "@tremor/react";
import { Bar } from "../charts/bar";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import React, { ChangeEvent, useState } from 'react';
import { Data, Execution } from "../utils/interfaces";
import { getFailedExecutions, getLastExecutions } from "../../components/utils/executions"
import { truncateName } from "../utils/helper";
import { formatTimeDuration, formatTimestamp } from "../utils/time";

export function Executions(dashboardData: Data) {
  let last24hExecutions = getLastExecutions(dashboardData, "DAY");
  const { t } = useTranslation(["common", "groups"]);
  const router = useRouter();
  
  const [selectedOption, setSelectedOption] = useState("failed");
  const handleOptionChange = (event: ChangeEvent<HTMLSelectElement>) => {
    setSelectedOption(event.target.value);
  };
  let data = selectedOption === "failed" ? getFailedExecutions(last24hExecutions) : last24hExecutions;
  const [hoveredRowIndex, setHoveredRowIndex] = useState(-1); 
  const handleButtonClick = () => router.push("/project");
  let executionsArray = [];
  if (Array.isArray(data.executions)) {
    executionsArray = data.executions;
  } else {
    executionsArray = [data];
  }

  return (
    <>
    <div className="flex mb-5">
      <strong>{t("groups:last24")}</strong>
      <select
          id="executionOption"
          className="ml-2"
          value={selectedOption}
          onChange={handleOptionChange}
        >
          <option value="failed">{t("groups:failed24")}</option>
          <option value="todos">{t("groups:all24")}</option>
      </select>
    </div>
    
    {executionsArray.length === 0 ? (
        <Text>
        {selectedOption === "failed"
          ? t("noFailedTests24h")
          : t("noTests24h")
        }
      </Text>
      ) : (
      <Table className="mt-4">
        <TableHead>
          <TableRow>
            <TableHeaderCell>{t("group")}</TableHeaderCell>
            <TableHeaderCell>{t("test")}</TableHeaderCell>
            <TableHeaderCell>{t("execution")}</TableHeaderCell>
            <TableHeaderCell>{t("dateTime")}</TableHeaderCell>
            <TableHeaderCell>{t("execTime")}</TableHeaderCell>
            <TableHeaderCell>{t("totalTest")}</TableHeaderCell>
            <TableHeaderCell>{t("result")}</TableHeaderCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {Array.isArray(data.executions) ? (
          data.executions.map((execution: Execution, index: number) => {
            const objResult = {
              data: [
                {
                  Failed: execution.numFailedTests,
                  Passed: execution.numPassedTests,
                  Other: execution.numOtherTests,
                  test: execution.testName,
                  group: execution.groupName,
                },
              ],
              test: execution.testName,
              group: execution.groupName,
              layout: "vertical" as const,
              stack: true,
              maxValue: execution.numTotalTests ? execution.numTotalTests : 0,
              index: "Date",
              className: "mt-4 h-14 ",
            };
            let truncatedGroupName;
            let truncatedTestName;
            const maxLength = 15;
            let showGroup = false;
            let showTest = false;

            if (execution.groupName !== undefined) {
              if (execution.groupName.length > maxLength) {
                truncatedGroupName = truncateName(execution.groupName);
                showGroup = true;
              }
            }
            if (execution.testName !== undefined) {
              if (execution.testName.length > maxLength) {
                truncatedTestName = truncateName(execution.testName);
                showTest = true;
              }
            }
            return (
              <TableRow
                key={execution._id}
                onClick={handleButtonClick}
                onMouseEnter={() => setHoveredRowIndex(index)}
                onMouseLeave={() => setHoveredRowIndex(-1)}
                data-testid="execution_row"
                className={hoveredRowIndex === index ? "bg-gray-100" : ""}
              >
                <TableCell data-testid="group">
                  {showGroup ? (
                    <div className="group relative w-max">
                      <div
                          className="w-32 truncate overflow-ellipsis"
                      >
                        {truncatedGroupName}
                      </div>
                      <span className="bg-white text-black rounded p-2 absolute -top-10 left-1/2 transform -translate-x-1/2 opacity-0 transition-opacity group-hover:opacity-100 border border-black/10 dark:border-gray-900/50 dark:bg-gray-700 shadow">
                        {execution.groupName}
                      </span>
                    </div>
                  ) : (
                    <div
                      className="w-32 truncate overflow-ellipsis"
                      data-testid="groupName"
                    >
                      {execution.groupName}
                    </div>
                  )}
                  
                </TableCell>
                <TableCell data-testid="test">
                  {showTest ? (
                    <div className="group relative w-max">
                      <div
                          className="w-32 truncate overflow-ellipsis"
                      >
                        {truncatedTestName}
                      </div>
                      <span className="bg-white text-black rounded p-2 absolute -top-10 left-1/2 transform -translate-x-1/2 opacity-0 transition-opacity group-hover:opacity-100 border border-black/10 dark:border-gray-900/50 dark:bg-gray-700 shadow">
                        {execution.testName}
                      </span>
                    </div>
                  ) : (
                    <div
                      className="w-32 truncate overflow-ellipsis"
                      data-testid="testName"
                    >
                      {execution.testName}
                    </div>
                  )}
                </TableCell>                
                <TableCell data-testid="executions">{execution.executionNum}</TableCell>
                <TableCell data-testid="timestamp">{execution.timestamp ? formatTimestamp(execution.timestamp) : ''}</TableCell>
                <TableCell data-testid="time">{execution.execTime ? formatTimeDuration(execution.execTime) : ''}</TableCell>
                <TableCell data-testid="totalTest">{execution.numTotalTests}</TableCell>
                <TableCell data-testid="bar" className="p-0 w-1/2">
                  <Bar {...objResult} />
                </TableCell>
              </TableRow>
            );
          })
          ) : (
            <TableRow>
              <TableCell colSpan={7}>{t("noTests24h")}</TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
      )}
    </>
  );
}