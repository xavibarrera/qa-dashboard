import {
    Table,
    TableHead,
    TableHeaderCell,
    TableBody,
    TableRow,
    Text,
  } from "@tremor/react";
import { useTranslation } from "next-i18next";
import { Execution } from "../../utils/interfaces";
import { TestExecutionsRow } from "./testExecutionRow";
  
export function TestExecutions(execs: Execution[]) {
    let aux = Object.values(execs);
    const { t } = useTranslation(["testinfo"]);

    return (    
        <>  
        {aux.length === 0 ? (
            <Text>{t("emptyList")}</Text>
          ) : (
          <Table className="mt-4">
            <TableHead>
              <TableRow>
                <TableHeaderCell>{t("executionNumber")}</TableHeaderCell>
                <TableHeaderCell>{t("dateAndTime")}</TableHeaderCell>
                <TableHeaderCell>{t("totalExecTime")}</TableHeaderCell>
                <TableHeaderCell>{t("totalTest")}</TableHeaderCell>
                <TableHeaderCell/>
                <TableHeaderCell>{t("result")}</TableHeaderCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {Array.isArray(aux) ? (
            aux.slice().sort((a, b) => (b.executionNum ?? 0) - (a.executionNum ?? 0)).map((execution: Execution, index: number) => {
              return (
                <TestExecutionsRow key={execution._id} exec={execution} index={index} />
              );   
            })
          ) : (
            <TableRow>
              <Text>{t("emptyList")}</Text>
            </TableRow>
          )}
            </TableBody>
          </Table>
          )}
          </>
    );
}