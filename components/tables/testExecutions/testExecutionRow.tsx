import {
  TableRow,
  TableCell,
  Button
} from "@tremor/react";
import { Bar } from "../../charts/bar";
import { useTranslation } from "next-i18next";
import { useState } from 'react';
import { Execution } from "../../utils/interfaces";
import { formatTimeDuration, formatTimestamp } from "@/components/utils/time";
import { useRouter } from "next/router";

export function TestExecutionsRow({ exec, index }: { exec: Execution; index: number }) {
  const [hoveredRowIndex, setHoveredRowIndex] = useState(-1); 
  const { t } = useTranslation(["testinfo"]);
  const router = useRouter();

  const objResult = {
    data: [
      {
        Failed: exec.numFailedTests,
        Passed: exec.numPassedTests,
        Other: exec.numOtherTests,
        test: exec.testName,
        group: exec.groupName,
      },
    ],
    test: exec.testName,
    group: exec.groupName,
    layout: "vertical" as const,
    stack: true,
    maxValue: exec.numTotalTests ? exec.numTotalTests : 0,
    index: "Date",
    className: "mt-4 h-14"
  };

  let redirect = window.location.href + '/execution';
  const handleButtonClick = (id: string) => {
    router.push({
      pathname: redirect + '/' +id,
    });
  };

  return (
    <TableRow
      key={exec._id}
      onMouseEnter={() => setHoveredRowIndex(index)}
      onMouseLeave={() => setHoveredRowIndex(-1)}
      data-testid="execution_row"
      className={hoveredRowIndex === index ? "bg-gray-100" : ""}
    >             
    <TableCell data-testid="executionNumber">{exec.executionNum}</TableCell>
    <TableCell data-testid="dateandtime">{exec.timestamp ? formatTimestamp(exec.timestamp) : ''}</TableCell>
    <TableCell data-testid="totalexectime">{exec.execTime ? formatTimeDuration(exec.execTime): ''}</TableCell>
    <TableCell data-testid="totaltest">{exec.numTotalTests}</TableCell>
    <TableCell data-testid="moreinsights">
      <Button 
        variant="light"
        className="mb-2 mt-2 w-40 h-6"
        color='orange'
        onClick={() => handleButtonClick(exec._id ? exec._id : '')}
      >
        {t("moreInsights")}
      </Button>
    </TableCell>
    <TableCell data-testid="result" className="p-0 w-1/2">
      <Bar {...objResult} />
    </TableCell>
    </TableRow>
  );
}