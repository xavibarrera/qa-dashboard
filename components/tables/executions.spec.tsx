import { test, expect } from "@playwright/experimental-ct-react";
import { Executions } from './executions'
import { Data, MultipleExecutions } from '../utils/interfaces';
import { MemoryRouterProvider } from 'next-router-mock/MemoryRouterProvider/next-12';
import { formatTimestamp } from "../utils/time";

let currentDate = new Date();
currentDate.setTime(currentDate.getTime() - 3600 * 1000);

let final_timestamp = formatTimestamp(currentDate.toISOString())
const data: Data = {
	"_id": "6458a1fae31ac858b9d86e0a",
	"totalExecutions": 23,
	"totalTest": 15500,
	"numFailedTests": 2450,
	"numPassedTests": 12500,
	"numOtherTests": 550,
	"groups": [
		{
			"name": "QA Dashboard",
			"numTotalTests": 5500,
			"numFailedTests": 450,
			"numPassedTests": 5000,
			"numOtherTests": 50,
			"tests": [
				{
					"token": "23545465678568fgh34",
					"name": "UT staging 01",
					"numTotalTests": 200,
					"numFailedTests": 40,
					"numPassedTests": 150,
					"numOtherTests": 10,
					"executions": [
						{
              "groupName": "QA Dashboard",
              "testName": "UT staging 01",
              "executionNum": 1,
							"timestamp": currentDate.toString(),
							"execTime": 12,
							"numTotalTests": 200,
							"numFailedTests": 40,
							"numPassedTests": 150,
							"numOtherTests": 10,
							"testSuites": [
								{
									"name": "Cart",
									"execTime": 12,
									"numFailedTests": 40,
									"numPassedTests": 36,
									"numOtherTests": 12,
									"testCases": [
										{
											"name": "test 1",
											"execTime": "14''",
											"status": "Successful"
										},
										{
											"name": "test 2",
											"execTime": "4''",
											"status": "Fail"
										}
									]
								}
							]
						}
					]
				}
			]
		}
	]
}

const ret: MultipleExecutions = {
  executions: [
    {
      "timestamp": final_timestamp,
      "execTime": 12,
      "numTotalTests": 200,
      "numFailedTests": 40,
      "numPassedTests": 150,
      "numOtherTests": 10,
      "testSuites": [
        {
          "name": "Cart",
          "execTime": 12,
          "numFailedTests": 40,
          "numPassedTests": 36,
          "numOtherTests": 12,
          "testCases": [
            {
              "name": "test 1",
              "execTime": "14''",
              "status": "Successful"
            },
            {
              "name": "test 2",
              "execTime": "4''",
              "status": "Fail"
            }
          ]
        }
      ]
    }
  ],
  numTotalExecutions: 0,
  numFailedTests: 2,
  totalTests: 2
}

test.describe("<Executions />", () => {
  let component: any;
  test.beforeEach(async ({ mount }) => {
    component = await mount(
      <MemoryRouterProvider url="/">
         <Executions {...data} />
      </MemoryRouterProvider>
    );
  });

  test("Show the correct values", async () => {
    const group = component.locator("[data-testid=groupName]");
    const test = component.locator("[data-testid=test]");
    const execNum = component.locator("[data-testid=executions]");
    const timestamp = component.locator("[data-testid=timestamp]");
    const execTime = component.locator("[data-testid=time]");
    const totalTest = component.locator("[data-testid=totalTest]");
    if(ret.executions && ret.executions[0].groupName !== undefined) await expect(group).toHaveText(ret.executions[0].groupName);
    if(ret.executions && ret.executions[0].testName !== undefined) await expect(test).toHaveText(ret.executions[0].testName);
    if(ret.executions && ret.executions[0].executionNum !== undefined) await expect(execNum).toHaveText(ret.executions[0].executionNum.toString());
    if(ret.executions && ret.executions[0].timestamp !== undefined) await expect(timestamp).toHaveText(ret.executions[0].timestamp);
    let finalExecTime = ret.executions?.[0]?.execTime?.toString() + " segs" || "N/A";
    if(ret.executions && ret.executions[0].execTime !== undefined) await expect(execTime).toHaveText(finalExecTime);
    if(ret.executions && ret.executions[0].numTotalTests !== undefined) await expect(totalTest).toHaveText(ret.executions[0].numTotalTests.toString());
  });

  test("Render de bar", async ({ page }) => {
    const legend = component.locator("[role='dialog']");

    await page.locator(".recharts-rectangle").nth(1).hover();
    await expect(legend).toContainText("40Failed");
    await expect(legend).toContainText("150Passed");
    await expect(legend).toContainText("10Other");
  });

});