import {
  TableRow,
  TableCell,
  Accordion,
  AccordionHeader,
  AccordionBody
} from "@tremor/react";
import { Bar } from "../../charts/bar";
import { TestSuite } from "../../utils/interfaces";
import { formatTimeDuration } from "@/components/utils/time";
import { TestCases } from "../testCases/testCases";

export function TestSuitesRow({ testsuite }: { testsuite: TestSuite }) {

    const objResult = {
        data: [
            {
                Failed: testsuite.numFailedTests,
                Passed: testsuite.numPassedTests,
                Other: testsuite.numOtherTests,
            }
        ],
        layout: "vertical" as const,
        stack: true,
        index: "Date",
        maxValue: testsuite.numPassedTests ? testsuite.numPassedTests : 0,
        className: "mt-4 h-14 "
    };

    return (
        <TableRow
            key={testsuite._id}>             
            <TableCell data-testid="testSuite">
                <Accordion className="border-none shadow-none m-0 p-0">
                    <AccordionHeader>
                        {testsuite.name}
                    </AccordionHeader>
                    <AccordionBody>
                        <TestCases {...testsuite.testCases || []}></TestCases>
                    </AccordionBody>
                </Accordion>
            </TableCell>
            <TableCell data-testid="totalexectime" className="align-top">{testsuite.execTime ? formatTimeDuration(testsuite.execTime): ''}</TableCell>
            <TableCell data-testid="result" className="p-0 w-1/2 align-top">
                <Bar {...objResult} />
            </TableCell>
        </TableRow>
    );
}