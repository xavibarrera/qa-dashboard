import {
    Table,
    TableHead,
    TableHeaderCell,
    TableBody,
    TableRow,
    Text,
  } from "@tremor/react";
import { useTranslation } from "next-i18next";
import { TestSuite } from "../../utils/interfaces";
import { TestSuitesRow } from "./testSuitesRow";
  
export function Testsuites(testSuites: TestSuite[]) {
    let aux = Object.values(testSuites);
    const { t } = useTranslation(["testinfo"]);

    return (    
        <>  
        {aux.length === 0 ? (
            <Text>{t("emptyTestsuites")}</Text>
          ) : (
          <Table className="mt-4">
            <TableHead>
              <TableRow>
                <TableHeaderCell>{"TEST SUITES"}</TableHeaderCell>
                <TableHeaderCell>{t("execTime")}</TableHeaderCell>
                <TableHeaderCell>{t("result")}</TableHeaderCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {Array.isArray(aux) ? (
            aux.map((testSuite: TestSuite) => {
              return (
                <>
                  <TestSuitesRow key={testSuite._id} testsuite={testSuite}></TestSuitesRow>
                </>
              );   
            })
          ) : (
            <TableRow>
              <Text>{t("emptyList")}</Text>
            </TableRow>
          )}
            </TableBody>
          </Table>
          )}
          </>
    );
}