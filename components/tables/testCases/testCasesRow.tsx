import {
    TableRow,
    TableCell,
} from "@tremor/react";
import { TestCase } from "../../utils/interfaces";
import { formatTimeDuration } from "@/components/utils/time";

export function TestCasesRow({ testcase }: { testcase: TestCase; index: number }) {
    // Definir estilos condicionales para el color del texto
    const textStyle = {
        color: testcase.status === "Successful" ? "green" : (testcase.status === "Fail" || testcase.status === "Error") ? "orange" : "black",
    };

    return (
        <TableRow>
            <TableCell data-testid="testcaseName">
                {testcase.name}
            </TableCell>
            <TableCell data-testid="totalexectime">
                {testcase.execTime ? formatTimeDuration(testcase.execTime) : ''}
            </TableCell>
            <TableCell data-testid="result" className="p-0 w-1/2" style={textStyle}>
                {testcase.status === "Successful" ? "OK" : testcase.status === "Fail" || testcase.status === "Error" ? "Fail" : testcase.status}
            </TableCell>
        </TableRow>
    );
}