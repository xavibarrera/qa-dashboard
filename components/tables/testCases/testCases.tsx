import {
    Table,
    TableHead,
    TableHeaderCell,
    TableBody,
    TableRow,
    Text,
  } from "@tremor/react";
import { useTranslation } from "next-i18next";
import { TestCase } from "../../utils/interfaces";
import { TestCasesRow } from "./testCasesRow";
  
export function TestCases(testcases: TestCase[]) {
    let aux = Object.values(testcases);
    const { t } = useTranslation(["testinfo"]);

    return (    
        <>  
        {aux.length === 0 ? (
            <Text>{t("emptyTestsuites")}</Text>
          ) : (
          <Table className="mt-4">
            <TableHead>
              <TableRow>
                <TableHeaderCell>{"TEST CASES"}</TableHeaderCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {Array.isArray(aux) ? (
            aux.map((testcase: TestCase, index: number) => {
              return (
                <>
                    <TestCasesRow testcase={testcase} index={index}></TestCasesRow>
                </>
              );   
            })
          ) : (
            <TableRow>
              <Text>{t("emptyList")}</Text>
            </TableRow>
          )}
            </TableBody>
          </Table>
          )}
          </>
    );
}