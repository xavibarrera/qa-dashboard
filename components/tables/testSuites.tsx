import React, { useState } from "react";
import { Bar } from "../charts/bar";
import { useRouter } from "next/router";
import { ArrowRightIcon } from '@heroicons/react/24/outline';
import { useTranslation } from "next-i18next";
import {
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableHeaderCell,
  TableBody,
  Icon,
  Button,
} from "@tremor/react";

const data = [
  {
    testSuite: "Cart",
    execTime: 34,
    objResult: {
      data: [
        {
          Date: " ",
          Failed: 20,
          Passed: 23,
          Other: 2,
        },
      ],
      layout: "vertical" as const,
      stack: true,
      maxValue: 45,
      index: "Date",
      className: "mt-4 h-14 w-3/4",
    },
    result: [
      {
        testCase: "Add product to cart",
        execTime: 12,
        result: "OK"
      },
      {
        testCase: "Delete product from cart",
        execTime: 12,
        result: "Fail"
      },
    ],
  },
  {
    testSuite: "Product",
    execTime: 21,
    objResult: {
      data: [
        {
          Date: " ",
          Failed: 20,
          Passed: 23,
          Other: 2,
        },
      ],
      layout: "vertical" as const,
      stack: true,
      maxValue: 45,
      index: "Date",
      className: "mt-4 h-14 w-3/4",
    },
    result: [
      {
        testCase: "Add product to cart",
        execTime: 12,
        result: "OK"
      },
      {
        testCase: "Delete product from cart",
        execTime: 14,
        result: "Fail"
      },
    ],
  },
];


export default function TestSuitesChart() {
  const [selectedItem, setSelectedItem] = useState(null);

  const handleButtonClick = (item: any) => {
    if (selectedItem === item) {
      setSelectedItem(null);
    } else {
      setSelectedItem(item);
    }
  };
  const { t } = useTranslation("common");
  return (
    <Table className="mt-2">
      <TableHead>
        <TableRow className="grid grid-cols-6 gap-4">
          <TableHeaderCell>Test Suites</TableHeaderCell>
          <TableHeaderCell>Exec. Time</TableHeaderCell>
          <TableHeaderCell> </TableHeaderCell>
          <TableHeaderCell>{t("result")}</TableHeaderCell>
        </TableRow>
      </TableHead>

      <TableBody>
        {data.map((item) => (
          <React.Fragment key={item.testSuite}>
            <TableRow className="grid grid-cols-6">
              <TableCell>{item.testSuite}</TableCell>
              <TableCell>{item.execTime}</TableCell>
              <TableCell>
                <Button
                  size="xs"
                  variant="secondary"
                  color="gray"
                  onClick={() => handleButtonClick(item)}
                >
                  {t("seeDetails")}
                </Button>
              </TableCell>
              <TableCell className="col-span-3 ">
                <Bar {...item.objResult} />
              </TableCell>
            </TableRow>
            {selectedItem === item && (
              <TableRow>
                <TableCell colSpan={6}>
                  <PanelContent item={item} />
                </TableCell>
              </TableRow>
            )}
          </React.Fragment>
        ))}
      </TableBody>
    </Table>
  );
}

function PanelContent({ item }: any) {
  const router = useRouter();
  const { t } = useTranslation("common");
  const handleButtonClick = () => router.push("www.google.com");
  return (
    <Table className="bg-slate-50 p-6 sm:p-10 mt-2">
      <TableHead>
        <TableRow>
          <TableHeaderCell>Test Cases</TableHeaderCell>
          <TableHeaderCell>Exec. Time</TableHeaderCell>
          <TableHeaderCell>{t("result")}</TableHeaderCell>
        </TableRow>
      </TableHead>

      <TableBody>
        {item.result.map((testCase: any) => (
          <React.Fragment key={testCase.testCase}>
            <TableRow>
              <TableCell>{testCase.testCase}</TableCell>
              <TableCell>{testCase.execTime}</TableCell>
              <TableCell>
                {
                  testCase.result === "Fail"
                    ?
                    <Button variant="light" color="red" onClick={handleButtonClick}>
                      {testCase.result} {"(Details)"}
                      <Icon size="xs" icon={ArrowRightIcon} color="orange" />
                    </Button>
                    :
                    <Button variant="light" color="green" onClick={handleButtonClick}>
                      {testCase.result} {"(Details)"}
                    </Button>
                }
              </TableCell>
            </TableRow>
          </React.Fragment>
        ))}
      </TableBody>
    </Table>
  )
}