import { test, expect } from "@playwright/experimental-ct-react";
import { NameDateDonut } from './nameDateDonut'

const mockValues = {
    executions: [
      {
        timestamp: "2023-06-27T09:14:24"
      }
    ],
    name: "UT staging 01",
    numTotalTests : 74,
    numFailedTests : 24,
    numPassedTests : 36,
    numOtherTests : 12,
}

test.describe("<NameDateDonut />", () => {
  let component: any;
  test.beforeEach(async ({ mount }) => {
    component = await mount(
      <NameDateDonut {...mockValues} />
    );
  });

  test("Show the correct values", async () => {
    const name = component.locator("[data-testid=executionName]");
    const date = component.locator("[data-testid=date]");
    const hour = component.locator("[data-testid=hour]");
    const button = component.locator("[data-testid=detailsButton]");

    
    await expect(name).toHaveText(mockValues.name);
    await expect(date).toHaveText("2023-06-27");
    await expect(hour).toHaveText("09:14:24");
    await expect(button).toHaveText("moreDetails");
    await button.click();
  });
});