import { test, expect } from "@playwright/experimental-ct-react";
import { Small } from './small';

const smallMockValues = {
  header: "10",
  text: "UT Staging 01",
  time: "24h",
  color1: undefined,
  color2: undefined,
  invisible: "no",
  percentNumber: 15
}

test.describe("<Small />", () => {
  let component: any;
  test.beforeEach(async ({ mount }) => {
    component = await mount(
      <Small {...smallMockValues} />
    );
  });

  test("Show the correct values", async () => {
    const text = component.locator("[data-testid=text]");
    const header = component.locator("[data-testid=total-number]");
    const title = component.locator("[data-testid=exit_rate]");
    const time = component.locator("[data-testid=time]");

    await expect(text).toHaveText(smallMockValues.text);
    await expect(header).toHaveText(smallMockValues.header);
    await expect(title).toHaveText(smallMockValues.percentNumber + "% Éxito");
    await expect(time).toHaveText(smallMockValues.time);

  });
});