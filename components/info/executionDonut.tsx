import { Subtitle, Metric, Icon } from "@tremor/react";
import Donut from "../charts/donut";
import { CalendarIcon, ClockIcon } from "@heroicons/react/24/outline";
import { Execution } from "../utils/interfaces";

export function ExecutionDonut(execution: Execution) {
  let date = "";
  let hour = "";
  if (execution !== undefined) {
    date = execution.timestamp?.slice(0, 10) || "";
    hour = execution.timestamp?.slice(11, 19) || "";
  }

  return (
    <div className="flex">
      <div className="w-1/2 p-4">
        <Metric className="h-10 w-48 text-2xl mt-4 whitespace-nowrap" data-testid="executionName">
          # {execution.executionNum}
        </Metric>
        <Subtitle className="flex text-xs mt-4" color="slate">
          <Icon className="relative bottom-1" size="xs" icon={CalendarIcon} color="gray" />
          <a data-testid="date">{date}</a>
          <Icon className="relative bottom-1" size="xs" icon={ClockIcon} color="gray" />
          <a data-testid="hour">{hour}</a>
        </Subtitle>
      </div>
      <div className="w-1/2 flex mb-2 items-center justify-center">
        <Donut
          numTotalTests={execution.numTotalTests}
          numFailedTests={execution.numFailedTests}
          numPassedTests={execution.numPassedTests}
          numOtherTests={execution.numOtherTests}
        />
      </div>
    </div>
  );
}
