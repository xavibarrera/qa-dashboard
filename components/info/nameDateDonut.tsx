import { Button, Subtitle, Metric, Icon } from "@tremor/react";
import Donut from "../charts/donut";
import { CalendarIcon, ClockIcon } from "@heroicons/react/24/outline";
import { useTranslation } from "next-i18next";
import { Test } from "../utils/interfaces";
import { truncateName } from "../utils/helper";

export function NameDateDonut({
  executions,
  name,
  numTotalTests,
  numFailedTests,
  numPassedTests,
  numOtherTests,
}: Test) {
  const { t } = useTranslation("common");
  let date = "";
  let hour = "";
  if (executions !== undefined && executions.length > 0) {
    date = executions[0].timestamp?.slice(0, 10) || "";
    hour = executions[0].timestamp?.slice(11, 19) || "";
  }
  let truncatedName;
  const maxLength = 20;
  let show = false;
  if (name !== undefined) {
    if (name.length > maxLength) {
      truncatedName = truncateName(name)
      show = true;
    }
  }
  return (
    <div>
      <div>
        {show ? (
          <div className="group relative w-max">
            <Metric className="h-10 w-48 text-2xl" data-testid="executionName">
              {truncatedName}
            </Metric>
            <span className="bg-white text-black rounded p-2 absolute -top-10 left-1/2 transform -translate-x-1/2 opacity-0 transition-opacity group-hover:opacity-100  border border-black/10 dark:border-gray-900/50 dark:bg-gray-700 shadow">
              {name}
            </span>
          </div>
        ) : (
          <Metric className="h-10 w-48 text-2xl whitespace-nowrap" data-testid="executionName">
            {name}
          </Metric>
        )}
      </div>
      <div className="grid grid-cols-2">
        <div className="grid content-around">
          <div>
            <Subtitle className="flex text-xs" color="slate">
              <Icon className="relative bottom-1" size="xs" icon={CalendarIcon} color="gray" />
              <a data-testid="date">{date}</a>
              <Icon className="relative bottom-1" size="xs" icon={ClockIcon} color="gray" />
              <a data-testid="hour">{hour}</a>
            </Subtitle>
          </div>
          <div>
            <Button variant="secondary" color="orange">
              <a data-testid="detailsButton">{t("moreDetails")}</a>
              
            </Button>
          </div>
        </div>
        <div>
          <Donut
            {...{ numTotalTests }}
            {...{ numFailedTests }}
            {...{ numPassedTests }}
            {...{ numOtherTests }}
          />
        </div>
      </div>
    </div>
  );
}
