import React, { ReactNode } from "react";
import { useRouter } from "next/router";

interface Multiple {
    column1: ReactNode;
    column2: ReactNode;
    column3?: ReactNode;
    redirect?: string;
    token?: string;
}

export function Multiple({ column1, column2, column3, redirect, token }: Multiple) {
    const router = useRouter();

    const handleButtonClick = () => {
        router.push({
          pathname: redirect + "/" + token,
        });
      };
    return (
        <div className="grid grid-cols-3 gap-4 justify-center h-fit" onClick={() => handleButtonClick()}>
            <div className="h-full border-r pr-4 flex justify-center items-center">{column1}</div>
            <div className="h-full border-r pr-4 flex justify-center items-center">{column2}</div>
            {column3 != null && <div className="flex justify-center items-center">{column3}</div>}
        </div>
      );
}