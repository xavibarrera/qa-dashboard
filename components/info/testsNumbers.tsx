import { Metric, Text, Flex, Icon } from "@tremor/react";
import { StopIcon } from "@heroicons/react/24/solid";
import { Test } from "../utils/interfaces";

export function TestsNumbers({numTotalTests, numFailedTests, numPassedTests, numOtherTests}: Test) {
  return (
    <Flex className="justify-evenly">
      <div className="text-center">
        <Text>Total tests</Text>
        <Metric data-testid="total-test">{numTotalTests}</Metric>
      </div>
      <div>
        <Flex className="justify-start">
          <Icon size="xs" icon={StopIcon} color="orange" />
          <Text>Failed</Text>
        </Flex>
        <Flex className="justify-start">
          <Icon size="xs" icon={StopIcon} color="green" />
          <Text>Passed</Text>
        </Flex>
        <Flex className="justify-start">
          <Icon size="xs" icon={StopIcon} color="gray" />
          <Text>Other</Text>
        </Flex>
      </div>
      <div>
        <Text>
          <strong data-testid="failed">{numFailedTests}</strong>
        </Text>
        <Text>
          <strong data-testid="passed">{numPassedTests}</strong>
        </Text>
        <Text>
          <strong data-testid="other">{numOtherTests}</strong>
        </Text>
      </div>
    </Flex>
  );
}
