import { test, expect } from "@playwright/experimental-ct-react";
import { ExecutionsResult } from './executionsResult';

const mockValues = {
    totalTests : 74,
    failed : 24,
    passed : 36,
    other : 12,
    executionName: "UT staging 01",
    date: "2023-03-12",
    hour: "17:45",
    showButton: true,
	executionUrl: "",
}

test.describe("<ExecutionsResult />", () => {
  let component: any;
  test.beforeEach(async ({ mount }) => {
    component = await mount(
      <ExecutionsResult {...mockValues} />
    );
  });

  test("Show the correct values", async () => {
    const failed = component.locator("[data-testid=failed]");
    const passed = component.locator("[data-testid=passed]");
    const other = component.locator("[data-testid=other]");
    const button = component.locator("[data-testid=button]");

    await expect(failed).toHaveText(mockValues.failed.toString());
    await expect(passed).toHaveText(mockValues.passed.toString());
    await expect(other).toHaveText(mockValues.other.toString());
    await expect(button).toHaveText("seeFailedTest");
    await button.click();
  });
});