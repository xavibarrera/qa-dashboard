import { Text, Metric, Subtitle, Color } from "@tremor/react";

interface Small {
    header: any;
    text: string;
    time: string;
    color1?: Color;
    color2?: Color;
    invisible: string
    percentNumber: number
}

export function Small({ header, text, time, color1 = undefined, color2 = undefined, invisible, percentNumber }: Small) {
    let color3: Color = "red";
    if (percentNumber >= 50) color3 = "yellow";
    if (percentNumber >= 70) color3 = "green";
    return (
        <div className="truncate">
            <div>
                <a data-testid="text">
                    <Text color={color1}>{text}</Text>
                </a>
                <a data-testid="total-number">
                    <Metric color={color2}>{header}</Metric>
                </a>
            </div>
            <div className="flex p-3 justify-between items-center mt-3">
                <a data-testid="exit_rate">
                    <Text color={color3} className={invisible}>{percentNumber}% Éxito</Text>
                </a>
                <div className="w-12 ml-4 rounded-[15px] opacity-100 bg-[#efefef]">
                    <a data-testid="time">
                        <Subtitle className="text-center">{time}</Subtitle>
                    </a>
                </div>
            </div>
        </div>
    );
}
