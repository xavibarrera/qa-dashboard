import { Button, Text, Flex, Icon } from "@tremor/react";
import ExecutionDonut from "../charts/donut";
import { StopIcon } from "@heroicons/react/24/solid";
import { useTranslation } from "next-i18next";

interface ExecutionsResult {
    failed: number;
    passed: number;
    other: number;
    showButton?: boolean;
}
export function handleButtonClick() {
    document.getElementById('test-cases-chart')?.scrollIntoView({ behavior: 'smooth' });
}
export function ExecutionsResult({ failed, passed, other, showButton = true }: ExecutionsResult) {
    const { t } = useTranslation("common");
    return (
        <div className="grid grid-cols-2 gap-12">
            <div className="col-span-1">
                <h1 className="h-16 flex mt-4 mb-6">
                    <strong>{t("executionResults")}</strong>
                </h1>
                <div className="grid grid-cols-2">
                    <div className="col-span-1">
                        <Flex className="justify-start">
                            <Icon size="xs" icon={StopIcon} color="orange" />
                            <Text>{t("Failed")}</Text>
                        </Flex>
                    </div>
                    <div className="col-span-1 ml-5">
                        <strong data-testid="failed">{failed}</strong>
                    </div>
                </div>
                <div className="grid grid-cols-2">
                    <div className="col-span-1">
                        <Flex className="justify-start">
                            <Icon size="xs" icon={StopIcon} color="green" />
                            <Text>{t("Passed")}</Text>
                        </Flex>
                    </div>
                    <div className="col-span-1 ml-5">
                        <strong data-testid="passed">{passed}</strong>
                    </div>
                </div>
                <div className="grid grid-cols-2">
                    <div className="col-span-1">
                        <Flex className="justify-start">
                            <Icon size="xs" icon={StopIcon} color="gray" />
                            <Text>{t("Other")}</Text>
                        </Flex>
                    </div>
                    <div className="col-span-1 ml-5">
                        <strong data-testid="other">{other}</strong>
                    </div>
                </div>

                {showButton ? (
                    <div className="h-8 mt-10 flex">
                        <Button data-testid="button" color="orange" onClick={handleButtonClick}>{t("seeFailedTest")}</Button>
                    </div>
                ) : null}
            </div>
            <div className="col-span-1 mt-4">
                <ExecutionDonut 
                    numFailedTests={failed}
                    numPassedTests={passed}
                    numOtherTests={other}
                />
            </div>
        </div>
    )
}