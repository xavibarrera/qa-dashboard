import { test, expect } from "@playwright/experimental-ct-react";
import { TestsNumbers } from './testsNumbers';

const testNumbersMockValues = {
  numTotalTests: 74,
  numFailedTests: 24,
  numPassedTests: 36,
  numOtherTests: 12
}

test.describe("<TestsNumbers />", () => {
    let component: any;
    test.beforeEach(async ({ mount }) => {
      component = await mount(
        <TestsNumbers {...testNumbersMockValues} />
      );
    });

    test("Show the correct values", async () => {
        const total_test = component.locator("[data-testid=total-test]");
        const failed = component.locator("[data-testid=failed]");
        const passed = component.locator("[data-testid=passed]");
        const other = component.locator("[data-testid=other]");
    
        await expect(total_test).toHaveText(testNumbersMockValues.numTotalTests.toString());
        await expect(failed).toHaveText(testNumbersMockValues.numFailedTests.toString());
        await expect(passed).toHaveText(testNumbersMockValues.numPassedTests.toString());
        await expect(other).toHaveText(testNumbersMockValues.numOtherTests.toString());
    });
});