import { test, expect } from "@playwright/experimental-ct-react";
import Donut from "./donut";

const donutMockValues = {
  numFailedTests : 24,
  numPassedTests : 36,
  numOtherTests : 12,
}

test.describe("<Donut />", () => {
    let component: any;
    test.beforeEach(async ({ mount }) => {
      component = await mount(
        <Donut {...donutMockValues}/>
      );
    });

    test("Show correct Data", async ({ page }) => {
        const legend = component.locator("[role='dialog']");
        
        await page.locator(".recharts-wrapper").hover({position: {x:132 , y:84}});
        page.waitForSelector("[role='dialog']");
        await expect(legend).toContainText("36Successful");

        await page.locator(".recharts-wrapper").hover({position: {x:19 , y:115}});
        await expect(legend).toContainText("24Failure");

        await page.locator(".recharts-wrapper").hover({position: {x:43 , y:37}});
        await expect(legend).toContainText("12Skipped");
    });
});