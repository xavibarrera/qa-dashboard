import { DonutChart } from "@tremor/react";
import { Test } from "../utils/interfaces";

const valueFormatter = (number: number) =>
  `${Intl.NumberFormat("us").format(number).toString()}`;

export default function Donut({ numFailedTests, numPassedTests, numOtherTests }: Test) {
  const tests = [
    {
      status: "Successful",
      quantity: numPassedTests,
    },
    {
      status: "Failure",
      quantity: numFailedTests,
    },
    {
      status: "Skipped",
      quantity: numOtherTests,
    },
  ];
  return (
    <div className="w-[150px] h-[150px]">
      <a data-testid="donut">
      <DonutChart
        data={tests}
        showLabel={false}
        category="quantity"
        index="status"
        valueFormatter={valueFormatter}
        colors={["green", "orange", "gray"]}
      />
      </a>
    </div>
  );
}
