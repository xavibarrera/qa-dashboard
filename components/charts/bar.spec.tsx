import { test, expect } from "@playwright/experimental-ct-react";
import { Bar } from './bar';

export const barMockValues = {
    data : [{
        Date: "10/10",
        Failed: 10,
        Passed: 60,
        Other: 10,
    }], 
	layout : "horizontal" as const,
	stack : true,
	maxValue : 80,
	index: "Date",
	className: "mt-4 h-40",
}

test.describe("<Bar />", () => {
    let component: any;
    test.beforeEach(async ({ mount }) => {
      component = await mount(
        <Bar {...barMockValues}/>
      );
    });

    test("Show correct Date", async () => {
        const x_axis = component.locator(".recharts-xAxis");
        await expect(x_axis).toHaveText("10/10");
      });

      test("Show correct MaxValue", async () => {
        const y_axis = component.locator(".recharts-surface");
        await expect(y_axis).toContainText("80");
        await expect(y_axis).toContainText("0");
      });

      test("Show correct Data", async ({ page }) => {
        const y_axis = component.locator(".text-gray-700");
        await page.locator(".recharts-surface").hover();
        await expect(y_axis.nth(0)).toContainText("10/10");
        await expect(y_axis.nth(1)).toContainText("10");
        await expect(y_axis.nth(2)).toContainText("60");
      });
});
