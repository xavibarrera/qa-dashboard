import { AreaChart } from "@tremor/react";
import { Execution } from "../../utils/interfaces";

const valueFormatter = (number: number) =>
  Intl.NumberFormat("us").format(number).toString();

function changePropertyNames(executions: Execution[]): Execution[] {
  return executions.map((execution) => {
    return {
      ...execution,
      Failed: execution.numFailedTests,
      Other: execution.numOtherTests,
      Passed: execution.numPassedTests,
    };
  });
}

export function Area(executions: Execution[]) {
  let aux = Object.values(executions);
  let data = changePropertyNames(aux);
  return (
    <AreaChart
      className={ "mt-4 h-40" }
      data={data}
      index="timestamp"
      categories={["Failed", "Passed", "Other"]}
      colors={["orange", "green", "gray"]}
      valueFormatter={valueFormatter}
    />
  );
}
