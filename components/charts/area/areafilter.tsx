import { Execution } from "../../utils/interfaces";
import { getLastExecutions } from "@/components/utils/executions";
import { Area } from "./area";
import { calculateDailyTotals } from "@/components/utils/time";
import { Selection } from "./selection";
import { DashboardContext } from "@/utils/dashboardContext";
import { useContext, useState } from "react";


function changePropertyNames(executions: Execution[]): Execution[] {
  return executions.map((execution) => {
    return {
      ...execution,
      Failed: execution.numFailedTests,
      Other: execution.numOtherTests,
      Passed: execution.numPassedTests,
    };
  });
}
  
export function AreaFilter() {
    const [selectedVariant, setSelectedVariant] = useState('7DAYS');
    const [finalData, setFinalData] = useState<Execution[]>([]);
    const {dashboardData} = useContext(DashboardContext);

  
    const handleVariantChange = (variant: string) => {
      setSelectedVariant(variant);
    
      let data;
      if (variant === '7DAYS') {
        data = getLastExecutions(dashboardData[0], '7DAYS');
      } else if (variant === '30DAYS') {
        data = getLastExecutions(dashboardData[0], '30DAYS');
      } else if (variant === '90DAYS') {
        data = getLastExecutions(dashboardData[0], '90DAYS');
      }
      const totals = calculateDailyTotals(data?.executions || []);
      const final = changePropertyNames(Object.values(totals));
      setFinalData(final);
    };
  
    return (
      <div>
        <Selection
          selectedVariant={selectedVariant}
          handleVariantChange={handleVariantChange}
        />
        <Area {...finalData}/>
      </div>
    );
}
