
import { useTranslation } from "next-i18next";

export function Selection({ selectedVariant, handleVariantChange }: any) {
    const { t } = useTranslation(["common"]);

    return (
        <div className="flex">
            <strong> {t("executionsResults")}</strong>
            <select
            className="ml-4 border rounded p-1"
            value={selectedVariant}
            onChange={(e) => handleVariantChange(e.target.value)}
            >
            <option value="7DAYS">{t("last7days")}</option>
            <option value="30DAYS">{t("last30days")}</option>
            <option value="90DAYS">{t("last90days")}</option>
            </select>
        </div>
    );
}