import { test, expect } from "@playwright/experimental-ct-react";
import { Area } from './area';

const areaMockValues = [
  {
    execTime: 80,
    numFailedTests: 10,
    numOtherTests: 20,
    numPassedTests: 30,
    numTotalTests: 40,
    testSuites: [] as any,
    timestamp: "2023-06-26T09:14:23",
    _id: "ef253544f4cfd8579847f9f6",
  },
  {
    execTime: 70,
    numFailedTests: 20,
    numOtherTests: 5,
    numPassedTests: 25,
    numTotalTests: 50,
    testSuites: [] as any,
    timestamp: "2022-05-22T11:10:56",
    _id: "ef253544f4cfd8579847f9f6",
  }
];

test.describe("<Area />", () => {
  let component: any;
  test.beforeEach(async ({ mount }) => {
    component = await mount(
      <Area {...areaMockValues} />
    );
  });

  test("Show correct Date", async () => {
    const x_axis = component.locator(".recharts-cartesian-axis-tick-value").nth(0);
    const x_axis2 = component.locator(".recharts-cartesian-axis-tick-value").nth(1);
    await expect(x_axis).toHaveText("2023-06-26T09:14:23");
    await expect(x_axis2).toHaveText("2022-05-22T11:10:56");
  });

  test("Show correct MaxValue", async () => {
    const y_axis = component.locator(".recharts-cartesian-axis-tick-value").nth(4);
    const y_axis2 = component.locator(".recharts-cartesian-axis-tick-value").nth(3);
    await expect(y_axis).toHaveText("32");
    await expect(y_axis2).toHaveText("16");
  });

  test("Show correct Data", async ({ page }) => {
    await page.locator(".recharts-surface").hover();
    
    const leftDataElement = component.locator(".recharts-tooltip-wrapper");
    await expect(leftDataElement).toContainText("10");
    await expect(leftDataElement).toContainText("20");
    await expect(leftDataElement).toContainText("30");
  });
});