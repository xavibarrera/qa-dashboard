import { BarChart } from "@tremor/react";

const valueFormatter = (number: number) =>
  Intl.NumberFormat("us").format(number).toString();

interface Bar {
  data: Array<any>;
  layout: "vertical" | "horizontal";
  stack: boolean;
  maxValue: number;
  index: string;
  className: string;
  test?: string;
  group?: string;
}
export function Bar({ data, layout, stack, maxValue, index, className }: Bar) {
  return (
    <BarChart
      className={className}
      data={data}
      index={index}
      categories={["Failed", "Passed", "Other"]}
      colors={["orange", "green", "gray"]}
      valueFormatter={valueFormatter}
      stack={stack}
      maxValue={maxValue}
      showLegend={false}
      layout={layout}
      yAxisWidth={20}
    />
  );
}
