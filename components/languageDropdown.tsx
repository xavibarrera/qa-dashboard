import { Dropdown, Button } from "@tremor/react";
import { useRouter } from "next/router";
import handleLocaleChange from "utils/handleLocaleChange";

export default function LanguageDropdown() {
    const router = useRouter();

    return (
        <Dropdown
            className="justify-center w-1"
            placeholder={router.locale!.toUpperCase()}
        >
            <div className="flex justify-center">
                <Button
                    className="flex justify-center"
                    variant="light"
                    color="gray"
                    onClick={() => handleLocaleChange("en", router)}
                >
                    {"English"}
                </Button>
            </div>
            <div className="flex justify-center">
                <Button
                    variant="light"
                    color="gray"
                    onClick={() => handleLocaleChange("es", router)}
                >
                    {"Español"}
                </Button>
            </div>
        </Dropdown>
    );
}