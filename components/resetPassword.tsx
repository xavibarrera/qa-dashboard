import { Button, Metric, Subtitle, Text } from "@tremor/react";
import { useTranslation } from "next-i18next";
import { useState } from "react";
import { useRouter } from "next/router";
import { InputPassword } from "../components/inputPassword";

interface ResetPasswordData {
  email: string;
  password: string;
}
interface Data {
  email: string;
}
export default function ResetPassword({ email }: Data) {
  const [messagePassword, setMessagePassword] = useState("");
  const [errorPassword, setErrorPassword] = useState(false);
  const [passwordData, setPasswordData] = useState("");
  const [passwordValidated, setPasswordValidated] = useState(false);
  const { t } = useTranslation("forgotPassword");
  const router = useRouter();

  function hideError() {
    setMessagePassword("");
    setErrorPassword(false);
  }
  async function handleResetPassword(data: ResetPasswordData) {
    if (passwordValidated) {
      try {
        const res = await fetch(
          process.env.NEXT_PUBLIC_url + `/api/users/auth/changePassword`,
          {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          }
        );

        if (res.status === 200) {
          router.push("/changedPassword");
        } else {
          console.log(res.json());
          const message = t("serverError");
          setMessagePassword(message);
          setErrorPassword(true);
          setTimeout(hideError, 2000);
        }
      } catch (err) {
        console.log(err);
      }
    }
  }
  return (
    <div className="flex h-screen justify-center items-center">
      <div className="text-center ">
        <Metric>{t("choosePassword")}</Metric>
        <Subtitle className="mt-4">{t("chooseSubtitle")}</Subtitle>

        <div className="mt-10">
          <InputPassword
            value={passwordData}
            setPasswordData={setPasswordData}
            setPasswordValidated={setPasswordValidated}
            details={true}
            onEnter={() => {
              handleResetPassword({
                email: email,
                password: passwordData,
              });
            }}
          />
        </div>
        {errorPassword && (
          <Text className="text-red-500"> {messagePassword}</Text>
        )}
        <Button
          variant="primary"
          color="orange"
          className="h-8 w-28 mt-10"
          data-testid="btnLogin"
          disabled={!passwordValidated}
          onClick={() =>
            handleResetPassword({
              email: email,
              password: passwordData,
            })
          }
        >
          {t("button")}
        </Button>
      </div>
    </div>
  );
}
