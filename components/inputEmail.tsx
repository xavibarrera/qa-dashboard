import { TextInput } from "@tremor/react";
import { Dispatch, SetStateAction, useState } from "react";
import validator from "email-validator";
interface InputEmailProps {
  value?: string;
  setEmailData: Dispatch<SetStateAction<string>>;
  setEmailValidated: Dispatch<SetStateAction<boolean>>;
  emptyEmailMessage?: string;
  wrongEmailMessage?: string;
  onEnter?: () => void;
}
export function InputEmail({
  value,
  setEmailData,
  setEmailValidated,
  emptyEmailMessage = "Enter your email address",
  wrongEmailMessage = "Invalid email",
  onEnter,
}: InputEmailProps) {
  const [errorEmail, setErrorEmail] = useState(false);
  const [messageEmail, setMessageEmail] = useState("");

  function resetAuthErrorStates() {
    setErrorEmail(false);
    setMessageEmail("");
  }
  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      onEnter?.(); // Ejecuta la función `onEnter` si está definida
    }
  };
  const checkInputs = (event: any) => {
    let error = false;
    const trimmedEmailInput = event.target.value.trim(); // Eliminar espacios en blanco al principio y al final
    if (trimmedEmailInput === "") {
      const message = emptyEmailMessage;
      setMessageEmail(message);
      setErrorEmail(true);
      error = true;
    } else if (!validator.validate(trimmedEmailInput)) {
      const message = wrongEmailMessage;
      setMessageEmail(message);
      setErrorEmail(true);
      error = true;
    }
    setEmailData(event.target.value);
    setEmailValidated(!error);
  };

  const handleEmailInputChange = (event: any) => {
    setEmailData(event.target.value);
	checkInputs(event)
    resetAuthErrorStates();
  };

  return (
    <TextInput
      type="text"
      error={errorEmail}
      errorMessage={messageEmail}
      placeholder="Email"
      value={value}
      data-testid="inputEmail"
      onChange={handleEmailInputChange}
      onBlur={checkInputs}
      onKeyDown={handleKeyDown}
    />
  );
}
